const FlashSuccessLogin = $('.flash-success-login').data('flashdata');
if (FlashSuccessLogin) {
  swal.fire({
     title: FlashSuccessLogin,
     text: "Login",
     icon: "success",
  });
}

const FlashErrorLogin = $('.flash-error-login').data('flashdata');
if (FlashErrorLogin) {
  swal.fire({
     title: FlashErrorLogin,
     text: "Periksa kembali username dan password anda",
     icon: "warning",
  });
}
const FlashSuccessRegister = $('.flash-success-register').data('flashdata');
if (FlashSuccessRegister) {
  swal.fire({
     title: FlashSuccessRegister,
     text: "Akun anda berhasil dibuat",
     icon: "success",
  });
}

const FlashErrorRegister = $('.flash-error-register').data('flashdata');
if (FlashErrorRegister) {
  swal.fire({
     title: FlashErrorRegister,
     text: "Password tidak sesuai, mohon cek kembali",
     icon: "warning",
  });
}
