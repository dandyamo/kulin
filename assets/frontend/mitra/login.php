<?php 
 $st = app_param();
?>
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Tentor - Kulin</title>
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/mitr/login/style.css" />

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

</head>

<body>
  <div class="container">
    <div class="container__forms">
      <div class="form">
        <form action="<?php echo base_url('mitra/login/aksi_login'); ?>" class="form__sign-in" method="post">
          <h2 class="form__title">Masuk</h2>
          <div class="form__input-field">
            <i class="fas fa-user"></i>
            <input type="text" name="username" placeholder="Username" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" placeholder="Password" required />
          </div>
          <input class="form__submit" type="submit" value="Masuk" />
        </form>
      </div>
    </div>
    <div class="container__panels">
      <div class="panel panel__left">
        <!-- Brand Logo -->
        <a href="<?php echo base_url(); ?>">
          <img class="panel__image" src="<?php echo base_url(); ?>assets/frontend/kitaarisan.png">
        </a>
      </div>
      <div class="panel panel__right">
      <a href="<?php echo base_url(); ?>">
          <img class="panel__image" src="<?php echo base_url(); ?>assets/frontend/kitaarisan.png">
        </a>
      </div>
    </div>
  </div>

  
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/mitr/jquery-3.3.1.js"></script>    
  <script src="https://kit.fontawesome.com/1788c719dd.js" crossorigin="anonymous"></script>

<script>
    const signInBtn = document.querySelector("#sign-in-btn");
    const container = document.querySelector(".container");
    signInBtn.addEventListener("click", () => {
      container.classList.remove("sign-up-mode");
    });

</script>

</body>