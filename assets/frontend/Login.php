<?php



class Login Extends CI_Controller{


	public function index()

	{
		$this->check_login();

	}


	public function check_login(){
		if(is_login())redirect((sesi('redir_login')?sesi('redir_login'):'home'));
		$this->load->view('mitra/login');
	}


	public function aksi_login(){

		$username = $this->input->post('username');

		$password = $this->input->post('password');



		$where = array(

			'username' => $username
		
		);



		$user = $this->Model_general->login('user',$where)->row_array();

		

		if ($this->bcrypt->check_password($password, $user['password']))

		{

			if ($user > 0) {



			$data_session = array(

				'username' => $username,

				'nama' => $user['nama_user'],

				'id' => $user['id_user'],

				'role' => $user['role'],

				'login_state' => TRUE

				);



			$this->session->set_userdata($data_session);



			redirect(site_url("mitra/home"));

			

			} else {



				$this->session->set_flashdata('fail','Username dan password tidak ditemukan');

				redirect(site_url("mitra/login"));



			}

		}

		else

		{

				$this->session->set_flashdata('fail','Periksa kembali username dan password anda');

				redirect(site_url("mitra/login"));

		}


	}


	function logout(){

		$this->session->sess_destroy();

		redirect(site_url('mitra/loginlogin'));

	}


}