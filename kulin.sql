-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 03, 2022 at 11:05 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kulin`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(11) NOT NULL,
  `id_kategori_artikel` int(11) NOT NULL,
  `nama_artikel` varchar(100) NOT NULL,
  `deskripsi_artikel` text NOT NULL,
  `tgl_artikel` date NOT NULL,
  `gambar_artikel` varchar(225) NOT NULL,
  `status` int(11) NOT NULL,
  `id_operator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `id_kategori_artikel`, `nama_artikel`, `deskripsi_artikel`, `tgl_artikel`, `gambar_artikel`, `status`, `id_operator`) VALUES
(1, 1, 'Test', 'Test', '2022-05-29', 'h2-1.jpg', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `freelancer`
--

CREATE TABLE `freelancer` (
  `id_freelancer` int(11) NOT NULL,
  `nama_freelancer` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `no_telp` char(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `photo` varchar(225) DEFAULT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `freelancer`
--

INSERT INTO `freelancer` (`id_freelancer`, `nama_freelancer`, `jenis_kelamin`, `no_telp`, `email`, `alamat`, `username`, `password`, `photo`, `role`) VALUES
(1, 'Amanah', 'Perempuan', '082233056769', 'amanah@gmail.com', 'Yogyakarta', 'amanah', 'qwerty', 'audrey.PNG', 3);

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `level` int(2) DEFAULT NULL,
  `ignore_limits` tinyint(1) DEFAULT 0,
  `is_private_key` tinyint(1) DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 0, 'a3VsaW4=', NULL, 0, 0, NULL, NULL),
(8, 10, 'MDgyMjMzMDU2NzY5YW1hbmFoQGdtYWlsLmNvbQ==', NULL, 0, 0, NULL, NULL),
(11, 14, 'NjY2YW1hbmFoQGdtYWlsLmNvbQ==', NULL, 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `operator`
--

CREATE TABLE `operator` (
  `id_operator` int(11) NOT NULL,
  `nama_operator` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `no_telp` char(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `operator`
--

INSERT INTO `operator` (`id_operator`, `nama_operator`, `jenis_kelamin`, `no_telp`, `email`, `alamat`, `username`, `password`, `role`) VALUES
(1, 'Super Admin', 'Laki-laki', '082233056769', 'superadmin@gmail.com', 'Yogyakarta', 'superadmin', 'qwerty', 1),
(2, 'Dandy Akhmarienno Putra', 'Laki-laki', '082233056769', 'dandyamo25@gmail.com', 'Yogyakarta, Indonesia', 'admin', 'qwerty', 2);

-- --------------------------------------------------------

--
-- Table structure for table `parameter`
--

CREATE TABLE `parameter` (
  `param` varchar(100) NOT NULL,
  `val` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `parameter`
--

INSERT INTO `parameter` (`param`, `val`) VALUES
('address', 'Yogyakarta'),
('description', 'KULIN - Kuliah & Kursus Online'),
('email', 'officialkulin@gmail.com'),
('footer', '2020 KULIN - Kuliah & Kursus Online - All Rights Reserved.'),
('keywords', ''),
('logo', 'logo-k.png'),
('phone', '098777779'),
('title', 'KULIN - Kuliah & Kursus Online');

-- --------------------------------------------------------

--
-- Table structure for table `peserta`
--

CREATE TABLE `peserta` (
  `id_peserta` int(11) NOT NULL,
  `nama_peserta` varchar(100) NOT NULL,
  `no_telp` char(15) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `photo` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `peserta`
--

INSERT INTO `peserta` (`id_peserta`, `nama_peserta`, `no_telp`, `email`, `alamat`, `username`, `password`, `jenis_kelamin`, `photo`) VALUES
(1, 'Dandy Akhmarienno Putra', '082233056769', 'dandyamo25@gmail.com', 'Yogyakarta', 'dandyamo', 'Ronggolawe1125', 'Laki-laki', NULL),
(2, 'Audrey Pass', '082233056769', 'audrey@gmail.com', 'Bandung', 'audreypass', 'qwerty', 'Perempuan', NULL),
(10, 'Indri Barbie', '082233056769', 'indribarbie@gmail.com', 'Bandung', 'indribarbie', 'qwerty', 'Perempuan', NULL),
(12, 'Dandy Amo', '082233056769', 'dandyamo25@gmail.com', 'Yogykarta', 'dandyamo', 'qwerty', 'Laki', NULL),
(14, 'Amanah', '666', 'amanah@gmail.com', 'Bandung', 'amanah', 'd8578edf8458ce06fbc5bb76a58c5ca4', 'Perempuan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ref_fakultas`
--

CREATE TABLE `ref_fakultas` (
  `id_ref_fakultas` int(11) NOT NULL,
  `id_ref_kategori` int(11) NOT NULL,
  `ref_fakultas` varchar(100) NOT NULL,
  `link_fakultas` varchar(100) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_fakultas`
--

INSERT INTO `ref_fakultas` (`id_ref_fakultas`, `id_ref_kategori`, `ref_fakultas`, `link_fakultas`, `status`) VALUES
(1, 1, 'Ilmu Komputer', 'ilmu-komputer', 1),
(2, 1, 'Hukum', 'hukum', 1),
(3, 1, 'Ekonomi & Sosial', 'ekonomi-sosial', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_icon_sosmed`
--

CREATE TABLE `ref_icon_sosmed` (
  `id_ref_icon_sosmed` int(11) NOT NULL,
  `icon_sosmed` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_icon_sosmed`
--

INSERT INTO `ref_icon_sosmed` (`id_ref_icon_sosmed`, `icon_sosmed`) VALUES
(1, 'instagram'),
(2, 'facebook'),
(3, 'twitter');

-- --------------------------------------------------------

--
-- Table structure for table `ref_jurusan`
--

CREATE TABLE `ref_jurusan` (
  `id_ref_jurusan` int(11) NOT NULL,
  `id_ref_fakultas` int(11) NOT NULL,
  `ref_jurusan` varchar(100) NOT NULL,
  `link_jurusan` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_jurusan`
--

INSERT INTO `ref_jurusan` (`id_ref_jurusan`, `id_ref_fakultas`, `ref_jurusan`, `link_jurusan`, `status`) VALUES
(1, 1, 'Teknik Informatika', 'teknik-informatika', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kategori`
--

CREATE TABLE `ref_kategori` (
  `id_ref_kategori` int(11) NOT NULL,
  `ref_kategori` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kategori`
--

INSERT INTO `ref_kategori` (`id_ref_kategori`, `ref_kategori`) VALUES
(1, 'Universitas'),
(2, 'Umum');

-- --------------------------------------------------------

--
-- Table structure for table `ref_kategori_artikel`
--

CREATE TABLE `ref_kategori_artikel` (
  `id_kategori_artikel` int(11) NOT NULL,
  `kategori_artikel` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kategori_artikel`
--

INSERT INTO `ref_kategori_artikel` (`id_kategori_artikel`, `kategori_artikel`, `status`) VALUES
(1, 'Umum', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_kat_umum`
--

CREATE TABLE `ref_kat_umum` (
  `id_ref_kat_umum` int(11) NOT NULL,
  `id_ref_kategori` int(11) NOT NULL,
  `ref_kat_umum` varchar(100) NOT NULL,
  `link_kat_umum` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_kat_umum`
--

INSERT INTO `ref_kat_umum` (`id_ref_kat_umum`, `id_ref_kategori`, `ref_kat_umum`, `link_kat_umum`, `status`) VALUES
(1, 2, 'Musik', 'musik', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_matkul`
--

CREATE TABLE `ref_matkul` (
  `id_ref_matkul` int(11) NOT NULL,
  `id_ref_fakultas` int(11) NOT NULL,
  `id_ref_jurusan` int(11) NOT NULL,
  `id_ref_semester` int(11) NOT NULL,
  `ref_matkul` varchar(100) NOT NULL,
  `deskripsi` longtext NOT NULL,
  `link_matkul` varchar(100) NOT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_matkul`
--

INSERT INTO `ref_matkul` (`id_ref_matkul`, `id_ref_fakultas`, `id_ref_jurusan`, `id_ref_semester`, `ref_matkul`, `deskripsi`, `link_matkul`, `status`) VALUES
(1, 1, 1, 1, 'Sistem Operasi', '<p>Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, eleifend euismod pellentesque vel, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus.</p>\r\n\r\n<p>Eleifend euismod pellentesque vel Phasellus enim magna, varius et commodo ut, ultricies vitae velit. Ut nulla tellus, sagittis vel justo. In libero urna, venenatis sit amet ornare non, suscipit nec risus. Sed consequat justo non mauris pretium at tempor justo sodales. Quisque tincidunt laoreet malesuada. Cum sociis natoque penatibus.</p>\r\n', 'sistem-operasi', 1),
(2, 1, 1, 1, 'Pengantar Teknologi Informasi', '<p>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'pengantar-teknologi-informasi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_rekening`
--

CREATE TABLE `ref_rekening` (
  `id_ref_rekening` int(11) NOT NULL,
  `nama_rekening` varchar(100) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `no_rekening` varchar(25) NOT NULL,
  `logo_rekening` varchar(225) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_rekening`
--

INSERT INTO `ref_rekening` (`id_ref_rekening`, `nama_rekening`, `nama_bank`, `no_rekening`, `logo_rekening`, `status`) VALUES
(1, 'Dandy Akhmarienno Putra', 'BCA', '0411024945', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_role`
--

CREATE TABLE `ref_role` (
  `id_ref_role` int(11) NOT NULL,
  `role` int(11) NOT NULL,
  `nama_role` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_role`
--

INSERT INTO `ref_role` (`id_ref_role`, `role`, `nama_role`) VALUES
(1, 1, 'Super Admin'),
(2, 2, 'Admin'),
(3, 3, 'Freelance'),
(4, 4, 'Tutor');

-- --------------------------------------------------------

--
-- Table structure for table `ref_semester`
--

CREATE TABLE `ref_semester` (
  `id_ref_semester` int(11) NOT NULL,
  `ref_semester` varchar(15) NOT NULL,
  `permalink` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_semester`
--

INSERT INTO `ref_semester` (`id_ref_semester`, `ref_semester`, `permalink`, `status`) VALUES
(1, 'Semester 1', 'semester-1', 0),
(2, 'Semester 2', 'semester-2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ref_sertifikat`
--

CREATE TABLE `ref_sertifikat` (
  `id_ref_sertifikat` int(11) NOT NULL,
  `nama_sertifikat` varchar(100) NOT NULL,
  `header_sertifikat` varchar(100) NOT NULL,
  `footer_sertifikat` varchar(100) NOT NULL,
  `background` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_sertifikat`
--

INSERT INTO `ref_sertifikat` (`id_ref_sertifikat`, `nama_sertifikat`, `header_sertifikat`, `footer_sertifikat`, `background`) VALUES
(2, 'Contoh Sertifikat', 'Contoh Sertifikat', 'Contoh Sertifikat', 'contoh-sertifikat.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `ref_slideshow`
--

CREATE TABLE `ref_slideshow` (
  `id_ref_slideshow` int(11) NOT NULL,
  `ref_slideshow` varchar(100) NOT NULL,
  `berkas` varchar(225) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_slideshow`
--

INSERT INTO `ref_slideshow` (`id_ref_slideshow`, `ref_slideshow`, `berkas`, `status`) VALUES
(1, 'Slide one', 'h2-1.jpg', 1),
(2, 'Slide two', 'h2-2.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_soal_essay`
--

CREATE TABLE `ref_soal_essay` (
  `id_ref_soal_essay` int(11) NOT NULL,
  `id_ref_kategori` int(11) NOT NULL,
  `id_ref_matkul` int(11) DEFAULT NULL,
  `id_ref_umum` int(11) DEFAULT NULL,
  `id_tentor` int(11) NOT NULL,
  `ref_soal_essay` text NOT NULL,
  `jawaban` text NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_soal_essay`
--

INSERT INTO `ref_soal_essay` (`id_ref_soal_essay`, `id_ref_kategori`, `id_ref_matkul`, `id_ref_umum`, `id_tentor`, `ref_soal_essay`, `jawaban`, `status`) VALUES
(1, 1, 1, NULL, 1, '<p>Sebutkan 5 contoh dari sistem operasi ...</p>\r\n', '<p>1. Windows</p>\r\n\r\n<p>2. Linux</p>\r\n\r\n<p>3. Ubuntu</p>\r\n\r\n<p>4. Mac Os</p>\r\n\r\n<p>5. Unknow</p>\r\n', 1),
(2, 1, 1, NULL, 1, '<p>Jika memiliki kerusakan pada sistem operasi, bagaimana anda memperbaikinya ?....</p>\r\n', '<p>unistall sistem operasi</p>\r\n', 1),
(3, 1, 2, NULL, 3, '<p>Jelaskan apa yang dimaksud dengan kecerdasan buatan artificial intelligence?</p>\r\n', '<p>Artificial intelligence atau kecerdasan buatan adalah suatu pengetahuan yang membuat komputer dapat &nbsp;meniru kecerdasan manusia sehingga diharapkan komputer (atau berupa suatu mesin) dapat melakukan hal-hal yang apabila dikerjakan manusia memerlukan kecerdasan &nbsp;yang mana tujuan dari IA &nbsp;tidak hanya membuat komputer dapat berfikir saja, tetapi bisa melihat, mendengar, berjalan, dan bermain. misalnya melakukan penalaran untuk mencapai suatu kesimpulan atau melakukan translasi dari sutu bahasa manusia ke bahan manusia yang lain.</p>\r\n', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_soal_ganda`
--

CREATE TABLE `ref_soal_ganda` (
  `id_ref_soal_ganda` int(11) NOT NULL,
  `id_ref_kategori` int(11) NOT NULL,
  `id_ref_matkul` int(11) DEFAULT NULL,
  `id_ref_umum` int(11) DEFAULT NULL,
  `ref_soal_ganda` text NOT NULL,
  `a` text NOT NULL,
  `b` text NOT NULL,
  `c` text NOT NULL,
  `d` text NOT NULL,
  `jawaban` varchar(100) NOT NULL,
  `gambar_soal` varchar(225) DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_soal_ganda`
--

INSERT INTO `ref_soal_ganda` (`id_ref_soal_ganda`, `id_ref_kategori`, `id_ref_matkul`, `id_ref_umum`, `ref_soal_ganda`, `a`, `b`, `c`, `d`, `jawaban`, `gambar_soal`, `status`) VALUES
(1, 1, 1, NULL, 'Suatu program perangkat lunak yang menjadi jembatan komunikasi dari software dan hardware dan membantu para user mengoperasikan komputernya disebut ?', '<p>Sistem Operasi</p>\r\n', '<p>Sistem Digital</p>\r\n', '<p>Sistem Informasi</p>\r\n', '<p>Sistem Pemograman Web</p>\r\n', 'a', NULL, 1),
(2, 1, 1, NULL, 'Versi pertama Microsoft Windows, yang disebut dengan Windows 1.0 dirilis pada tanggal ?', '<p>20 Januari 1984</p>\r\n', '<p>20 Januari 1983</p>\r\n', '<p>20 November 1985</p>\r\n', '<p>20 November 1986</p>\r\n', 'c', NULL, 1),
(3, 1, 1, NULL, 'Yang termasuk logo windows xp ialah ...', '<p><img alt=\"\" src=\"/kulin/assets/backend/kcfinder/upload/files/windows%2010.png\" style=\"height:40px; width:40px\" /></p>\r\n', '<p><img alt=\"\" src=\"/kulin/assets/backend/kcfinder/upload/files/windows%20xp.jpg\" style=\"height:48px; width:40px\" /></p>\r\n', '<p><img alt=\"\" src=\"/kulin/assets/backend/kcfinder/upload/files/windows%207.jpg\" style=\"height:40px; width:40px\" /></p>\r\n', '<p><img alt=\"\" src=\"/kulin/assets/backend/kcfinder/upload/files/windows%2011.png\" style=\"height:40px; width:40px\" /></p>\r\n', 'b', NULL, 1),
(4, 1, 2, NULL, 'Seperangkat komponen yang saling berhubungan dan saling bekerjasama untuk mencapai beberapa tujuan disebut…', '<p>Sistem</p>\r\n', '<p>Informasi</p>\r\n', '<p>Data</p>\r\n', '<p>Sistem Informasi</p>\r\n', 'd', NULL, 1),
(5, 2, NULL, 1, 'lagu in the end merupakan single song dari grup band ?', '<p>Linkin Park</p>\r\n', '<p>Greenday</p>\r\n', '<p>Mettalica</p>\r\n', '<p>Coldplay</p>\r\n', 'a', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_sosmed`
--

CREATE TABLE `ref_sosmed` (
  `id_ref_sosmed` int(11) NOT NULL,
  `id_ref_icon_sosmed` int(11) NOT NULL,
  `ref_sosmed` varchar(15) NOT NULL,
  `link_sosmed` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_sosmed`
--

INSERT INTO `ref_sosmed` (`id_ref_sosmed`, `id_ref_icon_sosmed`, `ref_sosmed`, `link_sosmed`, `status`) VALUES
(1, 1, 'Instagram', '#', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ref_umum`
--

CREATE TABLE `ref_umum` (
  `id_ref_umum` int(11) NOT NULL,
  `id_ref_kat_umum` int(11) NOT NULL,
  `ref_umum` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `link_umum` varchar(100) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ref_umum`
--

INSERT INTO `ref_umum` (`id_ref_umum`, `id_ref_kat_umum`, `ref_umum`, `deskripsi`, `link_umum`, `status`) VALUES
(1, 1, 'Musik Rock', '<p>-</p>\r\n', 'musik-rock', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rek_freelancer`
--

CREATE TABLE `rek_freelancer` (
  `id_rek_freelancer` int(11) NOT NULL,
  `id_freelancer` int(11) NOT NULL,
  `bank_freelancer` char(10) NOT NULL,
  `an_freelancer` varchar(100) NOT NULL,
  `rek_freelancer` varchar(25) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rek_freelancer`
--

INSERT INTO `rek_freelancer` (`id_rek_freelancer`, `id_freelancer`, `bank_freelancer`, `an_freelancer`, `rek_freelancer`, `status`) VALUES
(1, 1, 'BCA', 'Amanah', '0411024945', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rek_tentor`
--

CREATE TABLE `rek_tentor` (
  `id_rek_tentor` int(11) NOT NULL,
  `id_tentor` int(11) NOT NULL,
  `bank_tentor` varchar(15) NOT NULL,
  `an_tentor` varchar(100) NOT NULL,
  `rek_tentor` varchar(25) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `rek_tentor`
--

INSERT INTO `rek_tentor` (`id_rek_tentor`, `id_tentor`, `bank_tentor`, `an_tentor`, `rek_tentor`, `status`) VALUES
(1, 3, 'BCA', 'Pradmitha', '666', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tentor`
--

CREATE TABLE `tentor` (
  `id_tentor` int(11) NOT NULL,
  `id_ref_kategori` int(11) NOT NULL,
  `id_ref_matkul` int(11) DEFAULT NULL,
  `id_ref_umum` int(11) DEFAULT NULL,
  `nama_tentor` varchar(100) NOT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `no_telp` char(15) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` text DEFAULT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `photo` varchar(225) DEFAULT NULL,
  `role` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tentor`
--

INSERT INTO `tentor` (`id_tentor`, `id_ref_kategori`, `id_ref_matkul`, `id_ref_umum`, `nama_tentor`, `jenis_kelamin`, `no_telp`, `email`, `alamat`, `username`, `password`, `photo`, `role`) VALUES
(1, 1, 1, NULL, 'Audrey', 'Perempuan', '082233056769', 'audrey@gmail.com', 'Bandung', 'audrey', 'qwerty', 'drey.PNG', 4),
(2, 1, 1, NULL, 'cirilla', 'Perempuan', '082233056769', 'cirilla@gmail.com', 'Bandung', 'cirilla', 'qwerty', NULL, 4),
(3, 1, 2, NULL, 'Pradmitha', 'Perempuan', '082266034162', 'mitha@gmail.com', 'Yogyakarta', 'mitha', 'd8578edf8458ce06fbc5bb76a58c5ca4', NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id_video` int(11) NOT NULL,
  `id_freelancer` int(11) NOT NULL,
  `id_ref_kategori` int(11) NOT NULL,
  `id_ref_matkul` int(11) DEFAULT NULL,
  `id_ref_umum` int(11) DEFAULT NULL,
  `nama_video` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `thumbnail` varchar(100) NOT NULL,
  `berkas_file` varchar(225) NOT NULL,
  `harga_buy` int(11) NOT NULL,
  `harga_sell` int(11) DEFAULT NULL,
  `ukuran` char(10) NOT NULL,
  `status` int(1) DEFAULT 0,
  `aktif` int(11) NOT NULL,
  `views` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id_video`, `id_freelancer`, `id_ref_kategori`, `id_ref_matkul`, `id_ref_umum`, `nama_video`, `keterangan`, `thumbnail`, `berkas_file`, `harga_buy`, `harga_sell`, `ukuran`, `status`, `aktif`, `views`) VALUES
(1, 1, 1, 1, NULL, 'Test Video', '<p>Test Video</p>\r\n', 'logo_(5).png', 'azMNWOK_460sv.mp4', 1000000, NULL, '1895.98', 0, 1, 0),
(2, 1, 1, 1, NULL, 'Video Aja', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n', 'background1.jpg', 'DinasPPADProvinsiPapua1.mp4', 1000000, NULL, '16298.12', 0, 1, 0),
(3, 1, 1, 1, NULL, 'Video Nih', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n', '20161118010417.jpg', 'gTmXUE39zotl7aV9.mp4', 125000, NULL, '307.33', 0, 1, 0),
(4, 1, 1, 1, NULL, 'Video Ya', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\r\n', 'pantai_pandawa.jpg', 'kitaarisan.mp4', 300000, NULL, '40200.41', 0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `freelancer`
--
ALTER TABLE `freelancer`
  ADD PRIMARY KEY (`id_freelancer`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `operator`
--
ALTER TABLE `operator`
  ADD PRIMARY KEY (`id_operator`);

--
-- Indexes for table `parameter`
--
ALTER TABLE `parameter`
  ADD PRIMARY KEY (`param`);

--
-- Indexes for table `peserta`
--
ALTER TABLE `peserta`
  ADD PRIMARY KEY (`id_peserta`);

--
-- Indexes for table `ref_fakultas`
--
ALTER TABLE `ref_fakultas`
  ADD PRIMARY KEY (`id_ref_fakultas`);

--
-- Indexes for table `ref_icon_sosmed`
--
ALTER TABLE `ref_icon_sosmed`
  ADD PRIMARY KEY (`id_ref_icon_sosmed`);

--
-- Indexes for table `ref_jurusan`
--
ALTER TABLE `ref_jurusan`
  ADD PRIMARY KEY (`id_ref_jurusan`);

--
-- Indexes for table `ref_kategori`
--
ALTER TABLE `ref_kategori`
  ADD PRIMARY KEY (`id_ref_kategori`);

--
-- Indexes for table `ref_kategori_artikel`
--
ALTER TABLE `ref_kategori_artikel`
  ADD PRIMARY KEY (`id_kategori_artikel`);

--
-- Indexes for table `ref_kat_umum`
--
ALTER TABLE `ref_kat_umum`
  ADD PRIMARY KEY (`id_ref_kat_umum`);

--
-- Indexes for table `ref_matkul`
--
ALTER TABLE `ref_matkul`
  ADD PRIMARY KEY (`id_ref_matkul`);

--
-- Indexes for table `ref_rekening`
--
ALTER TABLE `ref_rekening`
  ADD PRIMARY KEY (`id_ref_rekening`);

--
-- Indexes for table `ref_role`
--
ALTER TABLE `ref_role`
  ADD PRIMARY KEY (`id_ref_role`);

--
-- Indexes for table `ref_semester`
--
ALTER TABLE `ref_semester`
  ADD PRIMARY KEY (`id_ref_semester`);

--
-- Indexes for table `ref_sertifikat`
--
ALTER TABLE `ref_sertifikat`
  ADD PRIMARY KEY (`id_ref_sertifikat`);

--
-- Indexes for table `ref_slideshow`
--
ALTER TABLE `ref_slideshow`
  ADD PRIMARY KEY (`id_ref_slideshow`);

--
-- Indexes for table `ref_soal_essay`
--
ALTER TABLE `ref_soal_essay`
  ADD PRIMARY KEY (`id_ref_soal_essay`);

--
-- Indexes for table `ref_soal_ganda`
--
ALTER TABLE `ref_soal_ganda`
  ADD PRIMARY KEY (`id_ref_soal_ganda`);

--
-- Indexes for table `ref_sosmed`
--
ALTER TABLE `ref_sosmed`
  ADD PRIMARY KEY (`id_ref_sosmed`);

--
-- Indexes for table `ref_umum`
--
ALTER TABLE `ref_umum`
  ADD PRIMARY KEY (`id_ref_umum`);

--
-- Indexes for table `rek_freelancer`
--
ALTER TABLE `rek_freelancer`
  ADD PRIMARY KEY (`id_rek_freelancer`);

--
-- Indexes for table `rek_tentor`
--
ALTER TABLE `rek_tentor`
  ADD PRIMARY KEY (`id_rek_tentor`);

--
-- Indexes for table `tentor`
--
ALTER TABLE `tentor`
  ADD PRIMARY KEY (`id_tentor`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id_video`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `freelancer`
--
ALTER TABLE `freelancer`
  MODIFY `id_freelancer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `operator`
--
ALTER TABLE `operator`
  MODIFY `id_operator` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `peserta`
--
ALTER TABLE `peserta`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `ref_fakultas`
--
ALTER TABLE `ref_fakultas`
  MODIFY `id_ref_fakultas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ref_icon_sosmed`
--
ALTER TABLE `ref_icon_sosmed`
  MODIFY `id_ref_icon_sosmed` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ref_jurusan`
--
ALTER TABLE `ref_jurusan`
  MODIFY `id_ref_jurusan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_kategori`
--
ALTER TABLE `ref_kategori`
  MODIFY `id_ref_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_kategori_artikel`
--
ALTER TABLE `ref_kategori_artikel`
  MODIFY `id_kategori_artikel` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_kat_umum`
--
ALTER TABLE `ref_kat_umum`
  MODIFY `id_ref_kat_umum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_matkul`
--
ALTER TABLE `ref_matkul`
  MODIFY `id_ref_matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_rekening`
--
ALTER TABLE `ref_rekening`
  MODIFY `id_ref_rekening` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_role`
--
ALTER TABLE `ref_role`
  MODIFY `id_ref_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ref_semester`
--
ALTER TABLE `ref_semester`
  MODIFY `id_ref_semester` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_sertifikat`
--
ALTER TABLE `ref_sertifikat`
  MODIFY `id_ref_sertifikat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_slideshow`
--
ALTER TABLE `ref_slideshow`
  MODIFY `id_ref_slideshow` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ref_soal_essay`
--
ALTER TABLE `ref_soal_essay`
  MODIFY `id_ref_soal_essay` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ref_soal_ganda`
--
ALTER TABLE `ref_soal_ganda`
  MODIFY `id_ref_soal_ganda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `ref_sosmed`
--
ALTER TABLE `ref_sosmed`
  MODIFY `id_ref_sosmed` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ref_umum`
--
ALTER TABLE `ref_umum`
  MODIFY `id_ref_umum` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `rek_freelancer`
--
ALTER TABLE `rek_freelancer`
  MODIFY `id_rek_freelancer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rek_tentor`
--
ALTER TABLE `rek_tentor`
  MODIFY `id_rek_tentor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id_video` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
