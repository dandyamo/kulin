<?php

class Model_api extends CI_Model
{

  function datagrabs($param) {
    $this->db->simple_query('SET SESSION group_concat_max_len=9999999');

    if (!empty($param['count'])) {

        if (!empty($param['search'])) {

            if (!empty($param['select'])) {
                $this->db->select($param['select'],false);
            }else{
                $this->db->select('*',false);
            }

        }else{
            $this->db->select($param['count'],false);
        }

    }elseif(!empty($param['select'])) {
        $this->db->select($param['select'],false);
    } 


    if (is_array($param['tabel'])) {    
        $n = 1;
        foreach($param['tabel'] as $tab => $on) {

            if ($n > 1) {
                if (is_array($on)) $this->db->join($tab,$on[0],$on[1]);
                else $this->db->join($tab,$on);
            } else { $this->db->from($tab); }
            $n+=1;
        }
    } else {
        $this->db->from($param['tabel']);
    }

    if (!empty($param['where'])) {
        foreach($param['where'] as $w => $an) {
// ini bikin error kalo empty jadi di ganti null
            if (is_null($an)){
                $this->db->where($w,null,false);
            }else {
                $this->db->where($w,$an);
            }
        }
    }
    // if (!empty($param['order'])) $this->db->order_by($param['order']);
    if (!empty($param['group_by'])) $this->db->group_by($param['group_by']);
//        $this->output->enable_profiler(TRUE);
    if (!empty($param['search'])) {
        // $q= $this->db->_compile_select(); 
        $q= $this->db->_compile_select(); 

        $this->db->_reset_select();
        if (!empty($param['count'])) {
            $this->db->select($param['count']." from ($q) t",false);
        }else{
            $this->db->select("* from ($q) t",false);	
        }


        foreach($param['search'] as $sc => $vl) {
            $this->db->or_like('t.'.$sc,$vl);
        }
        if (!empty($param['order'])) $this->db->order_by($param['order']);
    }else{
        if (!empty($param['order'])) $this->db->order_by($param['order']);
    }

    if (!empty($param['limit'])){
        if(!empty($param['offset'])){
            $this->db->limit($param['limit'],$param['offset']);
        }else{
            $this->db->limit($param['limit']);
        }
    }
    return $this->db->get();
}

  public function deleteData($id_admin)
  {
    $this->db->delete('tb_admin', ['id_admin' => $id_admin]);
    return $this->db->affected_rows();
  }

  public function createData($table,$data)
  {
    $this->db->insert($table,$data);
    return $this->db->affected_rows();
  }

  public function updateData($table,$data,$where)
  {
    $this->db->update($table, $data, $where);
    return $this->db->affected_rows();
  }
}
