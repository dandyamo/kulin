<?php


class Model_general extends CI_Model {

    function getdata($param) {
        #$this->db->cache_on();
        if (!empty($param['select'])) $this->db->select($param['select'],false);
        if (is_array($param['tabel'])) {    
            $n = 1;
            foreach($param['tabel'] as $tab => $on) {
    
                if ($n > 1) {
                    if (is_array($on)) $this->db->join($tab,@$on[0],@$on[1]);
                    else $this->db->join($tab,$on);
                } else { $this->db->from($tab); }
                $n++;
            }
        } else {
            $this->db->from($param['tabel']);
        }
        if (!empty($param['where'])) 
            foreach($param['where'] as $w => $an) {
    /*                if (!empty($an)) $this->db->where($w,$an);
                else $this->db->where($w,null,false);*/
    // ini bikin error kalo empty jadi di ganti null
                if (is_null($an)){
                    $this->db->where($w,null,false);
                }else {
                    $this->db->where($w,$an);
                }

            }
        
        if(!empty($param['not_in']))
        if(is_array($param['not_in'])){
            foreach($param['not_in'] as $w => $an) {
/*                if (!empty($an)) $this->db->where($w,$an);
                else $this->db->where($w,null,false);*/
// ini bikin error kalo empty jadi di ganti null
                if (is_null($an)){
                    $this->db->where_not_in($w,null,false);
                }else {
                    $this->db->where_not_in($w,$an);
                }
            }
        }else{
            $this->db->where_not_in($param['not_in'],$param['not_in_isi']);
        }
        
        if(!empty($param['in']))
        if(is_array($param['in'])){
            foreach($param['in'] as $w => $an) {
                if (is_null($an)){
                    $this->db->where_in($w,null,false);
                }else {
                    $this->db->where_in($w,$an);
                }
            }
         }else{
            $this->db->where_in($param['in'],$param['in_isi']);
         }
//ini tambahan yang nda harus pake offset
        if (!empty($param['limit']) && !empty($param['offset'])) $this->db->limit($param['limit'],$param['offset']);
        if (!empty($param['limit']) && empty($param['offset'])) $this->db->limit($param['limit']);
        if (!empty($param['order'])) $this->db->order_by($param['order']);
        if (!empty($param['search'])) {
            foreach($param['search'] as $sc => $vl) {
                $this->db->or_like($sc,$vl);
            }
        }
        if (!empty($param['group_by'])) $this->db->group_by($param['group_by']);
        return $this->db->get();
    }

    function save_data($tabel, $data = null, $column = null, $id = null){
    	if (is_array($tabel)) {
			if (!empty($tabel['where'])) {
    		$this->db->where($tabel['where']);
    		return $this->db->update($tabel['tabel'],$tabel['data']);
			} else {
				$this->db->insert($tabel['tabel'],$tabel['data']);
				return $this->db->insert_id();
			}
		} else {
			if (!empty($id)) {
				$this->db->where($column,$id);
				$this->db->update($tabel,$data);
			} else {
				$this->db->insert($tabel,$data);
				return $this->db->insert_id();	
			}    	
		}
    }

    function delete_data($tabel, $column = null, $id = null) {
		if (is_array($tabel)) {
			foreach($tabel['where'] as $w => $an) {
				if (is_null($an)) $this->db->where($w,null,false);
                else $this->db->where($w,$an);
			}
			return $this->db->delete($tabel['tabel']);
		} else {
			if (!empty($column)) { 
				if (is_array($column)) $this->db->where($column);
				else $this->db->where($column,$id);
			}
			return $this->db->delete($tabel);
		}
	}

    function combo_box($param) {
	    $combo=false;
        $data_combo =$this->getdata($param);

        if(@$param['pilih']!="-")$combo = array('' => !empty($param['pilih'])?$param['pilih']:'-- Pilih --');
//		$combo = array('' => !empty($param['pilih'])?$param['pilih']:'-- Pilih --');
        foreach($data_combo->result() as $row) {
			$valueb = array();
			foreach($param['val'] as $v) { 
				if (is_array($v)) {
					if ($v[0] == "(") $valueb[] = "(".$row->$v[1].")";
				} else {
					$valueb[] = $row->$v; 
				}
			}
			$keyb = array();
			if (is_array($param['key'])) 
				foreach($param['key'] as $k) { $keyb[] = (strlen($row->$k) > 100)?substr($row->$k,0,100).' ...':$row->$k; }
			$paramkey=$param['key'];
			$keyv = is_array($param['key']) ? implode("|",$keyb) : $row->$paramkey;
		
			$combo[$keyv] = implode(" ",$valueb);
		} return $combo;  
		
	}	

    function get_param($id = null,$t = null) {
		if (is_array($id)) {
			$this->db->where("param IN ('".implode("','",$id)."')",null,false);	
			$data = $this->db->get('parameter');
			$ret = array();
			foreach($data->result() as $d) { 
				if (!empty($t) and $t == 1) $ret[]= $d->param;
				if (!empty($t) and $t == 2) $ret[$d->param]= $d->val;
				else $ret[] = $d->val; 
			}
			return $ret;
		} else {
			$this->db->where('param',$id);
			$data = $this->db->get('parameter')->row();
			return !empty($data) ? $data->val : null;
		}	
	}

    function login($table,$where){
		return $this->db->get_where($table,$where);
	}

    public function getoperator ()
	{
		$this->db->select('*');
		$this->db->from('operator');
        $username = $this->session->userdata('username');
        $this->db->where('username', $username);
		$query = $this->db->get_where();
		return $query->row();
	}

    public function getfreelancer ()
	{
		$this->db->select('*');
		$this->db->from('freelancer');
        $username = $this->session->userdata('username');
        $this->db->where('username', $username);
		$query = $this->db->get_where();
		return $query->row();
	}

    public function gettentor ()
	{
		$this->db->select('*');
		$this->db->from('tentor');
		$this->db->join('ref_matkul','ref_matkul.id_ref_matkul=tentor.id_ref_matkul','left');
		$this->db->join('ref_umum','ref_umum.id_ref_umum=tentor.id_ref_umum','left');
        $username = $this->session->userdata('username');
        $this->db->where('username', $username);
		$query = $this->db->get_where();
		return $query->row();
	}

}
