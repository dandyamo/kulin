<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
/* Peserta */
$route['default_controller'] = 'home';
$route['about'] = 'home/about';
$route['category'] = 'home/category';
$route['umum'] = 'home/umum';
$route['category-list/(:any)'] = 'home/category_list/$1/$2';
$route['course-umum/(:any)'] = 'home/course_umum/$1/$2';
$route['course/(:any)'] = 'home/course/$1/$2';
$route['detail-course/(:any)'] = 'home/detail_course/$1/$2';
$route['detail-umum/(:any)'] = 'home/detail_umum/$1/$2';
/* Admin */
$route['cms'] = 'cms/login';
$route['cms/referensi/kategori'] = 'cms/kategori';
$route['cms/referensi/kategori/tambah'] = 'cms/kategori/tambah';
$route['cms/referensi/kategori/ubah/(:any)'] = 'cms/kategori/ubah/$1/$2';
$route['cms/referensi/kategori_artikel'] = 'cms/kategori_artikel';
$route['cms/referensi/kategori_artikel/tambah'] = 'cms/kategori_artikel/tambah';
$route['cms/referensi/kategori_artikel/ubah/(:any)'] = 'cms/kategori_artikel/ubah/$1/$2';
$route['cms/referensi/rekening'] = 'cms/rekening';
$route['cms/referensi/rekening/tambah'] = 'cms/rekening/tambah';
$route['cms/referensi/rekening/ubah/(:any)'] = 'cms/rekening/ubah/$1/$2';
$route['cms/referensi/fakultas'] = 'cms/fakultas';
$route['cms/referensi/fakultas/tambah'] = 'cms/fakultas/tambah';
$route['cms/referensi/fakultas/ubah/(:any)'] = 'cms/fakultas/ubah/$1/$2';
$route['cms/referensi/jurusan'] = 'cms/jurusan';
$route['cms/referensi/jurusan/tambah'] = 'cms/jurusan/tambah';
$route['cms/referensi/jurusan/ubah/(:any)'] = 'cms/jurusan/ubah/$1/$2';
$route['cms/referensi/semester'] = 'cms/semester';
$route['cms/referensi/semester/tambah'] = 'cms/semester/tambah';
$route['cms/referensi/semester/ubah/(:any)'] = 'cms/semester/ubah/$1/$2';
$route['cms/referensi/matkul'] = 'cms/matkul';
$route['cms/referensi/matkul/tambah'] = 'cms/matkul/tambah';
$route['cms/referensi/matkul/ubah/(:any)'] = 'cms/matkul/ubah/$1/$2';
$route['cms/referensi/sertifikat'] = 'cms/sertifikat';
$route['cms/referensi/sertifikat/tambah'] = 'cms/sertifikat/tambah';
$route['cms/referensi/sertifikat/ubah/(:any)'] = 'cms/sertifikat/ubah/$1/$2';
$route['cms/referensi/sertifikat/lihat_data/(:any)'] = 'cms/sertifikat/lihat_data/$1/$2';
$route['cms/referensi/kurmum'] = 'cms/kurmum';
$route['cms/referensi/kurmum/tambah'] = 'cms/kurmum/tambah';
$route['cms/referensi/kurmum/ubah/(:any)'] = 'cms/kurmum/ubah/$1/$2';
$route['cms/referensi/kurmum/lihat_data/(:any)'] = 'cms/kurmum/lihat_data/$1/$2';
$route['cms/data/operator'] = 'cms/operator';
$route['cms/data/operator/tambah'] = 'cms/operator/tambah';
$route['cms/data/operator/ubah/(:any)'] = 'cms/operator/ubah/$1/$2';
$route['cms/data/freelancer'] = 'cms/freelancer';
$route['cms/data/freelancer/tambah'] = 'cms/freelancer/tambah';
$route['cms/data/freelancer/ubah/(:any)'] = 'cms/freelancer/ubah/$1/$2';
$route['cms/data/peserta'] = 'cms/peserta';
$route['cms/data/peserta/tambah'] = 'cms/peserta/tambah';
$route['cms/data/peserta/ubah/(:any)'] = 'cms/peserta/ubah/$1/$2';
$route['cms/data/tentor'] = 'cms/tentor';
$route['cms/data/tentor/tambah'] = 'cms/tentor/tambah';
$route['cms/data/tentor/ubah/(:any)'] = 'cms/tentor/ubah/$1/$2';
$route['cms/data/tentor/list_data/(:any)'] = 'cms/tentor/list_data/$1/$2';
$route['cms/data/video'] = 'cms/video';
$route['cms/data/video/detail/(:any)'] = 'cms/video/detail/$1/$2';
$route['cms/data/video/konfirmasi/(:any)'] = 'cms/video/konfirmasi/$1/$2';
$route['cms/data/video/list_data/(:any)'] = 'cms/video/list_data/$1/$2';
$route['cms/transaksi/pembayaran'] = 'cms/pembayaran';
$route['cms/tampilan/slideshow'] = 'cms/slideshow';
$route['cms/tampilan/slideshow/tambah'] = 'cms/slideshow/tambah';
$route['cms/tampilan/slideshow/ubah/(:any)'] = 'cms/slideshow/ubah/$1/$2';
$route['cms/tampilan/sosmed'] = 'cms/sosmed';
$route['cms/tampilan/sosmed/tambah'] = 'cms/sosmed/tambah';
$route['cms/tampilan/sosmed/ubah/(:any)'] = 'cms/sosmed/ubah/$1/$2';
$route['cms/soal/ganda'] = 'cms/ganda';
$route['cms/soal/ganda/tambah/(:any)'] = 'cms/ganda/tambah/$1/$2';
$route['cms/soal/ganda/ubah/(:any)/(:any)/(:any)'] = 'cms/ganda/ubah/$1/$2/$3';
$route['cms/soal/ganda/detail_soal/(:any)/(:any)'] = 'cms/ganda/detail_soal/$1/$2';
$route['cms/soal/ganda/demo/(:any)/(:any)'] = 'cms/ganda/demo/$1/$2';
$route['cms/soal/ganda/list_data/(:any)'] = 'cms/ganda/list_data/$1/$2';
$route['cms/tampilan/umum'] = 'cms/umum';
$route['cms/soal/essay'] = 'cms/essay';
$route['cms/soal/essay/detail_soal/(:any)/(:any)'] = 'cms/essay/detail_soal/$1/$2';
$route['cms/soal/essay/demo/(:any)/(:any)'] = 'cms/essay/demo/$1/$2';
$route['cms/soal/essay/list_data/(:any)'] = 'cms/essay/list_data/$1/$2';

/* Freelancer */
$route['freelancer'] = 'freelancer/login';

/* tentor */
$route['tentor'] = 'tentor/login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
