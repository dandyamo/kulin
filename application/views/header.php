<?php 
 $st = app_param();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $title; ?> - Kulin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote-bs4.css">
  <!-- Ekko Lightbox -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/ekko-lightbox/ekko-lightbox.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

  <?php echo @$style; ?>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <?php
      $path_inst_logo = !empty($st['logo']) ? FCPATH.'uploads/brands/'.$st['logo'] : null;
      $logo_instansi = (file_exists($path_inst_logo) and !empty($st['logo'])) ? base_url().'uploads/brands/'.$st['logo'] : base_url().'assets/logo/brand.png';
      ?>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link"><img src="<?php echo $logo_instansi ?>" style="width: 70px;"></a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item">
        <?php if($this->session->userdata('role') == 1) { ?>
          <a class="nav-link" href="<?php echo base_url(); ?>cms/profil">
            Profil
          </a>
        <?php } else if($this->session->userdata('role') == 2) { ?>
          <a class="nav-link" href="<?php echo base_url(); ?>cms/profil">
            Profil
          </a>
        <?php } else if($this->session->userdata('role') == 3) { ?>
          <a class="nav-link" href="<?php echo base_url(); ?>freelancer/profil">
            Profil
          </a>
        <?php } else if($this->session->userdata('role') == 4) { ?>
          <a class="nav-link" href="<?php echo base_url(); ?>tentor/profil">
            Profil
          </a>
        <?php } ?>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#modal-default">
          <i class="fa fa-power-off"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url(); ?>cms/home" class="brand-link">
      <?php if($this->session->userdata('role') == 1) { ?>
        <span class="brand-text font-weight-light"><center>Super Administrator</center></span>
        <?php } else if($this->session->userdata('role') == 2) { ?>
          <span class="brand-text font-weight-light"><center>Administrator</center></span>
        <?php } else if($this->session->userdata('role') == 3) { ?>
          <span class="brand-text font-weight-light"><center>Freelancer</center></span>
        <?php } else if($this->session->userdata('role') == 4) { ?>
          <span class="brand-text font-weight-light"><center>Tentor</center></span>
        <?php } ?>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <?php 
          $ava = !empty($this->session->userdata('foto')) ? ''.base_url().'uploads/photo/'.$this->session->userdata('foto').'' : ''.base_url().'assets/backend/dist/img/user.png';
          ?>
          <?php if($this->session->userdata('role') == 3) { ?>

          <img src="<?php echo $ava; ?>" class="img-circle elevation-2" alt="User Image">
              
          <?php } else if($this->session->userdata('role') == 4) { ?>

          <img src="<?php echo $ava; ?>" class="img-circle elevation-2" alt="User Image">

          <?php } else { ?>

          <img src="<?php echo $ava; ?>" class="img-circle elevation-2" alt="User Image">

          <?php } ?>
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('nama'); ?></a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <?php $this->load->view('menu'); ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
