<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                    <?php echo @$detail; ?>
            </div>
            <div class="card-footer">
                <a href="<?php echo $back; ?>" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
        <!-- /.card-body -->
        </div>
        <!-- /.card -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->