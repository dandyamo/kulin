<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
          <span class="info-box-icon bg-info"><i class="far fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Peserta</span>
            <span class="info-box-number"><?php echo $peserta; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
          <span class="info-box-icon bg-success"><i class="far fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Freelance</span>
            <span class="info-box-number"><?php echo $freelancer; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
          <span class="info-box-icon bg-warning"><i class="far fa-user"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Tentor</span>
            <span class="info-box-number"><?php echo $tentor; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="small-box bg-info">
              <div class="inner">
                <h3><?php echo $tot_video; ?></h3>

                <p>Total Video</p>
              </div>
              <div class="icon">
                <i class="fa fa-video"></i>
              </div>
            </div>
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>Ke Halaman <br>Website</h3>
              </div>
              <div class="icon">
                <i class="fa fa-video"></i>
              </div>
              <a href="<?php echo base_url(); ?>" target="_blank" class="small-box-footer"> Tampilkan website <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-8">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Jadwal Kursus Bulan ini</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 300px;">
              <table class="table">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Tanggal</th>
                    <th>Nama Peserta</th>
                    <th>Kursus</th>
                    <th>Nama Tentor</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>
                  <tr style="background:#00FF00">
                    <td>1</td>
                    <td>11-7-2014</td>
                    <td>John Doe</td>
                    <td>John Doe</td>
                    <td>John Doe</td>
                    <td><a href="#" class="btn btn-xs btn-default">Berlangsung</a></td>
                  </tr>
                  <tr style="background:#F0E68C">
                    <td>2</td>
                    <td>11-7-2014</td>
                    <td>John Doe</td>
                    <td>John Doe</td>
                    <td>John Doe</td>
                    <td><a href="#" class="btn btn-xs btn-default">Ingatkan</a></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>
  </div>
</div>