<?php
 $x = $this->uri->segment(5);
 

 if($x == 1) {

    $aktif = 'active';
    $aktiff = '';

 } else if($x == 2) {

    $aktif = '';
    $aktiff = 'active';

 } else {
    $aktif = 'active';
    $aktiff = '';
 }
?>
<div class="row">
    <div class="col-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
            <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                <li class="nav-item">
                <a class="nav-link <?php echo @$aktif; ?>" id="univesitas" href="<?php echo site_url(); ?>cms/data/tentor/list_data/1">Universitas</a>
                </li>
                <li class="nav-item">
                <a class="nav-link <?php echo @$aktiff; ?>" id="umum" href="<?php echo site_url(); ?>cms/data/tentor/list_data/2">Umum</a>
                </li>
            </ul>
            </div>
            <div class="card-body">
            <div class="tab-content" id="custom-tabs-four-tabContent">
                <div class="tab-pane fade active show" id="tab-universitas" role="tabpanel" aria-labelledby="univesitas">
                <div class="row">
                    <div class="col-3 mb-3">
                        <?php echo @$tombol; ?>
                    </div>
                </div>
                    <?php echo @$table; ?>
                </div>
                <div class="tab-pane fade" id="tab-umum" role="tabpanel" aria-labelledby="umum">
                    <div class="row">
                        <div class="col-3 mb-3">
                            <?php echo @$tombol; ?>
                        </div>
                    </div>
                    <?php echo @$table; ?>
                </div>
            </div>
            </div>
            <!-- /.card -->
        </div>
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->