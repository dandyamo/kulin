<div class="row">
<div class="col-md-3">

<!-- Profile Image -->
<div class="card card-primary card-outline">
    <div class="card-body box-profile">
    <div class="text-center">
        <?php if($this->session->userdata('role') == 3) { ?>

            <img class="profile-user-img img-fluid img-circle"
                src="<?php echo $ava; ?>"
                alt="User profile picture">
                
        <?php } else if($this->session->userdata('role') == 4) { ?>

            <img class="profile-user-img img-fluid img-circle"
                src="<?php echo $ava; ?>"
                alt="User profile picture">

        <?php } else { ?>
            
            <img class="profile-user-img img-fluid img-circle"
                src="<?php echo $ava; ?>"
                alt="User profile picture">

        <?php } ?>
    </div>

    <h3 class="profile-username text-center"><?php echo $this->session->userdata('nama'); ?></h3>

    <?php if($this->session->userdata('role') == 1) { ?>
        <p class="text-muted text-center">Super Administrator</p>
    <?php } else if($this->session->userdata('role') == 2) { ?>
        <p class="text-muted text-center">Administrator</p>
    <?php } else if($this->session->userdata('role') == 3) { ?>
        <p class="text-muted text-center">Freelancer</p>
    <?php } else if($this->session->userdata('role') == 4) { ?>
        <p class="text-muted text-center">Tentor</p>
    <?php } ?>

    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->

<?php echo $data; ?>