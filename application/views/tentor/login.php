<?php 
 $st = app_param();
?>
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Tentor - Kulin</title>
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/login/style.css" />

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

</head>

<body>
  <div class="container">
    <div class="container__forms">
      <div class="form">
        <form action="<?php echo base_url('tentor/login/aksi_login'); ?>" class="form__sign-in" method="post">
          <h2 class="form__title">Masuk</h2>
          <div class="form__input-field">
            <i class="fas fa-user"></i>
            <input type="text" name="username" placeholder="Username" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" placeholder="Password" required />
          </div>
          <input class="form__submit" type="submit" value="Masuk" />
          <p class="form__social-text">Belum mempunyai akun? <a href="#" id="sign-up-btn">Daftar</a></p>
        </form>

        <form action="<?php echo base_url('tentor/login/aksi_register'); ?>" class="form__sign-up" method="post">
          <h2 class="form__title">Daftar</h2>
          <div class="form__input-field">
            <i class="fas fa-address-card"></i>
            <input type="text" name="nama_tentor" placeholder="Nama Lengkap" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-user"></i>
            <input type="text" name="username" placeholder="Username" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-envelope"></i>
            <input type="text" name="email" placeholder="Email" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" placeholder="Password" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-list-alt"></i>
            <select type="text" name="id_ref_kategori" id="id_ref_kategori" style="border-radius:30px; background-color: #f0f0f0; border-color: #f0f0f0;" required>
              <option value="">Pilih Kategori</option>
              <?php foreach($ref_kategori->result() as $row){ ?>
                <option value="<?php echo $row->id_ref_kategori; ?>"><?php echo $row->ref_kategori; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form__input-field">
            <i class="fas fa-book"></i>
            <select type="text" name="id_kursus" id="id_kursus" style="border-radius:30px; background-color: #f0f0f0; border-color: #f0f0f0;" required>
              <option value="">Pilih Kursus</option>
            </select>
          </div>

          <input class="form__submit" type="submit" value="Daftar" />

          <p class="form__social-text">Sudah mempunyai akun? <a href="#" id="sign-in-btn">Masuk</a></p>
        </form>
      </div>
    </div>
    <div class="container__panels">
      <div class="panel panel__left">
        <?php
          $path_inst_logo = !empty($st['logo']) ? FCPATH.'uploads/brands/'.$st['logo'] : null;
          $logo_instansi = (file_exists($path_inst_logo) and !empty($st['logo'])) ? base_url().'uploads/brands/'.$st['logo'] : base_url().'assets/logo/brand.png';
        ?>
        <!-- Brand Logo -->
        <a href="<?php echo base_url(); ?>">
          <img class="panel__image" src="<?php echo $logo_instansi ?>"">
        </a>
      </div>
      <div class="panel panel__right">
      <a href="<?php echo base_url(); ?>">
          <img class="panel__image" src="<?php echo $logo_instansi ?>"">
        </a>
      </div>
    </div>
  </div>

  
  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>assets/frontend/jquery-3.3.1.js"></script>    
  <script src="https://kit.fontawesome.com/1788c719dd.js" crossorigin="anonymous"></script>

<script>
    const signInBtn = document.querySelector("#sign-in-btn");
    const signUpBtn = document.querySelector("#sign-up-btn");
    const container = document.querySelector(".container");

    signUpBtn.addEventListener("click", () => {
      container.classList.add("sign-up-mode");
    });

    signInBtn.addEventListener("click", () => {
      container.classList.remove("sign-up-mode");
    });

</script>

<script type="text/javascript">
    $(document).ready(function(){
        $('#id_ref_kategori').change(function(){
            var id=$(this).val();

          if(id == 1){

            $.ajax({

            url : "<?php echo site_url();?>tentor/login/get_matkul",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'JSON',
            success: function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i].id_ref_matkul+'">'+data[i].ref_matkul+'</option>';
                }
                $('#id_kursus').html(html);
                
            }

            });

            } else {

              $.ajax({

              url : "<?php echo site_url();?>tentor/login/get_umum",
              method : "POST",
              data : {id: id},
              async : false,
              dataType : 'JSON',
              success: function(data){
                  var html = '';
                  var i;
                  for(i=0; i<data.length; i++){
                      html += '<option value="'+data[i].id_ref_umum+'">'+data[i].ref_umum+'</option>';
                  }
                  $('#id_kursus').html(html);
                  
              }

              });

            }
           
        });
    });
</script>
</body>