<div class="row">
    <!-- left column -->
    <div class="col-md-12">
        <!-- general form elements -->
        <div class="card">
            <!-- form start -->
            <form action="<?php echo $link; ?>" method="post" enctype="multipart/form-data">
                <?php echo $form_data; ?>

            <div class="card-footer">
                <button type="submit" class="btn btn-sm btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                <a href="<?php echo $back; ?>" class="btn btn-sm btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
            </div>
            </form>
        </div>
        <!-- /.card -->
    </div>
</div>