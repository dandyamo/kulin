<?php
$a = $this->uri->segment(1);
$b = $this->uri->segment(2);
$c = $this->uri->segment(3);
$d = $this->uri->segment(4);

?>

<?php if($this->session->userdata('role') == 1) { ?>

<li class="nav-item">
    <a href="<?php echo site_url(); ?>cms/home" class="nav-link">
        <i class="nav-icon fas fa-home"></i>
        <p>
        Beranda
        </p>
    </a>
</li> 
<li class="nav-item">
    <a href="<?php echo site_url(); ?>cms/artikel" <?php if ($b == "artikel")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-file-alt"></i>
        <p>
        Artikel
        </p>
    </a>
</li>  
<li class="nav-item <?php if ($b == "data") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-list-alt"></i>
        <p>
        Data
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/freelancer" <?php if ($c == "freelancer")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Freelancer
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/tentor" <?php if ($c == "tentor")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Tentor
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/peserta" <?php if ($c == "peserta")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Peserta
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/operator" <?php if ($c == "operator")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Operator
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/video" <?php if ($c == "video")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Video
                </p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item <?php if ($b == "soal") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fa fa-file-alt"></i>
        <p>
        Soal
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/soal/ganda" <?php if ($c == "ganda")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Soal Pilihan Ganda</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/soal/essay" <?php if ($c == "essay")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Soal Essay</p>
        </a>
        </li>
    </ul>
</li>
<li class="nav-item <?php if ($b == "laporan") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-file-alt"></i>
        <p>
        Laporan
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">

    </ul>
</li>
<li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/rekening" <?php if ($c == "rekening")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-id-card nav-icon"></i>
            <p>Rekening</p>
        </a>
</li>

<?php } else if($this->session->userdata('role') == 2) { ?>

<li class="nav-item">
    <a href="<?php echo site_url(); ?>cms/home" class="nav-link">
        <i class="nav-icon fas fa-home"></i>
        <p>
        Beranda
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>cms/artikel" <?php if ($b == "artikel")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-file-alt"></i>
        <p>
        Artikel
        </p>
    </a>
</li>
<li class="nav-item <?php if ($b == "data") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-list-alt"></i>
        <p>
        Data
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/freelancer" <?php if ($c == "freelancer")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Freelancer
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/peserta" <?php if ($c == "peserta")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Peserta
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/tentor" <?php if ($c == "tentor")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Tentor
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="<?php echo site_url(); ?>cms/data/video" <?php if ($c == "video")  echo 'class="nav-link active"' ?> class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Video
                </p>
            </a>
        </li>
    </ul>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>cms/jadwal" <?php if ($b == "jadwal")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-clock"></i>
        <p>
        Jadwal
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>cms/linkzoom" <?php if ($b == "linkzoom")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-video"></i>
        <p>
        Link Zoom
        </p>
    </a>
</li>
<li class="nav-item <?php if ($b == "referensi") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-cog"></i>
        <p>
        Referensi
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/fakultas" <?php if ($c == "fakultas")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Fakultas</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/jurusan" <?php if ($c == "jurusan")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Jurusan</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/semester" <?php if ($c == "semester")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Semester</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/kategori" <?php if ($c == "kategori")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Kategori</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/matkul" <?php if ($c == "matkul")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Mata Kuliah</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/rekening" <?php if ($c == "rekening")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Rekening</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/sertifikat" <?php if ($c == "sertifikat")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Sertifikat</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/kurmum" <?php if ($c == "kurmum")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Kursus Umum</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/referensi/kategori_artikel" <?php if ($c == "kategori_artikel")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Kategori Artikel</p>
        </a>
        </li>
    </ul>
</li>
<li class="nav-item <?php if ($b == "soal") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fa fa-file-alt"></i>
        <p>
        Soal
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/soal/ganda" <?php if ($c == "ganda")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Soal Pilihan Ganda</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/soal/essay" <?php if ($c == "essay")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Soal Essay</p>
        </a>
        </li>
    </ul>
</li>
<li class="nav-item <?php if ($b == "tampilan") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fa fa-desktop"></i>
        <p>
        Tampilan
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/tampilan/slideshow" <?php if ($c == "slideshow")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Slideshow</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/tampilan/sosmed" <?php if ($c == "sosmed")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Sosial Media</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/tampilan/umum" <?php if ($c == "umum")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Umum</p>
        </a>
        </li>
    </ul>
</li>
<li class="nav-item <?php if ($b == "transaksi") echo 'menu-open' ?>">
    <a href="#" class="nav-link">
        <i class="nav-icon fas fa-tags"></i>
        <p>
        Transaksi
        <i class="fas fa-angle-left right"></i>
        </p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="<?php echo site_url(); ?>cms/transaksi/pembayaran" <?php if ($c == "pembayaran")  echo 'class="nav-link active"' ?> class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Pembayaran</p>
        </a>
        </li>
    </ul>
</li>

<?php } else if ($this->session->userdata('role') == 3) { ?>

<li class="nav-item">
    <a href="<?php echo site_url(); ?>freelancer/home" class="nav-link">
        <i class="nav-icon fas fa-home"></i>
        <p>
        Beranda
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>freelancer/video" <?php if ($b == "video")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-video"></i>
        <p>
        Video
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>freelancer/pendapatan" <?php if ($b == "pendapatan")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-money-bill"></i>
        <p>
        Pendapatan
        </p>
    </a>
</li>

<?php } else if($this->session->userdata('role') == 4) { ?>

<li class="nav-item">
    <a href="<?php echo site_url(); ?>tentor/home" class="nav-link">
        <i class="nav-icon fas fa-home"></i>
        <p>
        Beranda
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>tentor/jadwal" <?php if ($b == "jadwal")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-clock"></i>
        <p>
        Jadwal
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>tentor/soal" <?php if ($b == "soal")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-file-alt"></i>
        <p>
        Soal Test
        </p>
    </a>
</li>
<li class="nav-item">
    <a href="<?php echo site_url(); ?>tentor/pendapatan" <?php if ($b == "pendapatan")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-money-bill"></i>
        <p>
        Pendapatan
        </p>
    </a>
</li>

<li class="nav-item">
    <a href="<?php echo site_url(); ?>tentor/pengaturan_jadwal" <?php if ($b == "pengaturan_jadwal")  echo 'class="nav-link active"' ?> class="nav-link">
        <i class="nav-icon fa fa-calendar"></i>
        <p>
        Setting Jadwal
        </p>
    </a>
</li>

<?php } ?>