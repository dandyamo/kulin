<div class="row">
  <div class="col-lg-12">
    <div class="row">
      <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
          <span class="info-box-icon bg-info"><i class="fa fa-video"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Total Video</span>
            <span class="info-box-number"><?php echo $tot_video; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
          <span class="info-box-icon bg-success"><i class="fa fa-video"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Video Aktif</span>
            <span class="info-box-number"><?php echo $video_aktif; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
      <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
          <span class="info-box-icon bg-danger"><i class="fa fa-video"></i></span>

          <div class="info-box-content">
            <span class="info-box-text">Video Pending</span>
            <span class="info-box-number"><?php echo $video_pending; ?></span>
          </div>
          <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
      </div>
      <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-lg-4">
            <div class="small-box bg-info">
              <div class="inner">
                <h3>150</h3>

                <p>Total Pendapatan</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>

            <div class="small-box bg-success">
              <div class="inner">
                <h3>150</h3>

                <p>Pendapatan Bulan <?php echo date('F'); ?></p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
            </div>
        </div>
        <div class="col-8">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Video Universitas</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 300px;">
              <table class="table">
                <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Nama Video</th>
                    <th>Fakultas</th>
                    <th>Jurusan</th>
                    <th>Mata Kuliah</th>
                  </tr>
                </thead>
                <tbody>
                    <?php echo $video; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Video Umum</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0" style="height: 300px;">
              <table class="table">
                <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Nama Video</th>
                    <th>Kursus Umum</th>
                  </tr>
                </thead>
                <tbody>
                    <?php echo $video_umum; ?>
                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
    </div>
  </div>
</div>