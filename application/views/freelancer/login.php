<?php 
 $st = app_param();
?>
<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Freelancer - Kulin</title>
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200;300;400;500;600;700;800&display=swap" rel="stylesheet" />
  <script src="https://kit.fontawesome.com/1788c719dd.js" crossorigin="anonymous"></script>

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/backend/login/style.css" />
</head>

<body>
  <div class="container">
    <div class="container__forms">
      <div class="form">
        <form action="<?php echo base_url('freelancer/login/aksi_login'); ?>" class="form__sign-in" method="post">
          <h2 class="form__title">Masuk</h2>
          <div class="form__input-field">
            <i class="fas fa-user"></i>
            <input type="text" name="username" placeholder="Username" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-lock"></i>
            <input type="password" name="password" placeholder="Password" required />
          </div>
          <input class="form__submit" type="submit" value="Masuk" />
          <p class="form__social-text">Belum mempunyai akun? <a href="#" id="sign-up-btn">Daftar</a></p>
        </form>

        <form action="" class="form__sign-up">
          <h2 class="form__title">Daftar</h2>
          <div class="form__input-field">
            <i class="fas fa-user"></i>
            <input type="text" placeholder="Username" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-envelope"></i>
            <input type="text" placeholder="Email" required />
          </div>
          <div class="form__input-field">
            <i class="fas fa-lock"></i>
            <input type="password" placeholder="Password" required />
          </div>

          <input class="form__submit" type="submit" value="Daftar" />

          <p class="form__social-text">Sudah mempunyai akun? <a href="#" id="sign-in-btn">Masuk</a></p>
        </form>
      </div>
    </div>
    <div class="container__panels">
      <div class="panel panel__left">
        <?php
          $path_inst_logo = !empty($st['logo']) ? FCPATH.'uploads/brands/'.$st['logo'] : null;
          $logo_instansi = (file_exists($path_inst_logo) and !empty($st['logo'])) ? base_url().'uploads/brands/'.$st['logo'] : base_url().'assets/logo/brand.png';
        ?>
        <!-- Brand Logo -->
        <a href="<?php echo base_url(); ?>">
          <img class="panel__image" src="<?php echo $logo_instansi ?>"">
        </a>
      </div>
      <div class="panel panel__right">
      <a href="<?php echo base_url(); ?>">
          <img class="panel__image" src="<?php echo $logo_instansi ?>"">
        </a>
      </div>
    </div>
  </div>


<script>
    const signInBtn = document.querySelector("#sign-in-btn");
    const signUpBtn = document.querySelector("#sign-up-btn");
    const container = document.querySelector(".container");

    signUpBtn.addEventListener("click", () => {
      container.classList.add("sign-up-mode");
    });

    signInBtn.addEventListener("click", () => {
      container.classList.remove("sign-up-mode");
    });

</script>
</body>