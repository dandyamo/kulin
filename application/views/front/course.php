<!-- Breadcrumbs Start -->
<div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/breadcrumbs/2.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title"><?php echo $title; ?></h1>
                    <ul>
                        <li>
                            <a class="active" href="index.php">Home</a>
                        </li>
                        <li>Course Category</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->

            <!-- Popular Courses Section Start -->
            <div id="rs-popular-courses" class="rs-popular-courses style1 orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                <div class="container">
                    <div class="gridFilter text-center mb-50">
                        <button class="active" data-filter="*">All</button>
                        <button data-filter=".filter1">Semester 1</button>
                        <button data-filter=".filter2">Semester 2</button>
                        <button data-filter=".filter3">Semester 3</button>
                        <button data-filter=".filter4">Semester 4</button>
                        <button data-filter=".filter4">Semester 5</button>
                        <button data-filter=".filter4">Semester 6</button>
                    </div>
                    <div class="row grid">
                        <?php echo $course; ?>
                    </div>
                    <div class="pagination-area orange-color text-center mt-30 md-mt-0">
                        <ul class="pagination-part">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">Next <i class="fa fa-long-arrow-right"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Popular Courses Section End -->

          
        </div> 
        <!-- Main content End --> 