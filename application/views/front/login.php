<!-- Main content Start -->
<div class="main-content">
    <!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
        
        <div class="breadcrumbs-text white-color">
            <h1 class="page-title">My Account</h1>
            <ul>
                <li>
                    <a class="active" href="index.html">Home</a>
                </li>
                <li>My Account</li>
            </ul>
        </div>
    </div>
    <!-- Breadcrumbs End -->            

    <!-- My Account Section Start -->
    <div class="rs-login pt-100 pb-100 md-pt-70 md-pb-70">
        <div class="container">
            <div class="noticed">
                <div class="main-part">                           
                    <div class="method-account">
                        <h2 class="login">Masuk</h2>
                        <form action="<?php echo site_url(); ?>login/action_login" method="post">
                            <input type="text" name="username" placeholder="Username" required>
                            <input type="password" name="password" placeholder="Password" required>
                            <button type="submit" class="readon submit-btn">Masuk</button>
                            <div class="last-password">
                                <p>Tidak terdaftar? <a href="<?php echo site_url(); ?>register">Buat akun sekarang</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- My Account Section End -->  

</div> 
<!-- Main content End -->