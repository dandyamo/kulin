<!-- Breadcrumbs Start -->

<!-- Breadcrumbs End -->            

<!-- Register Section -->
<section class="register-section pt-50 pb-50">
    <div class="container">
        <div class="register-box">
            
            <div class="sec-title text-center mb-30">
                <h2 class="title mb-10">Buat Akun Baru</h2>
            </div>
            
            <!-- Login Form -->
            <div class="styled-form">
                <div id="form-messages"></div>
                <form method="post" action="<?php echo site_url(); ?>register/action_register">                               
                    <div class="row clearfix">                                    
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="nama" name="nama_peserta" placeholder="Nama Lengkap" required>
                        </div>
                        
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <select class="form-control" name="jenis_kelamin">
                                <option value="" selected disabled hidden>Jenis Kelamin</option>
                                <option>Laki</option>
                                <option>Perempuan</option>
                            </select>
                        </div>
                    
                            <!-- Form Group -->
                            <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="nomor" name="no_telp" placeholder="Nomor Telephone " required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="email" id="email" name="email" placeholder="Alamat Email" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="alamat" name="alamat" placeholder="Alamat" required>
                        </div>
                        
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="user" name="username" placeholder="Username" required>
                        </div>    
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="puser" name="password" placeholder="Password" required>
                        </div>    
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-35">
                            <input type="text" id="Confirm" name="conf_password" placeholder="Confirm Password" required>
                        </div>
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-center mb-25">
                            <button type="submit" class="readon register-btn"><span class="txt">Daftar</span></button>
                        </div>
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <div class="users">Sudah memiliki akun? <a href="<?php echo site_url(); ?>login">Masuk</a></div>
                        </div>
                        
                    </div>
                    
                </form>
            </div>
            
        </div>
    </div>
</section>
<!-- End Login Section --> 

</div> 
<!-- Main content End -->