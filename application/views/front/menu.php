<nav class="rs-menu">
    <ul class="nav-menu">
    <li class="rs-mega-menu mega-rs current-menu-item"> <a href="<?php echo site_url(); ?>">Beranda</a>
        
    </li>
        <li class="rs-mega-menu mega-rs current-menu-item">
            <a href="<?php echo site_url(); ?>about">Tentang</a>
            <!--<ul class="sub-menu">
                <li><a href="about.html">About One</a> </li>
                <li><a href="about2.html">About Two</a> </li>
            </ul>-->
        </li>

        <!-- //.mega-menu -->
        <li class="rs-mega-menu mega-rs menu-item-has-children current-menu-item"> <a href="<?php echo site_url(); ?>category">Kategori</a>
        <ul class="mega-menu"> 
            <li class="mega-menu-container">
                <div class="mega-menu-innner">
                    <?php echo get_menu_kategori(); ?>
                </div>
            </li>
        </ul> <!-- //.mega-menu -->
    </li>

        <!-- //.mega-menu -->
        <li class="rs-mega-menu mega-rs menu-item-has-children current-menu-item"> <a href="<?php echo site_url(); ?>umum">Kursus</a>
        <ul class="mega-menu"> 
            <li class="mega-menu-container">
                <div class="mega-menu-innner">
                    <?php echo get_menu_umum(); ?>
                </div>
            </li>
        </ul> <!-- //.mega-menu -->
    </li>

    <li class="rs-mega-menu mega-rs current-menu-item">
            <a href="<?php echo site_url(); ?>artikel">Artikel</a>
            <!--<ul class="sub-menu">
                <li><a href="about.html">About One</a> </li>
                <li><a href="about2.html">About Two</a> </li>
            </ul>-->
        </li>

    

        <li class="rs-mega-menu mega-rs  current-menu-item">
            <a href="#">Kontak</a>
        
        </li>
    </ul> <!-- //.nav-menu -->
</nav> 