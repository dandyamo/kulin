<!-- Checkout section start -->
<div id="rs-checkout" class="rs-checkout orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                 <div class="container">
                     <div class="coupon-toggle">
                         <div id="accordion" class="accordion">
                             <div class="card">
                                 <div class="card-header" id="headingOne">
                                     <!-- <div class="card-title">
                                         <span><i class="fa fa-window-maximize"></i> Have a coupon?</span>
                                         <button class="accordion-toggle" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Click here to enter your code</button>
                                     </div> -->
                                 </div>
                                 <!-- <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-bs-parent="#accordion">
                                     <div class="card-body">
                                         <p>If you have a coupon code, please apply it below.</p>
                                         <div class="coupon-code-input">
                                             <input type="text" name="coupon_code" placeholder="Coupon code" required="">
                                         </div>
                                         <button class="btn-shop orange-color" type="submit">apply coupon</button>
                                     </div> 
                                 </div> -->
                             </div>
                         </div>
                     </div>

                     <div class="full-grid">
                         <form>
                             

                             <!-- <div class="additional-fields">
                                 <div class="form-content-box">
                                     <div class="checkout-title">
                                         <h3>Additional information</h3>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Order notes (optional)</label>
                                                 <textarea placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div> -->

                             <div class="ordered-product">
                                 <div class="checkout-title">
                                     <h3>Konfirmasi Pembayaran</h3>
                                 </div>
                                 <table>
                                     <thead>
                                         <tr>
                                             <th>Total</th>
                                             <th>30.000</th>
                                         </tr>
                                     </thead>
                                    
                                 </table>
                             </div>

                             <div class="payment-method mt-40 md-mt-20">
                                 <div class="top-area">
                                     <div class="payment-co"><span>PayPal</span> <img src="assets/images/paypal.png" alt="PayPal Logo"></div>
                                     <div class="p-msg">Nomor Rekening BCA a/n Kulin - 08917484859</div>
                                 </div>
                                 <div class="bottom-area">
                                     <p class="mt-15">Silahkan upload bukti pembayaran di bawah ini :</p>
                                     <button class="btn-shop orange-color" type="submit">Upload Konfirmasi</button>
                                 </div>
                                 
                             </div>
                         </form>
                     </div>
                 </div>
            </div>
            <!-- Checkout section end -->