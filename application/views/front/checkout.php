<!-- Checkout section start -->
<div id="rs-checkout" class="rs-checkout orange-color pt-100 pb-100 md-pt-70 md-pb-70">
                 <div class="container">
                     <div class="coupon-toggle">
                         <div id="accordion" class="accordion">
                             <div class="card">
                                 <div class="card-header" id="headingOne">
                                     <!-- <div class="card-title">
                                         <span><i class="fa fa-window-maximize"></i> Have a coupon?</span>
                                         <button class="accordion-toggle" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Click here to enter your code</button>
                                     </div> -->
                                 </div>
                                 <!-- <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-bs-parent="#accordion">
                                     <div class="card-body">
                                         <p>If you have a coupon code, please apply it below.</p>
                                         <div class="coupon-code-input">
                                             <input type="text" name="coupon_code" placeholder="Coupon code" required="">
                                         </div>
                                         <button class="btn-shop orange-color" type="submit">apply coupon</button>
                                     </div> 
                                 </div> -->
                             </div>
                         </div>
                     </div>

                     <div class="full-grid">
                         <form>
                             <div class="billing-fields">
                                 <div class="checkout-title">
                                     <h3>Billing details</h3>
                                 </div>
                                 <div class="form-content-box">
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Nama Lengkap *</label>
                                                 <input id="name" name="name" class="form-control-mod" type="text" required="" disabled>
                                             </div>
                                         </div>
                                         <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Jenis Kelamin *</label>
                                                 <input id="jk" name="jk" class="form-control-mod" type="text" required="" disabled>
                                             </div>
                                         </div> -->
                                     </div>
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Nomor Telephone</label>
                                                 <input id="phone" name="phone" class="form-control-mod" type="text" disabled>
                                             </div>
                                         </div>
                                     </div>
                                    
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Alamat Email *</label>
                                                 <input id="city" name="city" class="form-control-mod" type="text" required="" disabled>
                                             </div>
                                         </div>
                                     </div>
                                     
                                     <!-- <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Alamat *</label>
                                                 <input id="pcode" name="pcode" class="form-control-mod" type="text" disabled>
                                             </div>
                                         </div>
                                     </div> -->
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Username *</label>
                                                 <input id="number" name="number" class="form-control-mod" type="text" required="" disabled>
                                             </div>
                                         </div>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Password *</label>
                                                 <input id="email" name="email" class="form-control-mod" type="email" required="" disabled>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div><!-- .billing-fields -->

                             <!-- <div class="additional-fields">
                                 <div class="form-content-box">
                                     <div class="checkout-title">
                                         <h3>Additional information</h3>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                 <label>Order notes (optional)</label>
                                                 <textarea placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                             </div> -->

                             <div class="ordered-product">
                                 <div class="checkout-title">
                                     <h3>Your order</h3>
                                 </div>
                                 <table>
                                     <thead>
                                         <tr>
                                             <th>Product</th>
                                             <th>Total</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                         <tr>
                                             <td>Medicine Bottle <strong><i class="fa fa-close"></i> 1</strong></td>
                                             <td>$30.00</td>
                                         </tr>
                                         <tr>
                                             <td>Medicine Bottle <strong><i class="fa fa-close"></i> 1</strong></td>
                                             <td>$30.00</td>
                                         </tr>
                                     </tbody>
                                     <tfoot>
                                         <tr>
                                             <th>Subtotal</th>
                                             <th>$60.00</th>
                                         </tr>
                                         <tr>
                                             <th>Total</th>
                                             <th>$60.00</th>
                                         </tr>
                                     </tfoot>
                                 </table>
                             </div>

                             <div class="payment-method mt-40 md-mt-20">
                                 <div class="top-area">
                                     <div class="payment-co"><span>PayPal</span> <img src="assets/images/paypal.png" alt="PayPal Logo"></div>
                                     <div class="p-msg">Pay via PayPal you can pay with your credit card if you dont have a PayPal account.</div>
                                 </div>
                                 <div class="bottom-area">
                                     <p class="mt-15">Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy.</p>
                                     <button class="btn-shop orange-color" type="submit">Continue to payment</button>
                                 </div>
                                 
                             </div>
                         </form>
                     </div>
                 </div>
            </div>
            <!-- Checkout section end -->