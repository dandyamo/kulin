<!-- Breadcrumbs Start -->
    <div class="rs-breadcrumbs breadcrumbs-overlay">
                <div class="breadcrumbs-img">
                    <img src="<?php echo base_url(); ?>assets/frontend/images/breadcrumbs/2.jpg" alt="Breadcrumbs Image">
                </div>
                <div class="breadcrumbs-text white-color">
                    <h1 class="page-title"><?php echo $judul; ?></h1>
                    <ul>
                        <li>
                            <a class="active" href="index.php">Home</a>
                        </li>
                        <li>Course Details</li>
                    </ul>
                </div>
            </div>
            <!-- Breadcrumbs End -->            

            <!-- Intro Courses -->
            <section class="intro-section gray-bg pt-94 pb-100 md-pt-64 md-pb-70">
                <div class="container">
                    <div class="row clearfix">
                        <!-- Content Column -->
                        <div class="col-lg-8 md-mb-50">
                            <!-- Intro Info Tabs-->
                            <div class="intro-info-tabs">
                                <ul class="nav nav-tabs intro-tabs tabs-box" id="myTab" role="tablist">
                                    <li class="nav-item tab-btns" role="presentation">
                                        <a class="nav-link tab-btn active" id="prod-overview-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-overview" role="tab" aria-controls="prod-overview" aria-selected="true">Overview</a>
                                    </li>
                                    <li class="nav-item tab-btns" role="presentation">
                                        <a class="nav-link tab-btn" id="prod-curriculum-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-curriculum" role="tab" aria-controls="prod-curriculum" aria-selected="false">Curriculum</a>
                                    </li>
                                    <li class="nav-item tab-btns" role="presentation">
                                        <a class="nav-link tab-btn" id="prod-instructor-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-instructor" role="tab" aria-controls="prod-instructor" aria-selected="false">Instructor</a>
                                    </li>
                                    <!-- <li class="nav-item tab-btns" role="presentation">
                                        <a class="nav-link tab-btn" id="prod-faq-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-faq" role="tab" aria-controls="prod-faq" aria-selected="false">Faq</a>
                                    </li>
                                    <li class="nav-item tab-btns" role="presentation">
                                        <a class="nav-link tab-btn" id="prod-reviews-tab" data-bs-toggle="tab" href="#" data-bs-target="#prod-reviews" role="tab" aria-controls="prod-reviews" aria-selected="false">Reviews</a>
                                    </li> -->
                                </ul>
                                <div class="tab-content tabs-content" id="myTabContent">
                                    <div class="tab-pane tab fade show active" id="prod-overview" role="tabpanel" aria-labelledby="prod-overview-tab">
                                        <div class="content white-bg pt-30">
                                            <!-- Cource Overview -->
                                            <div class="course-overview">
                                                <div class="inner-box">
                                                    <h4><?php echo $judul; ?></h4>
                                                    <p><?php echo $deskripsi; ?></p>
                                                    <ul class="student-list">
                                                        <li><i class="fa fa-youtube-play"></i>  23,564 Total Viewers</li>
                                                        <li><i class="fa fa-files-o"></i>  6 Lessons</li>
                                                        
                                                    </ul>                                                                                                        
                                                </div>
                                            </div>                                                
                                        </div>
                                    </div>
                                      <!-- Video Box -->
                                    <div class="tab-pane fade" id="prod-curriculum" role="tabpanel" aria-labelledby="prod-curriculum-tab">
                                        <div class="content">
                                            <div id="accordion" class="accordion-box">
                                                <div class="card accordion block">
                                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-bs-parent="#accordion">
                                                        <div class="card-body acc-content current">
                                                            <?php echo $video; ?>
                                                            <?php echo @$video_kunci; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                                             
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="prod-instructor" role="tabpanel" aria-labelledby="prod-instructor-tab">
                                        <div class="content pt-30 pb-30 pl-30 pr-30 white-bg">
                                            <h3 class="instructor-title">Instructors</h3>
                                            <div class="row rs-team style1 orange-color transparent-bg clearfix">
                                                <?php echo $tentor; ?>                                                            
                                            </div>  
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>
                        
                        <!-- Video Column -->
                        <div class="video-column col-lg-4">
                            <div class="inner-column">
                          
                                <div class="course-features-info">
                                    <ul>
                                        <li class="lectures-feature">
                                            <i class="fa fa-files-o"></i>
                                            <span class="label">Lessons</span>
                                            <span class="value">6</span>
                                        </li>
                                       
                                        <li class="quizzes-feature">
                                            <i class="fa fa-puzzle-piece"></i>
                                            <span class="label">Quizzes</span>
                                            <span class="value">0</span>
                                        </li>
                                       
                                        <!-- <li class="duration-feature">
                                            <i class="fa fa-clock-o"></i>
                                            <span class="label">Duration</span>
                                            <span class="value">10 week </span>
                                        </li> -->
                                      
                                        <li class="students-feature">
                                            <i class="fa fa-users"></i>
                                            <span class="label">Enrolled Users</span>
                                            <span class="value">21</span>
                                        </li>
                                       
                                        <li class="assessments-feature">
                                            <i class="fa fa-check-square-o"></i>
                                            <span class="label">Assessments</span>
                                            <span class="value">Yes</span>
                                        </li>
                                    </ul>
                                </div>
                                
                                <div class="btn-part">
                                    <a href="#" class="btn readon2 orange"> Package include tutor <br>
                                    Only $35</a>
                                    <a href="#" class="btn readon2 orange-transparent">Enroll Course Now</a>
                                </div>
                            </div>
                        </div>                        
                    </div>                
                </div>
            </section>
            <!-- End intro Courses -->

        </div> 
        <!-- Main content End --> 