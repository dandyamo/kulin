<div id="rs-categories" class="rs-categories gray-bg style1 pt-94 pb-70 md-pt-64 md-pb-40">
        <div class="container">
            <div class="row y-middle mb-50 md-mb-30">
                <div class="col-md-6 sm-mb-30">
                    <div class="sec-title">
                        <div class="sub-title primary">Kategori Akademik</div>
                        <h2 class="title mb-0"> Kategori Teratas kami </h2>
                    </div>
                </div>
                
            </div>
            <div class="row">
                <?php echo $list_kategori; ?>
                <div class="pagination-area orange-color text-center mt-30 md-mt-0">
                <?php echo $links; ?>
            </div>
            </div>
        </div>
    </div>
    

    
</div> 
    
</div> 