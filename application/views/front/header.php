<!DOCTYPE html>
<html lang="zxx">  
    <head>
        <!-- meta tag -->
        <meta charset="utf-8">
        <title><?php echo $par['title']; ?></title>
        <meta name="description" content="<?php echo $par['description']; ?>">
        <meta name="keywords" content="<?php echo $par['keywords']; ?>">
        <!-- responsive tag -->
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- favicon
        <link rel="apple-touch-icon" href="apple-touch-icon.png">-->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>assets/frontend/images/logo-k.png"> 
        <!-- Bootstrap v5.0.2 css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/bootstrap.min.css">
        <!-- font-awesome css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/font-awesome.min.css">
        <!-- animate css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/animate.css">
        <!-- owl.carousel css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/owl.carousel.css">
        <!-- slick css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/slick.css">
        <!-- off canvas css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/off-canvas.css">
        <!-- linea-font css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/fonts/linea-fonts.css">
        <!-- flaticon css  -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/fonts/flaticon.css">
        <!-- magnific popup css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/magnific-popup.css">
        <!-- Main Menu css -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/frontend/css/rsmenu-main.css">
        <!-- spacing css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/rs-spacing.css">
        <!-- style css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/style.css"> <!-- This stylesheet dynamically changed from style.less -->
        <!-- responsive css -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/frontend/css/responsive.css">
        
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="defult-home">
        
        <!--Preloader area start here-->
        <div id="loader" class="loader">
            <div class="loader-container">
                <div class='loader-icon'>
                    <img src="<?php echo base_url(); ?>assets/frontend/images/logo-k.png" alt="">
                </div>
            </div>
        </div>
        <!--Preloader area End here--> 


        <!--Full width header Start-->
        <div class="full-width-header header-style1 home1-modifiy home12-modifiy">
            <!--Header Start-->
            <header id="rs-header" class="rs-header">
                <!-- Topbar Area Start -->
                <div class="topbar-area home11-topbar">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-md-5">
                                <ul class="topbar-contact">
                                    <li>
                                        <i class="flaticon-email"></i>
                                        <a href="mailto:<?php echo $par['email']; ?>"><?php echo $par['email']; ?></a>
                                    </li>
                                    <li>
                                        <i class="fa flaticon-call"></i>
                                        <a href="tel:"><?php echo $par['phone']; ?></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-7 text-end">
                                    <ul class="toolbar-sl-share">
                                        <!--<li class="opening"> <i class="flaticon-location"></i> Yogyakarta </li>-->
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Topbar Area End -->

               
                <!-- Menu Start -->
                <div class="menu-area menu-sticky">
                    <div class="container">
                        <div class="row y-middle">
                            <div class="col-lg-2">
                              <div class="logo-cat-wrap">
                                  <div class="logo-part">
                                      <a href="<?php echo site_url(); ?>">
                                          <img src="<?php echo base_url(); ?>uploads/brands/<?php echo $par['logo']; ?>" alt="">
                                      </a>
                                  </div>
                              </div>
                            </div>
                            <div class="col-lg-8 text-end">
                                <div class="rs-menu-area">
                                    <div class="main-menu">
                                      <div class="mobile-menu">
                                          <a class="rs-menu-toggle">
                                              <i class="fa fa-bars"></i>
                                          </a>
                                      </div>
                                      <?php $this->load->view('front/menu'); ?>                                        
                                    </div> <!-- //.main-menu -->                                
                                </div>
                            </div>
                            <div class="col-lg-2 text-end">
                                <div class="expand-btn-inner">
                                    <ul>
                                        <li>
                                            <a class="hidden-xs rs-search" data-bs-toggle="modal" data-bs-target="#searchModal" href="#">
                                                <i class="flaticon-search"></i>
                                            </a>
                                        </li>
                                        <li class="icon-bar cart-inner no-border mini-cart-active">
                                            <a class="cart-icon">
                                             
                                                <i class="fa fa-user"></i>
                                            </a>
                                             <div class="woocommerce-mini-cart text-start">
                                                <div class="cart-bottom-part">
                                                    <ul class="cart-icon-product-list">

                                                    <?php if($this->session->userdata('log') == TRUE) { ?>
                                                        <li class="display-flex">
                                                            <!-- <div class="icon-cart">
                                                                <a href="#"><i class="fa fa-times"></i></a>
                                                            </div> -->
                                                            
                                                            <div class="product-info">
                                                                <a href="<?php echo site_url(); ?>profile">Hi, <?php echo $this->session->userdata('nama'); ?>!</a><br>
                                                                
                                                            </div>
                                                        </li>

                                                        <li class="display-flex">
                                                            <!-- <div class="icon-cart">
                                                                <a href="#"><i class="fa fa-times"></i></a>
                                                            </div> -->
                                                            
                                                            <div class="product-info">
                                                                <a href="<?php echo site_url(); ?>login/logout">Keluar</a><br>
                                                                
                                                            </div>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li class="display-flex">
                                                            <!-- <div class="icon-cart">
                                                                <a href="#"><i class="fa fa-times"></i></a>
                                                            </div> -->
                                                            <div class="product-info">
                                                                <a href="<?php echo site_url(); ?>login">Masuk</a><br>
                                                                
                                                            </div>
                                                        </li>

                                                        <li class="display-flex">
                                                            <!-- <div class="icon-cart">
                                                                <a href="#"><i class="fa fa-times"></i></a>
                                                            </div> -->
                                                            <div class="product-info">
                                                                <a href="<?php echo site_url(); ?>register">Daftar</a><br>
                                                                
                                                            </div>
                                                        </li>
                                                    <?php } ?>

                                                         <!-- if sudah login -->
                                                                                                                  
                                                    </ul>
                                                </div>
                                            </div> 
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Menu End --> 
                <!-- Canvas Menu end -->
            </header>
            <!--Header End-->
        </div>
        <!--Full width header End-->

		<!-- Main content Start -->
        <div class="main-content">