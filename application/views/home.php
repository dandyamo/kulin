<?php $this->load->view('header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0"><?php echo $title; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item">Beranda</li>
              <li class="breadcrumb-item active"><?php echo $breadcrumb; ?></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <?php if ($this->session->flashdata('ok')) : ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
              <?= $this->session->flashdata('ok'); ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
        </div>
      <?php endif; ?> 
      <?php if ($this->session->flashdata('fail')) : ?>
        <div class="row">
          <div class="col-lg-12">
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <?= $this->session->flashdata('fail'); ?>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
          </div>
        </div>
      <?php endif; ?>  
      <?php if(@$content)$this->load->view($content);?>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
<?php $this->load->view('footer'); ?>