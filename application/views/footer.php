<footer class="main-footer">
  <strong>Copyright &copy; 2021</strong>
  <div class="float-right d-none d-sm-inline-block">
  </div>
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
  <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<?php echo @$modal; ?>

<div class="modal fade" id="modal-default">
<div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title"><i class="fa fa-power-off"></i> Logout</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <p>Apakah anda yakin ingin keluar?</p>
    </div>
    <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <?php if($this->session->userdata('role') == 1) { ?>
          <a href="<?php echo base_url(); ?>cms/login/logout" class="btn btn-danger">Keluar</a>
        <?php } else if($this->session->userdata('role') == 2) { ?>
          <a href="<?php echo base_url(); ?>cms/login/logout" class="btn btn-danger">Keluar</a>
        <?php } else if($this->session->userdata('role') == 3) { ?>
          <a href="<?php echo base_url(); ?>freelancer/login/logout" class="btn btn-danger">Keluar</a>
        <?php } else if($this->session->userdata('role') == 4) { ?>
          <a href="<?php echo base_url(); ?>tentor/login/logout" class="btn btn-danger">Keluar</a>
        <?php } ?>
    </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button)
</script>

<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/moment/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Summernote -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/backend/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/backend/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?php echo base_url(); ?>assets/backend/plugins/summernote/summernote-bs4.min.js"></script>
<!-- Ekko Lightbox -->
<script src="<?php echo base_url(); ?>assets/backend/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>assets/backend/ckeditor/ckeditor.js'></script>  

<?php echo @$script; ?>
<!-- page script -->
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
    $("#example2").DataTable({
      "responsive": true,
      "autoWidth": false,
    });
  });
</script>
<script>
  $(function () {
    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });

    $('.filter-container').filterizr({gutterPixels: 3});
    $('.btn[data-filter]').on('click', function() {
      $('.btn[data-filter]').removeClass('active');
      $(this).addClass('active');
    });
  })
</script>
<script type="text/javascript">
            $(document).ready(function(){
            $("#title").keyup(function () {
                var judul = $('#title').val();
                $('#permalink').val(slugify(judul));
            });
                
            function slugify(text)
            {
            return text.toString().toLowerCase()
                            .replace(/\s+/g, '-')           // Replace spaces with -
                            .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
                            .replace(/\-\-+/g, '-')         // Replace multiple - with single -
                            .replace(/^-+/, '')             // Trim - from start of text
                            .replace(/-+$/, '');            // Trim - from end of text
        }
	    });
    </script>

<script type="text/javascript">

  $('#add_jadwal').click(function(){
        var n = $('.rec-element-jadwal').length;
            var dd = n+1;
            var thecontent = '<tr class="rec-element-jadwal">'
                            +'<td style="width:10px">'+dd+'</td>'
                            +'<td>'
                                +'<div class="form-group">'
                                +'<label>Tanggal</label>'
                                +'<input name="tgl_jadwal'+n+'" type="date" class="form-control" placeholder="...">'
                                +'</div>'
                            +'</td>'
                            +'<td>'
                                +'<div class="form-group">'
                                +'<label>Waktu</label>'
                                +'<input name="waktu'+n+'" type="time" class="form-control" placeholder="...">'
                                +'</div>'
                            +'</td>'
                            +'<td style="width: 10px;"><a href="#" id="add_jadwal" class="btn btn-flat btn-sm btn-warning del-element-jadwal"><i class="fa fa-minus"></i></a></td>'
                        +'</tr>';
        $('#element_jadwal').append(thecontent);
        $('#jumlahjadwal').val($('.rec-element-jadwal').length);
        return false;
    });
    $(document).on('click','a.del-element-jadwal',function (e) {
		e.preventDefault();
		$(this).parents('.rec-element-jadwal').fadeOut(400);
		$(this).parents('.rec-element-jadwal').remove();
		$('#jumlahjadwal').val($('.rec-element-jadwal').length);
	});

    $(document).ready(function(){
        $('#id_ref_kategori').change(function(){
          var id=$(this).val();
          if(id == 1){

            $.ajax({

            url : "<?php echo site_url();?>cms/tentor/get_matkul",
            method : "POST",
            data : {id: id},
            async : false,
            dataType : 'JSON',
            success: function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option value="'+data[i].id_ref_matkul+'">'+data[i].ref_matkul+'</option>';
                }
                $('#id_kursus').html(html);
                
            }

            });

            } else {

              $.ajax({

              url : "<?php echo site_url();?>cms/tentor/get_umum",
              method : "POST",
              data : {id: id},
              async : false,
              dataType : 'JSON',
              success: function(data){
                  var html = '';
                  var i;
                  for(i=0; i<data.length; i++){
                      html += '<option value="'+data[i].id_ref_umum+'">'+data[i].ref_umum+'</option>';
                  }
                  $('#id_kursus').html(html);
                  
              }

              });

            }
           
        });
    });
</script>
</body>
</html>
