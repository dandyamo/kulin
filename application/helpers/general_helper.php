<?php
if(!function_exists('_ci')) {
	function _ci() {
		$_ci = &get_instance();
		return $_ci;
	}
}

function in_de($data) {
	$data_on = base64_encode(serialize($data));
	return preg_replace('/\=+/', '-', $data_on);
}

function un_de($data) {
	$on_data = preg_replace('/\-+/', '=', $data);
	return unserialize(base64_decode($on_data));
}

/* -- Menu Kategori */
if(!function_exists('get_menu_kategori')){
	function get_menu_kategori(){

		_ci()->load->database();
		_ci()->db->where('status','1');
		$q = _ci()->db->get('ref_fakultas');

		echo '<div class="single-megamenu">
				<ul class="sub-menu">';
		foreach($q->result() as $row){
			echo '
					<li><a href="'.site_url().'category-list/'.$row->link_fakultas.'">'.$row->ref_fakultas.'</a> </li>
				';
			
		}

		echo '</ul>
		</div>';
	}
}

/* -- Menu umum */
if(!function_exists('get_menu_umum')){
	function get_menu_umum(){

		_ci()->load->database();
		_ci()->db->where('status','1');
		$q = _ci()->db->get('ref_kat_umum');

		echo '<div class="single-megamenu">
				<ul class="sub-menu">';
		foreach($q->result() as $row){
			echo '
					<li><a href="'.site_url().'course-umum/'.$row->link_kat_umum.'">'.$row->ref_kat_umum.'</a> </li>
				';
			
		}

		echo '</ul>
		</div>';
	}
}

/* -- Slide */
if(!function_exists('get_slide')){
	function get_slide(){

		_ci()->load->database();
		_ci()->db->where('status','1');
		$q = _ci()->db->get('ref_slideshow');

		echo '<div class="rs-slider style1">
		<div class="rs-carousel owl-carousel" data-loop="true" data-items="1" data-margin="0" data-autoplay="true" data-hoverpause="true" data-autoplay-timeout="5000" data-smart-speed="800" data-dots="false" data-nav="false" data-nav-speed="false" data-center-mode="false" data-mobile-device="1" data-mobile-device-nav="false" data-mobile-device-dots="false" data-ipad-device="1" data-ipad-device-nav="false" data-ipad-device-dots="false" data-ipad-device2="1" data-ipad-device-nav2="true" data-ipad-device-dots2="false" data-md-device="1" data-md-device-nav="true" data-md-device-dots="false">';

		foreach($q->result() as $row){
			echo '
				<div class="slider-content slide" style="background: url('.base_url().'uploads/file_slideshow/'.$row->berkas.'); background-size: cover; background-position: center; background-repeat: no-repeat;">
					<div class="container">
						<div class="sl-sub-title white-color wow bounceInLeft" data-wow-delay="300ms" data-wow-duration="2000ms"></div>
					</div>
				</div>
				';
			
		}

		echo '</div>
		</div>';
	}
}

function app_param($id = null) {
	$CI =& get_instance();
	$CI->load->database();
	if ($id) {
		if(!$dt=$CI->db->where('param',$id)->get('parameter')->row('val'))
			if(!$dt=@$CI->config->item('app')[$id])
				$dt=@$CI->config->item($id);
		return $dt;
	}else{
		$app=array();
		if(!$app=@$CI->config->item('app')) 
			if($app_config=@$CI->config->item('app_config'))
				foreach ($app_config as $ap) $app[$ap]=@$CI->config->item($ap);
		
		@$app_key=array_keys($app);
		if(($db=$CI->db->where_in('param',$app_key)->get('parameter'))&&($db->num_rows()>0))
			foreach ($db->result() as $r) 
				$app[$r->param]=$r->val;                                
		return $app;
	}
}