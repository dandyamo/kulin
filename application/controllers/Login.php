<?php

class Login Extends CI_Controller{

	public function index()
	{
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$data['content'] = "front/login";
		$this->load->view('front/index', $data);
	}

	public function action_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password));
		$peserta = $this->Model_general->login('peserta',$where)->row_array();
		
		if ($peserta > 0) {

			$data_session = array(
				'username' => $username,
				'nama' => $peserta['nama_peserta'],
				'id' => $peserta['id_peserta'],
				'log' => TRUE
				);

			$this->session->set_userdata($data_session);
			$this->session->set_flashdata('ok_login', 'Berhasil');
			redirect(site_url());
			
		} else {

			$this->session->set_flashdata('fail_login','Gagal');
			redirect(site_url("login"));

		}


	}

	function logout(){
		$this->session->sess_destroy();
		redirect(site_url());
	}

}
