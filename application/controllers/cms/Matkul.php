<?php

class Matkul Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Mata Kuliah";
		$data['title'] = "Data Mata Kuliah";

        $from = array(
			  'ref_matkul rm' => '',
        'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
        'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
        'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left')
		);

        $select = 'rm.*,rj.ref_jurusan,rf.ref_fakultas,rs.ref_semester';
        $ref_matkul = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rm.ref_matkul ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_matkul->result() as $mat){

            if($mat->status == 1) {

                $statusx = anchor(site_url('cms/matkul/tidak_aktif/'.$mat->id_ref_matkul.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');

            }else{
                
                $statusx = anchor(site_url('cms/matkul/aktif/'.$mat->id_ref_matkul.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');

            }

            $edit = '<a href="'.site_url().'cms/referensi/matkul/ubah/'.$mat->id_ref_matkul.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$mat->id_ref_matkul.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$mat->ref_matkul.'</td>
            <td>'.$mat->ref_semester.'</td>
            <td>'.$mat->ref_jurusan.'</td>
            <td>'.$mat->ref_fakultas.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$mat->id_ref_matkul.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/matkul/hapus/'.$mat->id_ref_matkul.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30px">Status</th>
                <th>Mata Kuliah</th>
                <th>Semester</th>
                <th>Jurusan</th>
                <th>Fakultas</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/matkul/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Mata Kuliah";
		    $data['title'] = "Tambah Mata Kuliah";

        $jurusan = $this->Model_general->combo_box(array('tabel'=>'ref_jurusan', 'key'=>'id_ref_jurusan', 'val'=>array('ref_jurusan')));
        $fakultas = $this->Model_general->combo_box(array('tabel'=>'ref_fakultas', 'key'=>'id_ref_fakultas', 'val'=>array('ref_fakultas')));
        $semester = $this->Model_general->combo_box(array('tabel'=>'ref_semester', 'key'=>'id_ref_semester', 'val'=>array('ref_semester')));

        $data['form_data'] = '
        <input type="hidden" name="link_matkul" id="permalink">
        <div class="card-body">
            <div class="form-group">
            <label>Fakultas</label>
            '.form_dropdown('id_ref_fakultas', $fakultas, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Jurusan</label>
            '.form_dropdown('id_ref_jurusan', $jurusan, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Semester</label>
            '.form_dropdown('id_ref_semester', $semester, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Mata Kuliah</label>
            <input name="ref_matkul" type="text" id="title" class="form-control" placeholder="Mata Kuliah">
            </div>
            <div class="form-group">
            <label>Keterangan</label>
            <textarea name="deskripsi" id="ckeditor" type="text" class="form-control" placeholder="Keterangan"></textarea>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['script'] ='
        <script type="text/javascript">
            $(function () {
                    CKEDITOR.replace("ckeditor",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "200px"
                    });
                });
        </script>';

		$data['back'] = ''.site_url().'cms/referensi/matkul';
		$data['link'] = ''.site_url().'cms/matkul/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
    $data['breadcrumb'] = "Ubah Mata Kuliah";
		$data['title'] = "Ubah Mata Kuliah";

    $from = array(
      'ref_matkul rm' => '',
      'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
      'ref_fakultas rf' => array('rf.id_ref_fakultas = rj.id_ref_fakultas','left')
		);

        $ref_matkul = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rm.id_ref_matkul'=> $id)))->row();

        $jurusan = $this->Model_general->combo_box(array('tabel'=>'ref_jurusan', 'key'=>'id_ref_jurusan', 'val'=>array('ref_jurusan')));
        $fakultas = $this->Model_general->combo_box(array('tabel'=>'ref_fakultas', 'key'=>'id_ref_fakultas', 'val'=>array('ref_fakultas')));
        $semester = $this->Model_general->combo_box(array('tabel'=>'ref_semester', 'key'=>'id_ref_semester', 'val'=>array('ref_semester')));

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_matkul" value="'.$ref_matkul->id_ref_matkul.'">
            <input type="hidden" name="link_matkul" id="permalink" value="'.$ref_matkul->link_matkul.'">
            <div class="form-group">
            <label>Fakultas</label>
            '.form_dropdown('id_ref_fakultas', $fakultas, $ref_matkul->id_ref_fakultas,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Jurusan</label>
            '.form_dropdown('id_ref_jurusan', $jurusan, $ref_matkul->id_ref_jurusan,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Semester</label>
            '.form_dropdown('id_ref_semester', $semester, $ref_matkul->id_ref_semester,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Mata Kuliah</label>
            <input type="text" name="ref_matkul" class="form-control" id="title" value="'.$ref_matkul->ref_matkul.'" placeholder="Mata Kuliah">
            </div>
            <div class="form-group">
            <label>Keterangan</label>
            <textarea name="deskripsi" id="ckeditor" type="text" class="form-control" placeholder="Keterangan">'.$ref_matkul->deskripsi.'</textarea>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['script'] ='<script>
    var ckeditor = CKEDITOR.replace("ckeditor",{
          height:"100px"
    });
    
    CKEDITOR.disableAutoInline = true;
    </script>';

		$data['back'] = ''.site_url().'cms/referensi/matkul';
    $data['link'] = ''.site_url().'cms/matkul/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
    {
      $id_ref_matkul = $this->input->post('id_ref_matkul');
      $id_ref_fakultas = $this->input->post('id_ref_fakultas');
      $id_ref_jurusan = $this->input->post('id_ref_jurusan');
      $id_ref_semester = $this->input->post('id_ref_semester');
	    $ref_matkul = $this->input->post('ref_matkul');
	    $deskripsi = $this->input->post('deskripsi');
	    $link_matkul = $this->input->post('link_matkul');

        $par = array(
            'tabel'=>'ref_matkul',
            'data'=>array(
            'id_ref_fakultas'=>$id_ref_fakultas,
            'id_ref_jurusan'=>$id_ref_jurusan,
            'id_ref_semester'=>$id_ref_semester,
            'ref_matkul'=>$ref_matkul,
            'deskripsi'=>$deskripsi,
            'link_matkul'=>$link_matkul,
            'status'=> 1
            ),
        );

        if($id_ref_matkul != NULL) $par['where'] = array('id_ref_matkul'=>$id_ref_matkul);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/matkul');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_matkul',$data,'id_ref_matkul',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/referensi/matkul');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_matkul',$data,'id_ref_matkul',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/referensi/matkul');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_matkul','id_ref_matkul',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/matkul');
	}

}
