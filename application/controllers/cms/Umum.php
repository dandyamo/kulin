<?php

class Umum Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Umum";
		$data['title'] = "Data Umum";

        $from = array(
			'parameter rk' => '',
		);

        $vals = $this->Model_general->get_param(array(
			'title','description','address','phone','email','footer','logo','keywords'),2);

         if ($vals['logo'] == NULL) {
            $image = 'Tidak Ada Logo Website';
         } else {
            $image = '<img src="'.base_url('uploads/brands/'.$vals['logo']).'" style="width:100%;">';
            }

        $table = '';
        $data['table'] = '
        <form action="'.site_url().'cms/umum/simpan" method="post" enctype="multipart/form-data">
        <div class="row">
        <div class="col-lg-6">		
            <!-- kolom kiri -->
                <label>Judul Website</label>
                <p>
                '.form_hidden("keys[]","title").
                form_input("vals[]",!empty($vals['title'])?$vals['title']:null,
                'class="form-control"').'
                </p>
                <label>Deskripsi</label>
                    <p>
                    '.
                    form_hidden("keys[]","description").
                    form_input("vals[]",!empty($vals['description'])?$vals['description']:null,'
                        class="form-control"').'
                </p>
                <label>Kata Kunci</label>
                    <p>
                    '.
                    form_hidden("keys[]","keywords").
                    form_input("vals[]",!empty($vals['keywords'])?$vals['keywords']:null,'
                        class="form-control"').'
                </p>
                <label>Logo Website</label>
                    <p>'.$image.'</p>
                    <p>
                    '.
                    form_hidden('key','logo').
					form_upload('vals',!empty($vals['logo'])?$vals['logo']:null,'
					').'
                </p>
            <!-- kolom kiri -->

        </div>
        <div class="col-lg-6">		
            <!-- kolom kiri -->
                <label>Catatan Kaki</label>
                <p>
                '.form_hidden("keys[]","footer").
                form_input("vals[]",!empty($vals['footer'])?$vals['footer']:null,
                'class="form-control"').'
                </p>
                <label>Telepon</label>
                    <p>
                    '.
                    form_hidden("keys[]","phone").
                    form_input("vals[]",!empty($vals['phone'])?$vals['phone']:null,'
                        class="form-control"').'
                </p>
                <label>Email</label>
                    <p>
                    '.
                    form_hidden("keys[]","email").
                    form_input("vals[]",!empty($vals['email'])?$vals['email']:null,'
                        class="form-control"').'
                </p>
                <label>Alamat</label>
                    <p>
                    '.
                    form_hidden("keys[]","address").
                    form_input("vals[]",!empty($vals['address'])?$vals['address']:null,'
                        class="form-control"').'
                </p>
            <!-- kolom kiri -->

        </div>
        <div class="col-lg-12">
		<button class="btn btn-success btn-simpan float-right"><i class="fa fa-save"></i> &nbsp; Simpan Pengaturan</button>
	    </div>
        </div>
        </form>
        ';

        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}


    public function simpan()
	{
	    $keys = $this->input->post('keys');
	    $key = $this->input->post('key');
		$vals = $this->input->post('vals');
		
		for ($i = 0; $i < count($keys); $i++) {
			$g = $this->Model_general->getdata(array('tabel' => 'parameter', 'where' => array('param' => $keys[$i])));
			if ($g->num_rows() > 0) $this->ubah_param($keys[$i],$vals[$i]);
			else $this->simpan_param($keys[$i],$vals[$i]);
		}

        if ($_FILES['vals']['name']!='') {
            $files = $_FILES;
            
            $config['upload_path'] = './uploads/brands/';
            $config['allowed_types'] = '*';
            $config['max_size']	= '1000000';
            $this->load->library('upload');
            $this->upload->initialize($config);
            
                    if ($this->upload->do_upload('vals')){
                        $data = $this->upload->data();
                        $foto = $data['file_name'];
                        
                        //tabel
                        $file_gambar = $this->Model_general->getdata(array('tabel' => 'parameter', 'where' => array('param' => $key)))->row();
                        
                        $path = './uploads/brands/'.$file_gambar->val;
                        if(file_exists($path)) unlink($path);		
                        $this->Model_general->delete_data('parameter','param',$key);
                        
                        $this->simpan_param($key,$foto);

                    }else{
                        $data['error'] = $this->upload->display_errors();
                        $this->session->set_flashdata('fail','File tidak bisa diupload<br>'.$data['error']);
                    }
            }

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/tampilan/umum');

	}

    function simpan_param($par,$dat) {
		$this->Model_general->save_data('parameter',array('param' => $par,'val'=> $dat));
	}
	
	function ubah_param($par,$dat) {
		$this->Model_general->save_data('parameter',array('val' => $dat),'param',$par);
	}

}
