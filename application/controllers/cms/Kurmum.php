<?php

class Kurmum Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Kursus Umum";
		$data['title'] = "Data Kursus Umum";

        $from = array(
		'ref_umum ru' => '',
        'ref_kat_umum rku' => array('rku.id_ref_kat_umum = ru.id_ref_kat_umum','left')
		);

        $select = 'ru.*,rku.ref_kat_umum';
        $ref_umum = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ru.ref_umum ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_umum->result() as $mat){

            if($mat->status == 1) {

                $statusx = anchor(site_url('cms/kurmum/tidak_aktif/'.$mat->id_ref_umum.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');

            }else{
                
                $statusx = anchor(site_url('cms/kurmum/aktif/'.$mat->id_ref_umum.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');

            }

            $edit = '<a href="'.site_url().'cms/referensi/kurmum/ubah/'.$mat->id_ref_umum.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$mat->id_ref_umum.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$mat->ref_umum.'</td>
            <td>'.$mat->ref_kat_umum.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$mat->id_ref_umum.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/kurmum/hapus/'.$mat->id_ref_umum.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30px">Status</th>
                <th>Kursus</th>
                <th>Kategori</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/kurmum/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> <a href="'.site_url().'cms/katmum" class="btn btn-sm btn-warning"><i class="fa fa-plus"></i> Kategori Kursus</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Kursus Umum";
		$data['title'] = "Tambah Kursus Umum";

        $kat_umum = $this->Model_general->combo_box(array('tabel'=>'ref_kat_umum', 'key'=>'id_ref_kat_umum', 'val'=>array('ref_kat_umum')));

        $data['form_data'] = '
        <input type="hidden" name="link_umum" id="permalink">
        <div class="card-body">
            <div class="form-group">
            <label>Kategori Umum</label>
            '.form_dropdown('id_ref_kat_umum', $kat_umum, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Kursus Umum</label>
            <input name="ref_umum" type="text" id="title" class="form-control" placeholder="Kursus Umum">
            </div>
            <div class="form-group">
            <label>Keterangan</label>
            <textarea name="deskripsi" id="ckeditor" type="text" class="form-control" placeholder="Keterangan"></textarea>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['script'] ='<script>
    var ckeditor = CKEDITOR.replace("ckeditor",{
          height:"100px"
    });
    
    CKEDITOR.disableAutoInline = true;
    </script>';

		$data['back'] = ''.site_url().'cms/referensi/kurmum';
		$data['link'] = ''.site_url().'cms/kurmum/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
    $data['breadcrumb'] = "Ubah Kursus Umum";
		$data['title'] = "Ubah Kursus Umum";

    $from = array(
		'ref_umum ru' => '',
        'ref_kat_umum rku' => array('rku.id_ref_kat_umum = ru.id_ref_kat_umum','left')
		);

        $ref_umum = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('ru.id_ref_umum'=> $id)))->row();

        $kat_umum = $this->Model_general->combo_box(array('tabel'=>'ref_kat_umum', 'key'=>'id_ref_kat_umum', 'val'=>array('ref_kat_umum')));

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_umum" value="'.$ref_umum->id_ref_umum.'">
            <input type="hidden" name="link_umum" id="permalink" value="'.$ref_umum->link_umum.'">
            <div class="form-group">
            <label>Kategori Umum</label>
            '.form_dropdown('id_ref_kat_umum', $kat_umum, $ref_umum->id_ref_kat_umum,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Kursus Umum</label>
            <input type="text" name="ref_umum" class="form-control" id="title" value="'.$ref_umum->ref_umum.'" placeholder="Kursus Umum">
            </div>
            <div class="form-group">
            <label>Keterangan</label>
            <textarea name="deskripsi" id="ckeditor" type="text" class="form-control" placeholder="Keterangan">'.$ref_umum->deskripsi.'</textarea>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['script'] ='<script>
    var ckeditor = CKEDITOR.replace("ckeditor",{
          height:"100px"
    });
    
    CKEDITOR.disableAutoInline = true;
    </script>';

		$data['back'] = ''.site_url().'cms/referensi/kurmum';
    $data['link'] = ''.site_url().'cms/kurmum/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
    {
      $id_ref_umum = $this->input->post('id_ref_umum');
      $id_ref_kat_umum = $this->input->post('id_ref_kat_umum');
	    $ref_umum = $this->input->post('ref_umum');
	    $deskripsi = $this->input->post('deskripsi');
	    $link_umum = $this->input->post('link_umum');

        $par = array(
            'tabel'=>'ref_umum',
            'data'=>array(
            'id_ref_kat_umum'=>$id_ref_kat_umum,
            'ref_umum'=>$ref_umum,
            'deskripsi'=>$deskripsi,
            'link_umum'=>$link_umum,
            'status'=> 1
            ),
        );

        if($id_ref_umum != NULL) $par['where'] = array('id_ref_umum'=>$id_ref_umum);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/kurmum');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_umum',$data,'id_ref_umum',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/referensi/kurmum');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_umum',$data,'id_ref_umum',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/referensi/kurmum');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_umum','id_ref_umum',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/kurmum');
	}

}
