<?php

class Freelancer Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Freelancer";
		$data['title'] = "Data Freelancer";

        $from = array(
			'freelancer f' => '',
		);

        $freelancer = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'f.nama_freelancer ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($freelancer->result() as $fre){

          if($this->session->userdata('role') == 1) {

            $edit = '<a href="'.site_url().'cms/data/freelancer/ubah/'.$fre->id_freelancer.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$fre->id_freelancer.'"><i class="fa fa-trash"></i></a>';

            $view = '<center><a href="#" class="btn btn-info btn-xs" title="Klik untuk melihat rekening" data-toggle="modal" data-target="#view'.$fre->id_freelancer.'"><i class="fa fa-list-alt"></i></a></center>';

            $rek_freelancer = $this->Model_general->getdata(array('tabel'=>'rek_freelancer', 'order'=>'bank_freelancer ASC','where'=>array('id_freelancer'=>$fre->id_freelancer)))->result();

            $tab_rek = '';
            foreach($rek_freelancer as $rek){

            $tab_rek .='<tr>
            <td>'.$no.'</td>
            <td>'.$rek->bank_freelancer.'</td>
            <td>'.$rek->an_freelancer.'</td>
            <td>'.$rek->rek_freelancer.'</td>
            </tr>';

            $data['modal'] .= '
            <div class="modal fade" id="view'.$fre->id_freelancer.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-list-alt"></i> List Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <table class="table table-bordered">
                  <thead>
                      <tr>
                      <th style="width: 10px">No</th>
                      <th>Bank</th>
                      <th>Atas Nama</th>
                      <th>No Rekening</th>
                      </tr>
                  </thead>
                  <tbody>
                      '.$tab_rek.'
                  </tbody>
                  </table>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';

            }
            
          } else {

            $edit = '<a href="'.site_url().'cms/data/freelancer/ubah/'.$fre->id_freelancer.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$fre->id_freelancer.'"><i class="fa fa-trash"></i></a>';

          }

            
            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$fre->nama_freelancer.'</td>
            <td>'.$fre->jenis_kelamin.'</td>
            <td>'.$fre->no_telp.'</td>
            <td>'.$fre->email.'</td>
            <td>'.$fre->alamat.'</td>
            <td>'.@$edit.' '.@$hapus.' '. @$view.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$fre->id_freelancer.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/freelancer/hapus/'.$fre->id_freelancer.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>No Telepon</th>
                <th>Email</th>
                <th>Alamat</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';


        $data['tombol'] = '<a href="'.site_url().'cms/data/freelancer/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> <a target="_blank" href="'.site_url().'cms/freelancer/cetak" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Cetak</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Freelancer";
		$data['title'] = "Tambah Freelancer";

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Nama</label>
            <input name="nama_freelancer" type="text" class="form-control" placeholder="Nama">
            </div>
            <div class="form-group">
            <label>Jenis Kelamin</label>
            <select name="jenis_kelamin" class="form-control">
                <option>-- Pilih --</option>
                <option>Laki-laki</option>
                <option>Perempuan</option>
            </select>
            </div>
            <div class="form-group">
            <label>No Telepon</label>
            <input name="no_telp" type="text" class="form-control" placeholder="No Telepon">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input name="email" type="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" type="text" class="form-control" placeholder="Alamat"></textarea>
            </div>
            <div class="form-group">
            <label>Username</label>
            <input name="username" type="text" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
            <label>Password</label>
            <input name="password" type="password" class="form-control" placeholder="Password">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/data/freelancer';
		$data['link'] = ''.site_url().'cms/freelancer/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Freelancer";
		$data['title'] = "Ubah Freelancer";

        $from = array(
			'freelancer f' => '',
		);

        $freelancer = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('f.id_freelancer'=> $id)))->row();

        $jenis_kelamin = array(
			'' => '-- Pilih --',
			'Laki-laki' => 'Laki-laki',
			'Perempuan' => 'Perempuan'
		);

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_freelancer" value="'.$freelancer->id_freelancer.'">
            <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama_freelancer" class="form-control" value="'.$freelancer->nama_freelancer.'" placeholder="Nama">
            </div>
            <div class="form-group">
            <label>Jenis Kelamin</label>
            '.form_dropdown('jenis_kelamin', $jenis_kelamin, $freelancer->jenis_kelamin,'class="form-control"').'
            </div>
            <div class="form-group">
            <label>No Telepon</label>
            <input type="text" name="no_telp" class="form-control" value="'.$freelancer->no_telp.'" placeholder="No Telepon">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="'.$freelancer->email.'" placeholder="Email">
            </div>
            <div class="form-group">
            <label>Alamat</label>
            <textarea type="text" name="alamat" class="form-control" placeholder="Alamat">'.$freelancer->alamat.'</textarea>
            </div>
            <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="'.$freelancer->username.'" placeholder="Username">
            </div>
            <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" value="'.$freelancer->password.'" placeholder="Password">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/data/freelancer';
        $data['link'] = ''.site_url().'cms/freelancer/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_freelancer = $this->input->post('id_freelancer');
	    $nama_freelancer = $this->input->post('nama_freelancer');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');

        $par = array(
            'tabel'=>'freelancer',
            'data'=>array(
            'nama_freelancer'=>$nama_freelancer,
            'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
            'email'=>$email,
            'alamat'=>$alamat,
            'username'=>$username,
            'password'=>$password,
            'role'=> 3
            ),
        );

        if($id_freelancer != NULL) $par['where'] = array('id_freelancer'=>$id_freelancer);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/data/freelancer');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('freelancer','id_freelancer',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/data/freelancer');
	}

  function cetak() {

        $from = array(
          'freelancer f' => '',
        );

        $freelancer = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'f.nama_freelancer ASC'));
        
        $table = '
        <table class="table table-condensed table-bordered table-stripped table-hover no-margin" style=" vertical-align:middle;">
            <tbody>';


        $table .= '

            <tr>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">No.</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Nama</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Jenis Kelamin</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">No Telepon</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Email</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Alamat</td>
            </tr>';
        
          $no = 1;
        
          foreach($freelancer->result() as $free) {

          $table .='
					<tr>
					<td width="50" style="text-align:center">'.$no.'</td>
					<td width="200" class="text-left">'.$free->nama_freelancer.'</td>
					<td width="200" class="text-left">'.$free->jenis_kelamin.'</td>
					<td width="100" class="text-left">'.$free->no_telp.'</td>
					<td width="500" class="text-left">'.$free->email.'</td>
					<td width="500" class="text-left">'.$free->alamat.'</td>
					
					</tr>';


                $no++;
                    
                }
        
          $table .='
  
              </tbody>
          </table>
          ';

        $data['tabel']	= $table;
        
        $this->load->view('print_view',$data);
    }

}
