<?php

class Semester Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Semester";
		$data['title'] = "Data Semester";

        $from = array(
			'ref_semester rs' => '',
		);

        $ref_semester = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rs.ref_semester ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_semester->result() as $kat){

            $edit = '<a href="'.site_url().'cms/referensi/semester/ubah/'.$kat->id_ref_semester.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$kat->id_ref_semester.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$kat->ref_semester.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$kat->id_ref_semester.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/semester/hapus/'.$kat->id_ref_semester.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Semester</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/semester/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Semester";
		$data['title'] = "Tambah Semester";

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Semester</label>
            <input name="ref_semester" type="text" class="form-control" placeholder="Semester">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/semester';
		$data['link'] = ''.site_url().'cms/semester/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Semester";
		$data['title'] = "Ubah Semester";

        $from = array(
			'ref_semester rs' => '',
		);

        $ref_semester = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rs.id_ref_semester'=> $id)))->row();

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_semester" value="'.$ref_semester->id_ref_semester.'">
            <div class="form-group">
            <label>Semester</label>
            <input type="text" name="ref_semester" class="form-control" value="'.$ref_semester->ref_semester.'" placeholder="Semester">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/semester';
        $data['link'] = ''.site_url().'cms/semester/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_semester = $this->input->post('id_ref_semester');
	    $ref_semester = $this->input->post('ref_semester');

        $par = array(
            'tabel'=>'ref_semester',
            'data'=>array(
            'ref_semester'=>$ref_semester
            ),
        );

        if($id_ref_semester != NULL) $par['where'] = array('id_ref_semester'=>$id_ref_semester);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/semester');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_semester','id_ref_semester',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/semester');
	}

}
