<?php

class Pembayaran Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
		$data['title'] = "Pembayaran";
		$data['breadcrumb'] = "Pembayaran";

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Tanggal</th>
                <th>No Invoice</th>
                <th>Nama Peserta</th>
                <th>Kursus</th>
                <th>Keterangan</th>
                <th>Bukti Transfer</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/pembayaran/cetak" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Cetak</a>';
		$data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

}
