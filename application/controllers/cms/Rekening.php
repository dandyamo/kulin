<?php

class Rekening Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Rekening";
		$data['title'] = "Data Rekening";

        $from = array(
			'ref_rekening rr' => '',
		);

        $ref_rekening = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rr.nama_rekening ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_rekening->result() as $rek){

            if($rek->status == 1) {
                $statusx = anchor(site_url('cms/rekening/tidak_aktif/'.$rek->id_ref_rekening.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('cms/rekening/aktif/'.$rek->id_ref_rekening.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'cms/referensi/rekening/ubah/'.$rek->id_ref_rekening.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$rek->id_ref_rekening.'"><i class="fa fa-trash"></i></a>';


            $photo = !empty($rek->logo_rekening) ? FCPATH.'uploads/bank/'.$rek->logo_rekening : null;
            $background = (file_exists($photo) and !empty($rek->logo_rekening)) ? base_url().'uploads/bank/'.$rek->logo_rekening : base_url().'assets/backend/dist/img/credit-card.png';

            if($this->session->userdata('role') == 1) {
              $table .='<tr>
              <td>'.$no.'</td>
              <td><center>'.$statusx.'</center></td>
              <td>'.$rek->nama_bank.'</td>
              <td>'.$rek->nama_rekening.'</td>
              <td>'.$rek->no_rekening.'</td>
              <td><center><img style="width:45px;" src="'.$background.'"></center></td>
              <td>'.$edit.' '.$hapus.'</td>
              </tr>';
              } else if($this->session->userdata('role') == 2) {
                $table .='<tr>
                <td>'.$no.'</td>
                <td><center>'.$statusx.'</center></td>
                <td>'.$rek->nama_bank.'</td>
                <td>'.$rek->nama_rekening.'</td>
                <td>'.$rek->no_rekening.'</td>
                <td><center><img style="width:45px;" src="'.$background.'"></center></td>
                </tr>';
              }

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$rek->id_ref_rekening.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/rekening/hapus/'.$rek->id_ref_rekening.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        if($this->session->userdata('role') == 1) {
          $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30x">Status</th>
                <th>Nama Bank</th>
                <th>Atas Nama</th>
                <th>No Rekening</th>
                <th>Logo</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

            $data['tombol'] = '<a href="'.site_url().'cms/referensi/rekening/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
          } else if($this->session->userdata('role') == 2) {
            $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30x">Status</th>
                <th>Nama Bank</th>
                <th>Atas Nama</th>
                <th>No Rekening</th>
                <th>Logo</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';
          }

        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{

    $data['breadcrumb'] = "Tambah Rekening";
    $data['title'] = "Tambah Rekening";

    $data['form_data'] = '
    <div class="card-body">
        <div class="form-group">
        <label>Nama Bank</label>
        <input name="nama_bank" type="text" class="form-control" placeholder="Nama Bank">
        </div>
        <div class="form-group">
        <label>Atas Nama</label>
        <input name="nama_rekening" type="text" class="form-control" placeholder="Atas Nama">
        </div>
        <div class="form-group">
        <label>No Rekening</label>
        <input name="no_rekening" type="text" class="form-control" placeholder="No Rekening">
        </div>
        <div class="form-group">
        <label>Logo Rekening</label>
        <input name="logo_rekening" type="file" class="form-control" style="line-height: 1.1;">
        </div>
    </div>
    <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/rekening';
		$data['link'] = ''.site_url().'cms/rekening/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Rekening";
		$data['title'] = "Ubah Rekening";

        $from = array(
			'ref_rekening rr' => '',
		);

        $ref_rekening = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rr.id_ref_rekening'=> $id)))->row();

        if ($id!=NULL) {
            $logo = '<p><a href="'.base_url('uploads/bank/'.@$ref_rekening->logo_rekening).'" width="300">'.@$ref_rekening->logo_rekening.'</a></p>';
        }

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_rekening" value="'.$ref_rekening->id_ref_rekening.'">
            '.form_hidden('val_logo_rekening', @$ref_rekening->logo_rekening, 'class="form-control"').'
            <div class="form-group">
            <label>Nama Bank</label>
            <input type="text" name="nama_bank" class="form-control" value="'.$ref_rekening->nama_bank.'" placeholder="Nama Bank">
            </div>
            <div class="form-group">
            <label>Atas Nama</label>
            <input name="nama_rekening" type="text" class="form-control" value="'.$ref_rekening->nama_rekening.'" placeholder="Atas Nama">
            </div>
            <div class="form-group">
            <label>No Rekening</label>
            <input name="no_rekening" type="text" class="form-control" value="'.$ref_rekening->no_rekening.'" placeholder="No Rekening">
            </div>
            <div class="form-group">
            <label>Logo Rekening</label>
            <input name="logo_rekening" type="file" class="form-control" style="line-height: 1.1;">
            '.$logo.'
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/rekening';
        $data['link'] = ''.site_url().'cms/rekening/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_rekening = $this->input->post('id_ref_rekening');
	    $nama_rekening = $this->input->post('nama_rekening');
	    $nama_bank = $this->input->post('nama_bank');
	    $no_rekening = $this->input->post('no_rekening');

        $par = array(
            'tabel'=>'ref_rekening',
            'data'=>array(
            'nama_rekening'=>$nama_rekening,
            'nama_bank'=>$nama_bank,
            'no_rekening'=>$no_rekening,
            'status'=> 1
            ),
        );

      $logo_rekening = $_FILES['logo_rekening']['tmp_name'];
			if($logo_rekening!=NULL){
				$path = './uploads/bank';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				if ($in['val_logo_rekening']!=NULL) {
					$prev = $in['val_logo_rekening'];
					$path_pasfoto = $path.'/'.$prev;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('logo_rekening');
			    $data = $this->upload->data();
			    $logo_rekening = $data['file_name'];
			    $par['data']['logo_rekening'] = $logo_rekening;
			}

        if($id_ref_rekening != NULL) $par['where'] = array('id_ref_rekening'=>$id_ref_rekening);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/rekening');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_rekening',$data,'id_ref_rekening',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/referensi/rekening');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_rekening',$data,'id_ref_rekening',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/referensi/rekening');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_rekening','id_ref_rekening',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/rekening');
	}

}
