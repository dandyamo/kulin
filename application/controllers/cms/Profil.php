<?php

class Profil Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
		$data['title'] = "Profil";
		$data['breadcrumb'] = "Profil";
        $operator = $this->Model_general->getoperator();

        $jenis_kelamin = array(
			'' => '-- Pilih --',
			'Laki-laki' => 'Laki-laki',
			'Perempuan' => 'Perempuan'
		);

        $data['data'] ='
        <!-- About Me Box -->
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Tentang Saya</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong><i class="fas fa-user mr-1"></i> Jenis Kelamin</strong>
        
            <p class="text-muted">
                '.$operator->jenis_kelamin.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-phone mr-1"></i> No Telepon</strong>
        
            <p class="text-muted">
                '.$operator->no_telp.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-envelope mr-1"></i> Email</strong>
        
            <p class="text-muted">
                '.$operator->email.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
        
            <p class="text-muted">'.$operator->alamat.'</p>
        
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
        <div class="card">
            <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#profil" data-toggle="tab">Ubah Profil</a></li>
                <li class="nav-item"><a class="nav-link" href="#password" data-toggle="tab">Ubah Password</a></li>
            </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
            <div class="tab-content">
                <div class="active tab-pane" id="profil">
                <form class="form-horizontal" action="'.site_url().'cms/profil/simpan_profil" method="post">
                    <input type="hidden" name="id_operator" value="'.$operator->id_operator.'">
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_operator" value="'.$operator->nama_operator.'" placeholder="Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        '.form_dropdown('jenis_kelamin', $jenis_kelamin, $operator->jenis_kelamin,'class="form-control"').'
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">No Telepon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="no_telp" value="'.$operator->no_telp.'" placeholder="No Telepon">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" value="'.$operator->email.'" placeholder="Email">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Alamat">'.$operator->alamat.'</textarea>
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" value="'.$operator->username.'" placeholder="Username">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="password">
                <form class="form-horizontal" action="'.site_url().'cms/profil/simpan_password" method="post">
                    <input type="hidden" name="id_operator" value="'.$operator->id_operator.'">
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_baru" placeholder="Password Baru">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Konfirmasi Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_konfirm" placeholder="Konfirmasi Password Baru">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->';

		$data['content'] = "cms/profil";
		$this->load->view('home', $data);
	}

    public function simpan_profil()
	{
	    $id_operator = $this->input->post('id_operator');
	    $nama_operator = $this->input->post('nama_operator');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');

        $par = array(
            'tabel'=>'operator',
            'data'=>array(
            'nama_operator'=>$nama_operator,
            'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
            'email'=>$email,
            'alamat'=>$alamat,
            'username'=>$username
            ),
        );

        if($id_operator != NULL) $par['where'] = array('id_operator'=>$id_operator);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/profil');

	}

    public function simpan_password()
	{
	    $id_operator = $this->input->post('id_operator');
	    $password_baru = $this->input->post('password_baru');
	    $password_konfirm = $this->input->post('password_konfirm');


        if($password_baru == $password_konfirm){

            $par = array(
                'tabel'=>'operator',
                'data'=>array(
                'password'=>$password_konfirm
                ),
            );
    
            if($id_operator != NULL) $par['where'] = array('id_operator'=>$id_operator);
    
            $sim = $this->Model_general->save_data($par);
    
            $this->session->set_flashdata('ok', 'Password Berhasil Disimpan');
            
            redirect('cms/profil');

        } else {

            $this->session->set_flashdata('fail', 'Password Tidak Cocok! Cek kembali');

            redirect('cms/profil');

        }

	}

}
