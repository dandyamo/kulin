<?php

class Peserta Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Peserta";
		$data['title'] = "Data Peserta";

		$from = array(
			'peserta p' => '',
		);

        $peserta = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'p.nama_peserta ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($peserta->result() as $tut){

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$tut->nama_peserta.'</td>
            <td>'.$tut->jenis_kelamin.'</td>
            <td>'.$tut->no_telp.'</td>
            <td>'.$tut->email.'</td>
            <td>'.$tut->alamat.'</td>
            </tr>';

            $no++;
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>No Telepon</th>
                <th>Email</th>
                <th>Alamat</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

		$data['tombol'] = '<a target="_blank" href="'.site_url().'cms/peserta/cetak" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Cetak</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	function cetak() {

        $from = array(
          'peserta p' => '',
        );

        $peserta = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'p.nama_peserta ASC'));
        
        $table = '
        <table class="table table-condensed table-bordered table-stripped table-hover no-margin" style=" vertical-align:middle;">
            <tbody>';


        $table .= '

            <tr>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">No.</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Nama</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Jenis Kelamin</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">No Telepon</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Email</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Alamat</td>
            </tr>';
        
          $no = 1;
        
          foreach($peserta->result() as $free) {

          $table .='
					<tr>
					<td width="50" style="text-align:center">'.$no.'</td>
					<td width="200" class="text-left">'.$free->nama_peserta.'</td>
					<td width="200" class="text-left">'.$free->jenis_kelamin.'</td>
					<td width="100" class="text-left">'.$free->no_telp.'</td>
					<td width="500" class="text-left">'.$free->email.'</td>
					<td width="500" class="text-left">'.$free->alamat.'</td>
					
					</tr>';


                $no++;
                    
                }
        
          $table .='
  
              </tbody>
          </table>
          ';

        $data['tabel']	= $table;
        
        $this->load->view('print_view',$data);
    }

}
