<?php

class Jadwal Extends CI_Controller{

    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['title'] = "Jadwal";
		$data['breadcrumb'] = "Jadwal";


        $data['table'] = '
        <table class="table table-bordered" id="example2">
            <thead>
            <tr>
            <th width="10px">No</th>
            <th>Status</th>
            <th>Tanggal</th>
            <th>Nama Peserta</th>
            <th>Kursus</th>
            <th>Nama Tentor</th>
            <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            <tr style="background:#00FF00">
            <td>1</td>
            <td><center><a href="#" class="btn btn-xs btn-default">Berlangsung</a></center></td>
            <td>11-7-2014</td>
            <td>John Doe</td>
            <td>John Doe</td>
            <td>John Doe</td>
            <td></td>
            </tr>
            <tr style="background:#F0E68C">
            <td>2</td>
            <td><center><a href="#" class="btn btn-xs btn-default">Rencana</a></center></td>
            <td>11-7-2014</td>
            <td>John Doe</td>
            <td>John Doe</td>
            <td>John Doe</td>
            <td></td>
            </tr>
            </tbody>
        </table>';

		$data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

}
