<?php

class Kategori_artikel Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Kategori Artikel";
		$data['title'] = "Data Kategori Artikel";

        $from = array(
			'ref_kategori_artikel rk' => '',
		);

        $ref_kategori = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rk.kategori_artikel ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_kategori->result() as $kat){

            $edit = '<a href="'.site_url().'cms/referensi/kategori_artikel/ubah/'.$kat->id_kategori_artikel.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$kat->id_kategori_artikel.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$kat->kategori_artikel.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$kat->id_kategori_artikel.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/kategori/hapus/'.$kat->id_kategori_artikel.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Kategori</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/kategori_artikel/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Kategori Artikel";
		$data['title'] = "Tambah Kategori Artikel";

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Kategori Artikel</label>
            <input name="kategori_artikel" type="text" class="form-control" placeholder="Kategori Artikel">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/kategori_artikel';
		$data['link'] = ''.site_url().'cms/kategori_artikel/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Kategori Artikel";
		$data['title'] = "Ubah Kategori Artikel";

        $from = array(
			'ref_kategori_artikel rk' => '',
		);

        $ref_kategori = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rk.id_kategori_artikel'=> $id)))->row();

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_kategori_artikel" value="'.$ref_kategori->id_kategori_artikel.'">
            <div class="form-group">
            <label>Kategori Artikel</label>
            <input type="text" name="kategori_artikel" class="form-control" value="'.$ref_kategori->kategori_artikel.'" placeholder="Kategori">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/kategori_artikel';
        $data['link'] = ''.site_url().'cms/kategori_artikel/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_kategori_artikel = $this->input->post('id_kategori_artikel');
	    $kategori_artikel = $this->input->post('kategori_artikel');

        $par = array(
            'tabel'=>'ref_kategori_artikel',
            'data'=>array(
            'kategori_artikel'=>$kategori_artikel,
            'status'=> '1'
            ),
        );

        if($id_kategori_artikel != NULL) $par['where'] = array('id_kategori_artikel'=>$id_kategori_artikel);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/kategori_artikel');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_kategori_artikel','id_kategori_artikel',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/kategori_artikel');
	}

}
