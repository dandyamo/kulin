<?php

class Tentor Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
      $this->list_data();
	}

  public function list_data($aktif = NULL) {
    $data['breadcrumb'] = "Data Tentor";
		$data['title'] = "Data Tentor";


    if ($aktif == null) {
      $aktif = 1;
    }

    if($aktif == 1){

        $from = array(
			'tentor t' => '',
      'ref_matkul rm' => array('rm.id_ref_matkul = t.id_ref_matkul','left'),
		);

        $tentor = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'t.nama_tentor ASC','where'=>array('t.id_ref_kategori'=> 1)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($tentor->result() as $tut){

            $edit = '<a href="'.site_url().'cms/data/tentor/ubah/'.$tut->id_tentor.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$tut->id_tentor.'"><i class="fa fa-trash"></i></a>';
            $view = '<a href="#" class="btn btn-info btn-xs" title="Klik untuk melihat rekening" data-toggle="modal" data-target="#view'.$tut->id_tentor.'"><i class="fa fa-list-alt"></i></a>';
            
            $rek_tentor = $this->Model_general->getdata(array('tabel'=>'rek_tentor', 'order'=>'bank_tentor ASC','where'=>array('id_tentor'=>$tut->id_tentor)))->result();

            $tab_rek = '';
            foreach($rek_tentor as $rek){

            $tab_rek .='<tr>
            <td>'.$no.'</td>
            <td>'.$rek->bank_tentor.'</td>
            <td>'.$rek->an_tentor.'</td>
            <td>'.$rek->rek_tentor.'</td>
            </tr>';

            }

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$tut->nama_tentor.'</td>
            <td>'.$tut->jenis_kelamin.'</td>
            <td>'.$tut->no_telp.'</td>
            <td>'.$tut->email.'</td>
            <td>'.$tut->alamat.'</td>
            <td>'.$tut->ref_matkul.'</td>
            <td>'.$view.' '.$edit.'  '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$tut->id_tentor.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/tentor/hapus/'.$tut->id_tentor.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          
          <div class="modal fade" id="view'.$tut->id_tentor.'">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <h4 class="modal-title"><i class="fa fa-list-alt"></i> List Rekening</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th style="width: 10px">No</th>
                        <th>Bank</th>
                        <th>Atas Nama</th>
                        <th>No Rekening</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tab_rek.'
                    </tbody>
                    </table>
                  </div>
                  <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
                
                    </div>
                </div>
                <!-- /.modal-content -->
              </div>
              <!-- /.modal-dialog -->
            </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>No Telepon</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>Tentor</th>
                <th width="75px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/data/tentor/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a> <a target="_blank" href="'.site_url().'cms/tentor/cetak" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Cetak</a>';

      } else {

            $from = array(
              'tentor t' => '',
              'ref_umum ru' => array('ru.id_ref_umum = t.id_ref_umum','left'),
            );
    
            $tentor = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'t.nama_tentor ASC','where'=>array('t.id_ref_kategori'=> 2)));
    
            $table = '';
            $data['modal'] = '';
            $no = 1;
            foreach($tentor->result() as $tut){
    
                $edit = '<a href="'.site_url().'cms/data/tentor/ubah/'.$tut->id_tentor.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
                $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$tut->id_tentor.'"><i class="fa fa-trash"></i></a>';
                $view = '<a href="#" class="btn btn-info btn-xs" title="Klik untuk melihat rekening" data-toggle="modal" data-target="#view'.$tut->id_tentor.'"><i class="fa fa-list-alt"></i></a>';
                
                $rek_tentor = $this->Model_general->getdata(array('tabel'=>'rek_tentor', 'order'=>'bank_tentor ASC','where'=>array('id_tentor'=>$tut->id_tentor)))->result();
    
                $tab_rek = '';
                foreach($rek_tentor as $rek){
    
                $tab_rek .='<tr>
                <td>'.$no.'</td>
                <td>'.$rek->bank_tentor.'</td>
                <td>'.$rek->an_tentor.'</td>
                <td>'.$rek->rek_tentor.'</td>
                </tr>';
    
                }
    
                $table .='<tr>
                <td>'.$no.'</td>
                <td>'.$tut->nama_tentor.'</td>
                <td>'.$tut->jenis_kelamin.'</td>
                <td>'.$tut->no_telp.'</td>
                <td>'.$tut->email.'</td>
                <td>'.$tut->alamat.'</td>
                <td>'.$tut->ref_umum.'</td>
                <td> '.$view.' '.$edit.' '.$hapus.'</td>
                </tr>';
    
                $no++;
    
                $data['modal'] .= '
                <div class="modal fade" id="hapus'.$tut->id_tentor.'">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>Yakin ini menghapus data ini?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                      <a href="'.site_url().'cms/tentor/hapus/'.$tut->id_tentor.'" class="btn btn-success">Hapus</a>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
              
              <div class="modal fade" id="view'.$tut->id_tentor.'">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="modal-title"><i class="fa fa-list-alt"></i> List Rekening</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">×</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <table class="table table-bordered">
                        <thead>
                            <tr>
                            <th style="width: 10px">No</th>
                            <th>Bank</th>
                            <th>Atas Nama</th>
                            <th>No Rekening</th>
                            </tr>
                        </thead>
                        <tbody>
                            '.$tab_rek.'
                        </tbody>
                        </table>
                      </div>
                      <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
                    
                        </div>
                    </div>
                    <!-- /.modal-content -->
                  </div>
                  <!-- /.modal-dialog -->
                </div>';
            }
    
            $data['table'] = '
                <table class="table table-bordered" id="example1">
                  <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>No Telepon</th>
                    <th>Email</th>
                    <th>Alamat</th>
                    <th>Tentor</th>
                    <th width="75px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    '.$table.'
                  </tbody>
                </table>';
    
            $data['tombol'] = '<a href="'.site_url().'cms/data/tentor/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>  <a target="_blank" href="'.site_url().'cms/tentor/cetak" class="btn btn-sm btn-warning"><i class="fa fa-print"></i> Cetak</a>';

      }
        $data['content'] = "cms/tentor_view";
		    $this->load->view('home', $data);
  }

  public function tambah()
  {
        $data['breadcrumb'] = "Tambah Tentor";
        $data['title'] = "Tambah Tentor";

        $kategori = $this->Model_general->combo_box(array('tabel'=>'ref_kategori', 'key'=>'id_ref_kategori', 'val'=>array('ref_kategori')));

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Nama</label>
            <input name="nama_tentor" type="text" class="form-control" placeholder="Nama">
            </div>
            <div class="form-group">
            <label>Jenis Kelamin</label>
            <select name="jenis_kelamin" class="form-control">
                <option>-- Pilih --</option>
                <option>Laki-laki</option>
                <option>Perempuan</option>
            </select>
            </div>
            <div class="form-group">
            <label>No Telepon</label>
            <input name="no_telp" type="text" class="form-control" placeholder="No Telepon">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input name="email" type="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" type="text" class="form-control" placeholder="Alamat"></textarea>
            </div>
            <div class="form-group">
            <label>Username</label>
            <input name="username" type="text" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
            <label>Password</label>
            <input name="password" type="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
            <label>Kategori</label>
            '.form_dropdown('id_ref_kategori', $kategori, '','class="form-control" id="id_ref_kategori"').'
            </div>
            <div class="form-group">
            <label>Kursus</label>
            <select class="form-control" id="id_kursus" name="id_kursus">
            <option value="">Pilih Kursus</option>
            </select>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['back'] = ''.site_url().'cms/data/tentor';
        $data['link'] = ''.site_url().'cms/tentor/simpan';
        $data['content'] = "cms/form_view";
        $this->load->view('home', $data);
  }

  public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Tentor";
        $data['title'] = "Ubah Tentor";

            $from = array(
            'tentor t' => '',
            'ref_kategori rk' => array('rk.id_ref_kategori=t.id_ref_kategori','left')
        );

        $tentor = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('t.id_tentor'=> $id)))->row();


        $kategori = $this->Model_general->combo_box(array('tabel'=>'ref_kategori', 'key'=>'id_ref_kategori', 'val'=>array('ref_kategori')));

                $jenis_kelamin = array(
          '' => '-- Pilih --',
          'Laki-laki' => 'Laki-laki',
          'Perempuan' => 'Perempuan'
        );

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_tentor" value="'.$tentor->id_tentor.'">
            <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama_tentor" class="form-control" value="'.$tentor->nama_tentor.'" placeholder="Nama">
            </div>
            <div class="form-group">
            <label>Jenis Kelamin</label>
            '.form_dropdown('jenis_kelamin', $jenis_kelamin, $tentor->jenis_kelamin,'class="form-control"').'
            </div>
            <div class="form-group">
            <label>No Telepon</label>
            <input type="text" name="no_telp" class="form-control" value="'.$tentor->no_telp.'" placeholder="No Telepon">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="'.$tentor->email.'" placeholder="Email">
            </div>
            <div class="form-group">
            <label>Alamat</label>
            <textarea type="text" name="alamat" class="form-control" placeholder="Alamat">'.$tentor->alamat.'</textarea>
            </div>
            <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="'.$tentor->username.'" placeholder="Username">
            </div>
            <div class="form-group">
            <label>Kategori</label>
            '.form_dropdown('id_ref_kategori', $kategori, $tentor->id_ref_kategori,'class="form-control" id="id_ref_kategori"').'
            </div>
            <div class="form-group">
            <label>Kursus</label>
            <select class="form-control" id="id_kursus" name="id_kursus">
            <option value="">Pilih Kursus</option>
            </select>
            </div>
        </div>
        <!-- /.card-body -->';


        $data['script'] = "<script type='text/javascript'>
    $(document).ready(function(){
        $('#id_ref_kategori').change(function(){
            var id=$(this).val();

          if(id == 1){

            $.ajax({

            url : '".site_url()."tentor/login/get_matkul',
            method : 'POST',
            data : {id: id},
            async : false,
            dataType : 'JSON',
            success: function(data){
                var html = '';
                var i;
                for(i=0; i<data.length; i++){
                    html += '<option value=''+data[i].id_ref_matkul+''>'+data[i].ref_matkul+'</option>';
                }
                $('#id_kursus').html(html);
                
            }

            });

            } else {

              $.ajax({

              url : '".site_url()."tentor/login/get_umum',
              method : 'POST',
              data : {id: id},
              async : false,
              dataType : 'JSON',
              success: function(data){
                  var html = '';
                  var i;
                  for(i=0; i<data.length; i++){
                      html += '<option value=''+data[i].id_ref_umum+''>'+data[i].ref_umum+'</option>';
                  }
                  $('#id_kursus').html(html);
                  
              }

              });

            }
           
        });
    });
</script>";

		$data['back'] = ''.site_url().'cms/data/tentor';
    $data['link'] = ''.site_url().'cms/tentor/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_tentor = $this->input->post('id_tentor');
      $nama_tentor = $this->input->post('nama_tentor');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');
	    $id_ref_kategori = $this->input->post('id_ref_kategori');
    	$id_kursus = $this->input->post('id_kursus');

      if($id_ref_kategori == 1) {

        $par = array(
          'tabel'=>'tentor',
          'data'=>array(
          'nama_tentor'=>$nama_tentor,
          'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
          'email'=>$email,
          'alamat'=>$alamat,
          'username'=>$username,
          'password'=>md5($password),
          'id_ref_kategori'=>$id_ref_kategori,
          'id_ref_matkul'=>$id_kursus,
          'role'=> 4
          ),
        );
  
      } else {
  
        $par = array(
          'tabel'=>'tentor',
          'data'=>array(
          'nama_tentor'=>$nama_tentor,
          'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
          'email'=>$email,
          'alamat'=>$alamat,
          'username'=>$username,
          'password'=>md5($password),
          'id_ref_kategori'=>$id_ref_kategori,
          'id_ref_umum'=>$id_kursus,
          'role'=> 4
          ),
        );
  
      }
        if($id_tentor != NULL) $par['where'] = array('id_tentor'=>$id_tentor);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/data/tentor');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('tentor','id_tentor',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/data/tentor');
	}

    function cetak() {

        $from = array(
          'tentor t' => '',
        );

        $tentor = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'t.nama_tentor ASC'));
        
        $table = '
        <table class="table table-condensed table-bordered table-stripped table-hover no-margin" style=" vertical-align:middle;">
            <tbody>';


        $table .= '

            <tr>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">No.</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Nama</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Jenis Kelamin</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">No Telepon</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Email</td>
            <td style="font-weight:bold;vertical-align:middle;" class="text-center">Alamat</td>
            </tr>';
        
          $no = 1;
        
          foreach($tentor->result() as $free) {

          $table .='
					<tr>
					<td width="50" style="text-align:center">'.$no.'</td>
					<td width="200" class="text-left">'.$free->nama_tentor.'</td>
					<td width="200" class="text-left">'.$free->jenis_kelamin.'</td>
					<td width="100" class="text-left">'.$free->no_telp.'</td>
					<td width="500" class="text-left">'.$free->email.'</td>
					<td width="500" class="text-left">'.$free->alamat.'</td>
					
					</tr>';


                $no++;
                    
                }
        
          $table .='
  
              </tbody>
          </table>
          ';

        $data['tabel']	= $table;
        
        $this->load->view('print_view',$data);
    }

    function get_matkul(){
      $id_ref_kategori=$this->input->post('id');

  $from = array(
    'ref_matkul rm' => '',
    'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left')
  );

  $select = 'rm.*,rf.id_ref_kategori';
  $data = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rm.ref_matkul ASC','where'=>array('rf.id_ref_kategori'=>$id_ref_kategori)))->result();
      echo json_encode($data);
  }

function get_umum(){
      $id_ref_kategori=$this->input->post('id');

  $from = array(
    'ref_umum ru' => '',
    'ref_kat_umum rku' => array('rku.id_ref_kat_umum = ru.id_ref_kat_umum','left')
  );

  $select = 'ru.*,rku.id_ref_kategori';
  $data = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ru.ref_umum ASC','where'=>array('rku.id_ref_kategori'=>$id_ref_kategori)))->result();
      echo json_encode($data);
  }

}
