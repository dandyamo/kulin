<?php

class Video Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

  public function index()
	{
    $this->list_data();
	}

	public function list_data($aktif = NULL,$status = 1)
	{
		$data['title'] = "Video";
		$data['breadcrumb'] = "Video";

    if ($aktif == null) {
      $aktif = 1;
    }

    if($aktif == 1){

      $from = array(
        'video v' => '',
        'freelancer f' => array('f.id_freelancer=v.id_freelancer','left')
      );

      $video = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'v.nama_video ASC','where'=>array('v.id_ref_kategori'=>1)));

      $table = '';
      $data['modal'] = '';
      $no = 1;
      foreach($video->result() as $vid){

          $view = '<a href="'.site_url().'cms/data/video/detail/'.$vid->id_video.'" class="btn btn-info btn-xs" title="Klik untuk melihat video"><i class="fa fa-video"></i></a>';

          if($vid->status == 1) {
              $verifikasi = '<center><i class="fa fa-star"></i></center>';
              $acc = '<a href="#" data-toggle="modal" data-target="#b_konfirmasi'.$vid->id_video.'" class="btn btn-xs btn-success" title="Klik untuk membatalkan data"><i class="fa fa-check-square"></i></a>';
              $harga = 'Rp. '.@number_format($vid->harga_sell,2,",",".").'';
          }else{
              $verifikasi = '';
              $acc = '<a href="'.site_url().'cms/data/video/konfirmasi/'.$vid->id_video.'" class="btn btn-xs btn-default" title="Klik untuk konfirmasi data"><i class="fa fa-square"></i></a>';
              $harga = 'Rp. '.@number_format($vid->harga_buy,2,",",".").'';
          }

          if($this->session->userdata('role') == 1){

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$verifikasi.'</td>
            <td>'.$vid->nama_video.'</td>
            <td>'.$vid->nama_freelancer.'</td>
            <td>'.$harga.'</td>
            <td>'.$vid->ukuran.' KB</td>
            <td>'.$vid->views.'</td>
            <td>'.$view.' '.$acc.'</td>
            </tr>';


          } else {

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$vid->nama_video.'</td>
            <td>'.$vid->nama_freelancer.'</td>
            <td>'.$vid->ukuran.' KB</td>
            <td>'.$vid->views.'</td>
            <td>'.$view.'</td>
            </tr>';

          }

          $no++;

          $data['modal'] .= '

          <!-- Modal Batal Konfirmasi -->
          <div class="modal fade" id="b_konfirmasi'.$vid->id_video.'">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-info"></i> Konfirmasi </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Yakin untuk membatalkan konfirmasi video?</p>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <a href="'.site_url().'cms/video/batal_konfirmasi/'.$vid->id_video.'" class="btn btn-success">Simpan</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      ';
      }

      if($this->session->userdata('role') == 1){

        $data['table'] = '
        <table class="table table-bordered" id="example1">
          <thead>
          <tr>
            <th width="10px">No</th>
            <th width="10px"></th>
            <th>Nama Video</th>
            <th>Nama Pengunggah</th>
            <th>Harga</th>
            <th>Ukuran File</th>
            <th>Views</th>
            <th width="20px">Aksi</th>
          </tr>
          </thead>
          <tbody>
            '.$table.'
          </tbody>
        </table>';



      } else {

        $data['table'] = '
        <table class="table table-bordered" id="example1">
          <thead>
          <tr>
            <th width="10px">No</th>
            <th>Nama Video</th>
            <th>Nama Pengunggah</th>
            <th>Ukuran File</th>
            <th>Views</th>
            <th width="20px">Aksi</th>
          </tr>
          </thead>
          <tbody>
            '.$table.'
          </tbody>
        </table>';


      }

    } else {

      $from = array(
        'video v' => '',
        'freelancer f' => array('f.id_freelancer=v.id_freelancer','left')
      );

      $video = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'v.nama_video ASC','where'=>array('v.id_ref_kategori'=>2)));

      $table = '';
      $data['modal'] = '';
      $no = 1;
      foreach($video->result() as $vid){

          $view = '<a href="'.site_url().'cms/data/video/detail_umum/'.$vid->id_video.'" class="btn btn-info btn-xs" title="Klik untuk melihat video"><i class="fa fa-video"></i></a>';

          if($vid->status == 1) {
              $verifikasi = '<center><i class="fa fa-star"></i></center>';
              $acc = '<a href="#" data-toggle="modal" data-target="#b_konfirmasi'.$vid->id_video.'" class="btn btn-xs btn-success" title="Klik untuk membatalkan data"><i class="fa fa-check-square"></i></a>';
              $harga = 'Rp. '.@number_format($vid->harga_sell,2,",",".").'';
          }else{
              $verifikasi = '';
              $acc = '<a href="'.site_url().'cms/data/video/konfirmasi/'.$vid->id_video.'" class="btn btn-xs btn-default" title="Klik untuk konfirmasi data"><i class="fa fa-square"></i></a>';
              $harga = 'Rp. '.@number_format($vid->harga_buy,2,",",".").'';
          }

          if($this->session->userdata('role') == 1){

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$verifikasi.'</td>
            <td>'.$vid->nama_video.'</td>
            <td>'.$vid->nama_freelancer.'</td>
            <td>'.$harga.'</td>
            <td>'.$vid->ukuran.' KB</td>
            <td>'.$vid->views.'</td>
            <td>'.$view.' '.$acc.'</td>
            </tr>';


          } else {

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$vid->nama_video.'</td>
            <td>'.$vid->nama_freelancer.'</td>
            <td>'.$vid->ukuran.' KB</td>
            <td>'.$vid->views.'</td>
            <td>'.$view.'</td>
            </tr>';

          }

          $no++;

          $data['modal'] .= '

          <!-- Modal Batal Konfirmasi -->
          <div class="modal fade" id="b_konfirmasi'.$vid->id_video.'">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-info"></i> Konfirmasi </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Yakin untuk membatalkan konfirmasi video?</p>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                <a href="'.site_url().'cms/video/batal_konfirmasi/'.$vid->id_video.'" class="btn btn-success">Simpan</a>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
      ';
      }

      if($this->session->userdata('role') == 1){

        $data['table'] = '
        <table class="table table-bordered" id="example1">
          <thead>
          <tr>
            <th width="10px">No</th>
            <th width="10px"></th>
            <th>Nama Video</th>
            <th>Nama Pengunggah</th>
            <th>Ukuran File</th>
            <th>Views</th>
            <th width="20px">Aksi</th>
          </tr>
          </thead>
          <tbody>
            '.$table.'
          </tbody>
        </table>';



      } else {

        $data['table'] = '
        <table class="table table-bordered" id="example1">
          <thead>
          <tr>
            <th width="10px">No</th>
            <th>Nama Video</th>
            <th>Nama Pengunggah</th>
            <th>Ukuran File</th>
            <th>Views</th>
            <th width="20px">Aksi</th>
          </tr>
          </thead>
          <tbody>
            '.$table.'
          </tbody>
        </table>';


      }

    }

		$data['content'] = "cms/video_view";
		$this->load->view('home', $data);
	}

    public function detail($id = null)
	{
        $data['breadcrumb'] = "Detail Video";
		    $data['title'] = "Detail Video";

        $from = array(
          'video v' => '',
          'ref_matkul rm' => array('rm.id_ref_matkul = v.id_ref_matkul','left'),
          'ref_kategori rk' => array('rk.id_ref_kategori = v.id_ref_kategori','left'),
          'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left'),
          'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
          'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
		);

        $video = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('v.id_video'=> $id)))->row();

        $data['detail'] ='
        <!-- About Me Box -->
        <div class="row">
        <div class="col-lg-3">
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Detail Video</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong>Kategori</strong>
        
            <p class="text-muted">
                '.$video->ref_kategori.'
            </p>
        
            <hr>
        
            <strong>Fakultas</strong>
        
            <p class="text-muted">
                '.$video->ref_fakultas.'
            </p>
        
            <hr>
        
            <strong>Jurusan</strong>
        
            <p class="text-muted">
                '.$video->ref_jurusan.'
            </p>
        
            <hr>
        
            <strong>Semester</strong>
        
            <p class="text-muted">'.$video->ref_semester.'</p>
        
            <hr>
        
            <strong>Mata Kuliah</strong>
        
            <p class="text-muted">'.$video->ref_matkul.'</p>
        
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
            <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                  <h3>'.$video->nama_video.'</h3>
                  <video oncontextmenu="return false;" width="100%" height="350px" controls controlsList="nodownload">
                      <source src="'.base_url('uploads/file_video/'.urlencode($video->berkas_file)).'" type="video/mp4">
                  </video>
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->';

		$data['back'] = ''.site_url().'cms/data/video';
		$data['content'] = "cms/detail_view";
		$this->load->view('home', $data);
	}

  public function detail_umum($id = null)
	{
        $data['breadcrumb'] = "Detail Video";
		$data['title'] = "Detail Video";

        $from = array(
          'video v' => '',
          'ref_umum ru' => array('ru.id_ref_umum = v.id_ref_umum','left')
		);

        $video = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('v.id_video'=> $id)))->row();

        $data['detail'] ='
        <!-- About Me Box -->
        <div class="row">
        <div class="col-lg-3">
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Detail Video</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong>Kategori</strong>
        
            <p class="text-muted">
                '.$video->ref_kategori.'
            </p>
        
            <hr>
        
            <strong>Kategori</strong>
        
            <p class="text-muted">
                '.$video->ref_umum.'
            </p>
        
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
            <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                  <h3>'.$video->nama_video.'</h3>
                  <video oncontextmenu="return false;" width="100%" height="350px" controls controlsList="nodownload">
                      <source src="'.base_url('uploads/file_video/'.urlencode($video->berkas_file)).'" type="video/mp4">
                  </video>
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->';

		$data['back'] = ''.site_url().'cms/data/video';
		$data['content'] = "cms/detail_view";
		$this->load->view('home', $data);
	}

    public function konfirmasi($id = null)
	{
        $data['breadcrumb'] = "Konfirmasi video";
		$data['title'] = "Konfirmasi video";

        $id_cms = $this->session->userdata('id');

        $from = array(
			'video v' => '',
		);

        $video = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('v.id_video'=> $id)))->row();

        if ($id!=NULL) {
            $thumbnail = '<p><a href="'.base_url('uploads/thumbnail/'.@$video->thumbnail).'" width="300">'.@$video->thumbnail.'</a></p>';
            $file_video = '<p><a href="'.base_url('uploads/file_video/'.@$video->berkas_file).'" width="300">'.@$video->berkas_file.'</a></p>';
        }

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_video" value="'.$video->id_video.'">
            <div class="form-group">
            <label>Harga</label>
            <input name="harga" type="number" class="form-control" placeholder="Harga">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/data/video';
        $data['link'] = ''.site_url().'cms/video/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}


    public function simpan()
	{
	    $id_video = $this->input->post('id_video');
	    $harga = $this->input->post('harga');

        $par = array(
            'tabel'=>'video',
            'data'=>array(
            'harga_sell'=>$harga,
            'status'=> 1
            ),
        );

        if($id_video != NULL) $par['where'] = array('id_video'=>$id_video);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/data/video');

	}

  public function batal_konfirmasi($id = null)
	{

        $par = array(
            'tabel'=>'video',
            'data'=>array(
            'harga_sell'=>NULL,
            'status'=> 0
            ),
        );

        if($id != NULL) $par['where'] = array('id_video'=>$id);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/data/video');

	}

}
