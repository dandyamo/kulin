<?php

class Operator Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Operator";
		$data['title'] = "Data Operator";

        $from = array(
			'operator o' => '',
		);

        $operator = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'o.nama_operator ASC', 'where'=>array('role'=>2)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($operator->result() as $ope){

            $edit = '<a href="'.site_url().'cms/data/operator/ubah/'.$ope->id_operator.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$ope->id_operator.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$ope->nama_operator.'</td>
            <td>'.$ope->jenis_kelamin.'</td>
            <td>'.$ope->no_telp.'</td>
            <td>'.$ope->email.'</td>
            <td>'.$ope->alamat.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$ope->id_operator.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/operator/hapus/'.$ope->id_operator.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>No Telepon</th>
                <th>Email</th>
                <th>Alamat</th>
                <th>Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/data/operator/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Operator";
		$data['title'] = "Tambah Operator";

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Nama</label>
            <input name="nama_operator" type="text" class="form-control" placeholder="Nama">
            </div>
            <div class="form-group">
            <label>Jenis Kelamin</label>
            <select name="jenis_kelamin" class="form-control">
                <option>-- Pilih --</option>
                <option>Laki-laki</option>
                <option>Perempuan</option>
            </select>
            </div>
            <div class="form-group">
            <label>No Telepon</label>
            <input name="no_telp" type="text" class="form-control" placeholder="No Telepon">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input name="email" type="email" class="form-control" placeholder="Email">
            </div>
            <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" type="text" class="form-control" placeholder="Alamat"></textarea>
            </div>
            <div class="form-group">
            <label>Username</label>
            <input name="username" type="text" class="form-control" placeholder="Username">
            </div>
            <div class="form-group">
            <label>Password</label>
            <input name="password" type="password" class="form-control" placeholder="Password">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/data/operator';
		$data['link'] = ''.site_url().'cms/operator/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Operator";
		$data['title'] = "Ubah Operator";

        $from = array(
			'operator o' => '',
		);

        $operator = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('o.id_operator'=> $id)))->row();

        $jenis_kelamin = array(
			'' => '-- Pilih --',
			'Laki-laki' => 'Laki-laki',
			'Perempuan' => 'Perempuan'
		);

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_operator" value="'.$operator->id_operator.'">
            <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama_operator" class="form-control" value="'.$operator->nama_operator.'" placeholder="Nama">
            </div>
            <div class="form-group">
            <label>Jenis Kelamin</label>
            '.form_dropdown('jenis_kelamin', $jenis_kelamin, $operator->jenis_kelamin,'class="form-control"').'
            </div>
            <div class="form-group">
            <label>No Telepon</label>
            <input type="text" name="no_telp" class="form-control" value="'.$operator->no_telp.'" placeholder="No Telepon">
            </div>
            <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" value="'.$operator->email.'" placeholder="Email">
            </div>
            <div class="form-group">
            <label>Alamat</label>
            <textarea type="text" name="alamat" class="form-control" placeholder="Alamat">'.$operator->alamat.'</textarea>
            </div>
            <div class="form-group">
            <label>Username</label>
            <input type="text" name="username" class="form-control" value="'.$operator->username.'" placeholder="Username">
            </div>
            <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" class="form-control" value="'.$operator->password.'" placeholder="Password">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/data/operator';
        $data['link'] = ''.site_url().'cms/operator/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_operator = $this->input->post('id_operator');
	    $nama_operator = $this->input->post('nama_operator');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');

        $par = array(
            'tabel'=>'operator',
            'data'=>array(
            'nama_operator'=>$nama_operator,
            'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
            'email'=>$email,
            'alamat'=>$alamat,
            'username'=>$username,
            'password'=>$password,
            'role'=> 2
            ),
        );

        if($id_operator != NULL) $par['where'] = array('id_operator'=>$id_operator);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/data/operator');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('operator','id_operator',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/data/operator');
	}

}
