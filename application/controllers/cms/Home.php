<?php

class Home Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$data['breadcrumb'] = "Dashboard";

		$data['tot_video'] = $this->Model_general->getdata(array('tabel'=>'video'))->num_rows();
		$data['freelancer'] = $this->Model_general->getdata(array('tabel'=>'freelancer'))->num_rows();
		$data['tentor'] = $this->Model_general->getdata(array('tabel'=>'tentor'))->num_rows();
		$data['peserta'] = $this->Model_general->getdata(array('tabel'=>'peserta'))->num_rows();

		$data['content'] = "cms/home";
		$this->load->view('home', $data);
	}

}
