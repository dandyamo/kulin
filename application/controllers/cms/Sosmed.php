<?php

class Sosmed Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Sosial Media";
		$data['title'] = "Data Sosial Media";

        $from = array(
			'ref_sosmed rs' => '',
			'ref_icon_sosmed ris' => array('ris.id_ref_icon_sosmed = rs.id_ref_sosmed','left')
		);

        $ref_sosmed = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rs.ref_sosmed ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_sosmed->result() as $sos){

            if($sos->status == 1) {
                $statusx = anchor(site_url('cms/sosmed/tidak_aktif/'.$sos->id_ref_sosmed.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('cms/sosmed/aktif/'.$sos->id_ref_sosmed.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'cms/tampilan/sosmed/ubah/'.$sos->id_ref_sosmed.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$sos->id_ref_sosmed.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$sos->ref_sosmed.'</td>
            <td><a href="'.$sos->link_sosmed.'">'.$sos->link_sosmed.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$sos->id_ref_sosmed.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/sosmed/hapus/'.$sos->id_ref_sosmed.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Status</th>
                <th>Sosial Media</th>
                <th>Icon</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/tampilan/sosmed/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Sosial Media";
		$data['title'] = "Tambah Sosial Media";

        $sosmed = $this->Model_general->combo_box(array('tabel'=>'ref_icon_sosmed', 'key'=>'id_ref_icon_sosmed', 'val'=>array('icon_sosmed')));

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Sosial Media</label>
            <input name="ref_sosmed" type="text" class="form-control" placeholder="Sosial Media">
            </div>
            <div class="form-group">
            <label>Icon Sosial Media</label>
            '.form_dropdown('id_ref_icon_sosmed', $sosmed, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Link Sosial Media</label>
            <input name="link_sosmed" type="text" class="form-control" placeholder="Link Sosial Media">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/tampilan/sosmed';
		$data['link'] = ''.site_url().'cms/sosmed/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah sosmed";
		$data['title'] = "Ubah sosmed";

        $from = array(
			'ref_sosmed rs' => '',
		);

        $ref_sosmed = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rs.id_ref_sosmed'=> $id)))->row();

        $sosmed = $this->Model_general->combo_box(array('tabel'=>'ref_icon_sosmed', 'key'=>'id_ref_icon_sosmed', 'val'=>array('icon_sosmed')));

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Sosial Media</label>
            <input name="ref_sosmed" type="text" class="form-control" value="'.$ref_sosmed->ref_sosmed.'" placeholder="Sosial Media">
            </div>
            <div class="form-group">
            <label>Icon Sosial Media</label>
            '.form_dropdown('id_ref_icon_sosmed', $sosmed, $ref_sosmed->id_ref_icon_sosmed,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Link Sosial Media</label>
            <input name="link_sosmed" type="text" class="form-control" value="'.$ref_sosmed->link_sosmed.'" placeholder="Link Sosial Media">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/tampilan/sosmed';
        $data['link'] = ''.site_url().'cms/sosmed/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_sosmed = $this->input->post('id_ref_sosmed');
	    $id_ref_icon_sosmed = $this->input->post('id_ref_icon_sosmed');
	    $ref_sosmed = $this->input->post('ref_sosmed');
	    $link_sosmed = $this->input->post('link_sosmed');

        $par = array(
            'tabel'=>'ref_sosmed',
            'data'=>array(
            'id_ref_icon_sosmed'=>$id_ref_icon_sosmed,
            'ref_sosmed'=>$ref_sosmed,
            'link_sosmed'=>$link_sosmed,
            'status'=>1
            ),
        );

        if($id_ref_sosmed != NULL) $par['where'] = array('id_ref_sosmed'=>$id_ref_sosmed);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/tampilan/sosmed');

	}


  function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_sosmed',$data,'id_ref_sosmed',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/tampilan/sosmed');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_sosmed',$data,'id_ref_sosmed',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/tampilan/sosmed');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_sosmed','id_ref_sosmed',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/tampilan/sosmed');
	}

}
