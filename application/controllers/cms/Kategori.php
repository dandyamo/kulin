<?php

class Kategori Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['breadcrumb'] = "Data Kategori";
		$data['title'] = "Data Kategori";

        $from = array(
			'ref_kategori rk' => '',
		);

        $ref_kategori = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rk.ref_kategori ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_kategori->result() as $kat){

            $edit = '<a href="'.site_url().'cms/referensi/kategori/ubah/'.$kat->id_ref_kategori.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$kat->id_ref_kategori.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$kat->ref_kategori.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$kat->id_ref_kategori.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/kategori/hapus/'.$kat->id_ref_kategori.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Kategori</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/kategori/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Kategori";
		$data['title'] = "Tambah Kategori";

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Kategori</label>
            <input name="ref_kategori" type="text" class="form-control" placeholder="Kategori">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/kategori';
		$data['link'] = ''.site_url().'cms/kategori/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Kategori";
		$data['title'] = "Ubah Kategori";

        $from = array(
			'ref_kategori rk' => '',
		);

        $ref_kategori = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rk.id_ref_kategori'=> $id)))->row();

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_kategori" value="'.$ref_kategori->id_ref_kategori.'">
            <div class="form-group">
            <label>Kategori</label>
            <input type="text" name="ref_kategori" class="form-control" value="'.$ref_kategori->ref_kategori.'" placeholder="Kategori">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/kategori';
        $data['link'] = ''.site_url().'cms/kategori/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_kategori = $this->input->post('id_ref_kategori');
	    $ref_kategori = $this->input->post('ref_kategori');

        $par = array(
            'tabel'=>'ref_kategori',
            'data'=>array(
            'ref_kategori'=>$ref_kategori
            ),
        );

        if($id_ref_kategori != NULL) $par['where'] = array('id_ref_kategori'=>$id_ref_kategori);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/kategori');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_kategori','id_ref_kategori',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/kategori');
	}

}
