<?php

class Katmum Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data kategori Umum";
		$data['title'] = "Data kategori Umum";

        $from = array(
			'ref_kat_umum rku' => '',
		);

        $ref_kat_umum = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rku.ref_kat_umum ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_kat_umum->result() as $kat){

            if($kat->status == 1) {

                $statusx = anchor(site_url('cms/katmum/tidak_aktif/'.$kat->id_ref_kat_umum.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');

            }else{
                
                $statusx = anchor(site_url('cms/katmum/aktif/'.$kat->id_ref_kat_umum.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');

            }

            $edit = '<a href="'.site_url().'cms/katmum/ubah/'.$kat->id_ref_kat_umum.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$kat->id_ref_kat_umum.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$kat->ref_kat_umum.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$kat->id_ref_kat_umum.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/katmum/hapus/'.$kat->id_ref_kat_umum.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30px">Status</th>
                <th>Kategori Umum</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/kurmum" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'cms/katmum/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Kategori Umum";
		$data['title'] = "Tambah Kategori Umum";

        $data['form_data'] = '
        <input type="hidden" name="permalink" id="permalink">
        <div class="card-body">
            <div class="form-group">
            <label>Kategori Umum</label>
            <input name="ref_kat_umum" id="title" type="text" class="form-control" placeholder="Kategori Umum">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/katmum';
		$data['link'] = ''.site_url().'cms/katmum/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Kategori Umum";
		$data['title'] = "Ubah Kategori Umum";

        $from = array(
			'ref_kat_umum rku' => '',
		);

        $ref_kat_umum = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rku.id_ref_kat_umum'=> $id)))->row();

        $data['form_data'] = '
        <input type="hidden" name="permalink" id="permalink" value="'.$ref_kat_umum->link_kat_umum.'">
        <div class="card-body">
            <input type="hidden" name="id_ref_kat_umum" value="'.$ref_kat_umum->id_ref_kat_umum.'">
            <div class="form-group">
            <label>Kategori Umum</label>
            <input type="text" name="ref_kat_umum" id="title" class="form-control" value="'.$ref_kat_umum->ref_kat_umum.'" placeholder="Kategori Umum">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/katmum';
        $data['link'] = ''.site_url().'cms/katmum/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_kat_umum = $this->input->post('id_ref_kat_umum');
	    $ref_kat_umum = $this->input->post('ref_kat_umum');

        $par = array(
            'tabel'=>'ref_kat_umum',
            'data'=>array(
            'ref_kat_umum'=>$ref_kat_umum,
            'id_ref_kategori'=>2,
            'status'=>1
            ),
        );

        if($id_ref_kat_umum != NULL) $par['where'] = array('id_ref_kat_umum'=>$id_ref_kat_umum);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/katmum');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_kat_umum',$data,'id_ref_kat_umum',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/katmum');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_kat_umum',$data,'id_ref_kat_umum',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/katmum');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_kat_umum','id_ref_kat_umum',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/katmum');
	}

}
