<?php

class Ganda Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

    public function index()
	{
    $this->list_data();
	}

	public function list_data($aktif = NULL)
	{
        $data['breadcrumb'] = "Data Soal Pilihan Ganda";
		$data['title'] = "Data Soal Pilihan Ganda";

        if ($aktif == null) {
            $aktif = 1;
          }

        if($aktif == 1){

        $from = array(
			'ref_soal_ganda rsg' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rsg.id_ref_matkul','left'),
            'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
            'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
            'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left')
		);

        $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'group_by'=>'rsg.id_ref_matkul','order'=>'rsg.ref_soal_ganda ASC','where'=>array('rsg.id_ref_kategori'=>1)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_ganda->result() as $gan){

            $jml = $this->Model_general->getdata(array('tabel'=> $from ,'where'=>array('rsg.id_ref_matkul'=>$gan->id_ref_matkul,'rsg.id_ref_kategori'=>1)))->num_rows();

            $detail = '<a href="'.site_url().'cms/soal/ganda/detail_soal/'.$gan->id_ref_matkul.'/1" class="btn btn-xs btn-info" title="Klik untuk Lihat data"><i class="fa fa-file-alt"></i> List Soal</a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$gan->ref_matkul.'</td>
            <td>'.$gan->ref_semester.'</td>
            <td>'.$gan->ref_jurusan.'</td>
            <td>'.$gan->ref_fakultas.'</td>
            <td>Pilihan Ganda</td>
            <td>'.$jml.'</td>
            <td>'.$detail.'</td>
            </tr>';

            $no++;

        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Mata Kuliah</th>
                <th>Semester</th>
                <th>Jurusan</th>
                <th>Fakultas</th>
                <th>Jenis</th>
                <th>Jumlah Soal</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/soal/ganda/tambah/1" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';

    } else {

        $from = array(
			'ref_soal_ganda rsg' => '',
            'ref_umum ru' => array('ru.id_ref_umum = rsg.id_ref_umum','left')
		);

        $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'group_by'=>'rsg.id_ref_umum','order'=>'rsg.ref_soal_ganda ASC','where'=>array('rsg.id_ref_kategori'=>2)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_ganda->result() as $gan){

            $jml = $this->Model_general->getdata(array('tabel'=> $from ,'where'=>array('rsg.id_ref_umum'=>$gan->id_ref_umum,'rsg.id_ref_kategori'=>2)))->num_rows();

            $detail = '<a href="'.site_url().'cms/soal/ganda/detail_soal/'.$gan->id_ref_umum.'/2" class="btn btn-xs btn-info" title="Klik untuk Lihat data"><i class="fa fa-file-alt"></i> List Soal</a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$gan->ref_umum.'</td>
            <td>Pilihan Ganda</td>
            <td>'.$jml.'</td>
            <td>'.$detail.'</td>
            </tr>';

            $no++;

        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Kursus</th>
                <th>Jenis</th>
                <th>Jumlah Soal</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/soal/ganda/tambah/2" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';

    }
        $data['content'] = "cms/soal_view";
		$this->load->view('home', $data);
	}

    public function detail_soal($id = null, $aktif = null, $status = 1)
	{
        $data['breadcrumb'] = "Data Soal Pilihan Ganda";
		$data['title'] = "Data Soal Pilihan Ganda";

        if($aktif == 1){

        $from = array(
			'ref_soal_ganda rsg' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rsg.id_ref_matkul','left')
		);


        $select = 'rsg.*,rm.ref_matkul';
        $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'rsg.ref_soal_ganda ASC','where'=>array('rsg.id_ref_matkul'=>$id)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_ganda->result() as $gan){

            if($gan->status == 1) {

                $statusx = anchor(site_url('cms/ganda/tidak_aktif/'.$gan->id_ref_soal_ganda.'/'.$id.'/1'),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');

            }else{
                
                $statusx = anchor(site_url('cms/ganda/aktif/'.$gan->id_ref_soal_ganda.'/'.$id.'/1'),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');

            }

            $edit = '<a href="'.site_url().'cms/soal/ganda/ubah/'.$gan->id_ref_soal_ganda.'/'.$id.'/1" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$gan->id_ref_soal_ganda.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$gan->ref_soal_ganda.'</td>
            <td>'.$gan->a.'</td>
            <td>'.$gan->b.'</td>
            <td>'.$gan->c.'</td>
            <td>'.$gan->d.'</td>
            <td>'.$gan->jawaban.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$gan->id_ref_soal_ganda.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/ganda/hapus/'.$gan->id_ref_soal_ganda.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';

        }

        $data['table'] = '
            <h3>Soal '.$ref_soal_ganda->row()->ref_matkul.'</h3>
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Status</th>
                <th>Judul Soal</th>
                <th>Kunci A</th>
                <th>Kunci B</th>
                <th>Kunci C</th>
                <th>Kunci D</th>
                <th>Kunci Jawaban</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/soal/ganda" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'cms/soal/ganda/demo/'.$id.'/1" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Demo Soal</a>';

        } else {

            $from = array(
                'ref_soal_ganda rsg' => '',
                'ref_umum ru' => array('ru.id_ref_umum = rsg.id_ref_umum','left')
            );
    
    
            $select = 'rsg.*,ru.ref_umum';
            $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'rsg.ref_soal_ganda ASC','where'=>array('rsg.id_ref_umum'=>$id)));
    
            $table = '';
            $data['modal'] = '';
            $no = 1;
            foreach($ref_soal_ganda->result() as $gan){
    
                if($gan->status == 1) {
    
                    $statusx = anchor(site_url('cms/ganda/tidak_aktif/'.$gan->id_ref_soal_ganda.'/'.$id.'/2'),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
    
                }else{
                    
                    $statusx = anchor(site_url('cms/ganda/aktif/'.$gan->id_ref_soal_ganda.'/'.$id.'/2'),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
    
                }
    
                $edit = '<a href="'.site_url().'cms/soal/ganda/ubah/'.$gan->id_ref_soal_ganda.'/'.$id.'/2" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
                $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$gan->id_ref_soal_ganda.'"><i class="fa fa-trash"></i></a>';
    
                $table .='<tr>
                <td>'.$no.'</td>
                <td>'.$statusx.'</td>
                <td>'.$gan->ref_soal_ganda.'</td>
                <td>'.$gan->a.'</td>
                <td>'.$gan->b.'</td>
                <td>'.$gan->c.'</td>
                <td>'.$gan->d.'</td>
                <td>'.$gan->jawaban.'</td>
                <td>'.$edit.' '.$hapus.'</td>
                </tr>';
    
                $no++;
    
                $data['modal'] .= '
                <div class="modal fade" id="hapus'.$gan->id_ref_soal_ganda.'">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <p>Yakin ini menghapus data ini?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                      <a href="'.site_url().'cms/ganda/hapus/'.$gan->id_ref_soal_ganda.'" class="btn btn-success">Hapus</a>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>';
    
            }
    
            $data['table'] = '
                <h3>Soal '.$ref_soal_ganda->row()->ref_umum.'</h3>
                <table class="table table-bordered" id="example1">
                  <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Status</th>
                    <th>Judul Soal</th>
                    <th>Kunci A</th>
                    <th>Kunci B</th>
                    <th>Kunci C</th>
                    <th>Kunci D</th>
                    <th>Kunci Jawaban</th>
                    <th width="60px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    '.$table.'
                  </tbody>
                </table>';
    
            $data['tombol'] = '<a href="'.site_url().'cms/soal/ganda" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'cms/soal/ganda/demo/'.$id.'/2" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Demo Soal</a>';

        }
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

    public function demo($id = null, $aktif = null)
	{
        $data['breadcrumb'] = "Demo Soal";
		$data['title'] = "Demo Soal";

        if($aktif == 1){

        $from = array(
			'ref_soal_ganda rsg' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rsg.id_ref_matkul','left')
		);

        $select = 'rsg.*,rm.ref_matkul';
        $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rsg.ref_soal_ganda ASC','where'=>array('rsg.id_ref_matkul'=>$id,'rsg.status'=>1)));

        $data['table'] = '<h3>Soal '.$ref_soal_ganda->row()->ref_matkul.'</h3>';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_ganda->result() as $gan){

            $data['table'] .='
            <div class="card card-primary card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title w-100">
                        '.$no.'. '.$gan->ref_soal_ganda.'
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="collapse show" data-parent="#accordion" style="">
                <div class="card-body">
                        <tr>
                        <td></td>
                        <td>
                        <div class="row">
                        <div class="col-md-1" style="margin-right: -50px">
                        <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="a"> A.
                        </div>
                        <div class="col-md-9">
                        '.$gan->a.'
                        </div>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td>
                        <div class="row">
                        <div class="col-md-1"  style="margin-right: -50px">
                        <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="b"> B.
                        </div>
                        <div class="col-md-9">
                        '.$gan->b.'
                        </div>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td>
                        <div class="row">
                        <div class="col-md-1"  style="margin-right: -50px">
                        <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="c"> C.
                        </div>
                        <div class="col-md-9">
                        '.$gan->c.'
                        </div>
                        </div>
                        </td>
                        </tr>
                        <tr>
                        <td></td>
                        <td>
                        <div class="row">
                        <div class="col-md-1"  style="margin-right: -50px">
                        <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="d"> D.
                        </div>
                        <div class="col-md-9">
                        '.$gan->d.'
                        </div>
                        </div>
                        </td>
                        </tr>
                </div>
            </div>
            </div>';

            $no++;

        }

        $data['tombol'] = '<a href="'.site_url().'cms/soal/ganda/detail_soal/'.$id.'/1" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>';

        } else {

            $from = array(
                'ref_soal_ganda rsg' => '',
                'ref_umum ru' => array('ru.id_ref_umum = rsg.id_ref_umum','left')
            );
    
            $select = 'rsg.*,ru.ref_umum';
            $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rsg.ref_soal_ganda ASC','where'=>array('rsg.id_ref_umum'=>$id,'rsg.status'=>1)));
    
            $data['table'] = '<h3>Soal '.$ref_soal_ganda->row()->ref_umum.'</h3>';
            $data['modal'] = '';
            $no = 1;
            foreach($ref_soal_ganda->result() as $gan){
    
                $data['table'] .='
                <div class="card card-primary card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                    <div class="card-header">
                        <h4 class="card-title w-100">
                            '.$no.'. '.$gan->ref_soal_ganda.'
                        </h4>
                    </div>
                </a>
                <div id="collapseOne" class="collapse show" data-parent="#accordion" style="">
                    <div class="card-body">
                            <tr>
                            <td></td>
                            <td>
                            <div class="row">
                            <div class="col-md-1" style="margin-right: -50px">
                            <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="a"> A.
                            </div>
                            <div class="col-md-9">
                            '.$gan->a.'
                            </div>
                            </div>
                            </td>
                            </tr>
                            <tr>
                            <td></td>
                            <td>
                            <div class="row">
                            <div class="col-md-1"  style="margin-right: -50px">
                            <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="b"> B.
                            </div>
                            <div class="col-md-9">
                            '.$gan->b.'
                            </div>
                            </div>
                            </td>
                            </tr>
                            <tr>
                            <td></td>
                            <td>
                            <div class="row">
                            <div class="col-md-1"  style="margin-right: -50px">
                            <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="c"> C.
                            </div>
                            <div class="col-md-9">
                            '.$gan->c.'
                            </div>
                            </div>
                            </td>
                            </tr>
                            <tr>
                            <td></td>
                            <td>
                            <div class="row">
                            <div class="col-md-1"  style="margin-right: -50px">
                            <input name="pilihan['.$gan->id_ref_soal_ganda.']" type="radio" value="d"> D.
                            </div>
                            <div class="col-md-9">
                            '.$gan->d.'
                            </div>
                            </div>
                            </td>
                            </tr>
                    </div>
                </div>
                </div>';
    
                $no++;
    
            }
    
            $data['tombol'] = '<a href="'.site_url().'cms/soal/ganda/detail_soal/'.$id.'/2" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>';

        }
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah($aktif = null)
	{
        $data['breadcrumb'] = "Tambah Soal Pilihan Ganda";
		$data['title'] = "Tambah Soal Pilihan Ganda";

        if($aktif == 1){

        $matkul = $this->Model_general->combo_box(array('tabel'=>'ref_matkul', 'key'=>'id_ref_matkul', 'val'=>array('ref_matkul')));

        $data['form_data'] = '
        <div class="card-body">
         <input type="hidden" name="id_ref_kategori" value="1">
            <div class="form-group">
            <label>Mata Kuliah</label>
            '.form_dropdown('id_ref_matkul', $matkul, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Judul Soal</label>
            <textarea name="ref_soal_ganda" type="text" class="form-control" placeholder="Judul Soal"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban A</label>
            <textarea name="a" id="ckeditor" type="text" class="form-control" placeholder="Jawaban A"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban B</label>
            <textarea name="b" id="ckeditor1" type="text" class="form-control" placeholder="Jawaban B"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban C</label>
            <textarea name="c" id="ckeditor2" type="text" class="form-control" placeholder="Jawaban C"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban D</label>
            <textarea name="d" id="ckeditor3" type="text" class="form-control" placeholder="Jawaban D"></textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            <select name="jawaban" type="text" class="form-control">
            <option value="a">A</option>
            <option value="b">B</option>
            <option value="c">C</option>
            <option value="d">D</option>
            </select>
            </div>
            <div class="form-group">
            <label>Gambar Soal</label>
            <input name="gambar_soal" type="file" class="form-control" style="line-height: 1.1;">
            <small>*Opsional</small>
            </div>
        </div>
        <!-- /.card-body -->';

        } else {

            $umum = $this->Model_general->combo_box(array('tabel'=>'ref_umum', 'key'=>'id_ref_umum', 'val'=>array('ref_umum')));

        $data['form_data'] = '
        <div class="card-body">
        <input type="hidden" name="id_ref_kategori" value="2">
            <div class="form-group">
            <label>Kursus</label>
            '.form_dropdown('id_ref_umum', $umum, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Judul Soal</label>
            <textarea name="ref_soal_ganda" type="text" class="form-control" placeholder="Judul Soal"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban A</label>
            <textarea name="a" id="ckeditor" type="text" class="form-control" placeholder="Jawaban A"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban B</label>
            <textarea name="b" id="ckeditor1" type="text" class="form-control" placeholder="Jawaban B"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban C</label>
            <textarea name="c" id="ckeditor2" type="text" class="form-control" placeholder="Jawaban C"></textarea>
            </div>
            <div class="form-group">
            <label>Jawaban D</label>
            <textarea name="d" id="ckeditor3" type="text" class="form-control" placeholder="Jawaban D"></textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            <select name="jawaban" type="text" class="form-control">
            <option value="a">A</option>
            <option value="b">B</option>
            <option value="c">C</option>
            <option value="d">D</option>
            </select>
            </div>
            <div class="form-group">
            <label>Gambar Soal</label>
            <input name="gambar_soal" type="file" class="form-control" style="line-height: 1.1;">
            <small>*Opsional</small>
            </div>
        </div>
        <!-- /.card-body -->';

        }

        $data['script'] ='
        <script type="text/javascript">
            $(function () {
                    CKEDITOR.replace("ckeditor",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });

                    CKEDITOR.replace("ckeditor1",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });

                    CKEDITOR.replace("ckeditor2",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });

                    CKEDITOR.replace("ckeditor3",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });
                });
        </script>';

		$data['back'] = ''.site_url().'cms/soal/ganda';
		$data['link'] = ''.site_url().'cms/ganda/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null, $id_detail = null,$aktif = null)
	{
        $data['breadcrumb'] = "Ubah Pilihan Ganda";
		$data['title'] = "Ubah Pilihan Ganda";

        if($aktif == 1){

        $from = array(
			'ref_soal_ganda rsg' => '',
		);

        $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rsg.id_ref_soal_ganda'=> $id)))->row();

        $matkul = $this->Model_general->combo_box(array('tabel'=>'ref_matkul', 'key'=>'id_ref_matkul', 'val'=>array('ref_matkul')));

        $combo_status = array(
			'a' => 'A',
			'b' => 'B',
			'c' => 'C',
			'd' => 'D'
		);

        $data['form_data'] = '
        <input type="hidden" name="id_ref_soal_ganda" value="'.$ref_soal_ganda->id_ref_soal_ganda.'">
        <input type="hidden" name="id_ref_kategori" value="1">
        <div class="card-body">
            <div class="form-group">
            <label>Mata Kuliah</label>
            '.form_dropdown('id_ref_matkul', $matkul, $ref_soal_ganda->id_ref_matkul,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Judul Soal</label>
            <textarea name="ref_soal_ganda" type="text" class="form-control" placeholder="Judul Soal">'.$ref_soal_ganda->ref_soal_ganda.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban A</label>
            <textarea name="a" id="ckeditor" type="text" class="form-control" placeholder="Jawaban A">'.$ref_soal_ganda->a.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban B</label>
            <textarea name="b" id="ckeditor1" type="text" class="form-control" placeholder="Jawaban B">'.$ref_soal_ganda->b.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban C</label>
            <textarea name="c" id="ckeditor2" type="text" class="form-control" placeholder="Jawaban C">'.$ref_soal_ganda->c.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban D</label>
            <textarea name="d" id="ckeditor3" type="text" class="form-control" placeholder="Jawaban D">'.$ref_soal_ganda->d.'</textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            '.form_dropdown('jawaban', $combo_status, $ref_soal_ganda->jawaban,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Gambar Soal</label>
            <input name="gambar_soal" type="file" class="form-control" style="line-height: 1.1;">
            <small>*Opsional</small>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['back'] = ''.site_url().'cms/soal/ganda/detail_soal/'.$id_detail.'/1';


    } else {

        $from = array(
			'ref_soal_ganda rsg' => '',
		);

        $ref_soal_ganda = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rsg.id_ref_soal_ganda'=> $id)))->row();

        $umum = $this->Model_general->combo_box(array('tabel'=>'ref_umum', 'key'=>'id_ref_umum', 'val'=>array('ref_umum')));

        $combo_status = array(
			'a' => 'A',
			'b' => 'B',
			'c' => 'C',
			'd' => 'D'
		);

        $data['form_data'] = '
        <input type="hidden" name="id_ref_soal_ganda" value="'.$ref_soal_ganda->id_ref_soal_ganda.'">
        <input type="hidden" name="id_ref_kategori" value="2">
        <div class="card-body">
            <div class="form-group">
            <label>Mata Kuliah</label>
            '.form_dropdown('id_ref_umum', $umum, $ref_soal_ganda->id_ref_umum,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Judul Soal</label>
            <textarea name="ref_soal_ganda" type="text" class="form-control" placeholder="Judul Soal">'.$ref_soal_ganda->ref_soal_ganda.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban A</label>
            <textarea name="a" id="ckeditor" type="text" class="form-control" placeholder="Jawaban A">'.$ref_soal_ganda->a.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban B</label>
            <textarea name="b" id="ckeditor1" type="text" class="form-control" placeholder="Jawaban B">'.$ref_soal_ganda->b.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban C</label>
            <textarea name="c" id="ckeditor2" type="text" class="form-control" placeholder="Jawaban C">'.$ref_soal_ganda->c.'</textarea>
            </div>
            <div class="form-group">
            <label>Jawaban D</label>
            <textarea name="d" id="ckeditor3" type="text" class="form-control" placeholder="Jawaban D">'.$ref_soal_ganda->d.'</textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            '.form_dropdown('jawaban', $combo_status, $ref_soal_ganda->jawaban,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Gambar Soal</label>
            <input name="gambar_soal" type="file" class="form-control" style="line-height: 1.1;">
            <small>*Opsional</small>
            </div>
        </div>
        <!-- /.card-body -->';

        $data['back'] = ''.site_url().'cms/soal/ganda/detail_soal/'.$id_detail.'/2';

    }

        $data['script'] ='
        <script type="text/javascript">
            $(function () {
                    CKEDITOR.replace("ckeditor",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });

                    CKEDITOR.replace("ckeditor1",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });

                    CKEDITOR.replace("ckeditor2",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });

                    CKEDITOR.replace("ckeditor3",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });
                });
        </script>';

        $data['link'] = ''.site_url().'cms/ganda/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_soal_ganda = $this->input->post('id_ref_soal_ganda');
	    $ref_soal_ganda = $this->input->post('ref_soal_ganda');
	    $id_ref_matkul = $this->input->post('id_ref_matkul');
        $id_ref_kategori = $this->input->post('id_ref_kategori');
	    $a = $this->input->post('a');
	    $b = $this->input->post('b');
	    $c = $this->input->post('c');
	    $d = $this->input->post('d');
	    $jawaban = $this->input->post('jawaban');

        $par = array(
            'tabel'=>'ref_soal_ganda',
            'data'=>array(
            'id_ref_matkul'=>$id_ref_matkul,
            'id_ref_kategori'=>$id_ref_kategori,
            'ref_soal_ganda'=>$ref_soal_ganda,
            'a'=>$a,
            'b'=>$b,
            'c'=>$c,
            'd'=>$d,
            'jawaban'=>$jawaban,
            'status'=>1,
            'nama_created'=>$this->session->userdata('nama'),
            'tgl_created'=>date('Y-m-d')
            ),
        );

        if($id_ref_soal_ganda != NULL) $par['where'] = array('id_ref_soal_ganda'=>$id_ref_soal_ganda);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/soal/ganda');

	}

    function aktif($id=null,$status,$aktif) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_soal_ganda',$data,'id_ref_soal_ganda',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/soal/ganda/detail_soal/'.$status.'/'.$aktif);
	}

	
	function tidak_aktif($id=null,$status,$aktif) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_soal_ganda',$data,'id_ref_soal_ganda',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/soal/ganda/detail_soal/'.$status.'/'.$aktif);
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_soal_ganda','id_ref_soal_ganda',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/soal/ganda');
	}

}
