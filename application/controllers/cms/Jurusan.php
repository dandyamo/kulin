<?php

class Jurusan Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Jurusan";
		$data['title'] = "Data Jurusan";

        $from = array(
			'ref_jurusan rj' => '',
            'ref_fakultas rf' => array('rf.id_ref_fakultas = rj.id_ref_fakultas','left')
		);

        $select = 'rj.*,rf.ref_fakultas';
        $ref_jurusan = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rj.ref_jurusan ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_jurusan->result() as $jur){

            if($jur->status == 1) {
                $statusx = anchor(site_url('cms/jurusan/tidak_aktif/'.$jur->id_ref_jurusan.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('cms/jurusan/aktif/'.$jur->id_ref_jurusan.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'cms/referensi/jurusan/ubah/'.$jur->id_ref_jurusan.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$jur->id_ref_jurusan.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$jur->ref_jurusan.'</td>
            <td>'.$jur->ref_fakultas.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$jur->id_ref_jurusan.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/jurusan/hapus/'.$jur->id_ref_jurusan.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30px">Status</th>
                <th>Jurusan</th>
                <th>Fakultas</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/jurusan/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Jurusan";
		$data['title'] = "Tambah Jurusan";

        $fakultas = $this->Model_general->combo_box(array('tabel'=>'ref_fakultas', 'key'=>'id_ref_fakultas', 'val'=>array('ref_fakultas')));

        $data['form_data'] = '
        <div class="card-body">
          <input type="hidden" name="permalink" id="title">
            <div class="form-group">
            <label>Fakultas</label>
            '.form_dropdown('id_ref_fakultas', $fakultas, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Jurusan</label>
            <input name="ref_jurusan" id="title" type="text" class="form-control" placeholder="Jurusan">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/jurusan';
		$data['link'] = ''.site_url().'cms/jurusan/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Jurusan";
		$data['title'] = "Ubah Jurusan";

        $from = array(
			'ref_jurusan rj' => '',
            'ref_fakultas rf' => array('rf.id_ref_fakultas = rj.id_ref_fakultas','left')
		);

        $ref_jurusan = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rj.id_ref_jurusan'=> $id)))->row();

        $fakultas = $this->Model_general->combo_box(array('tabel'=>'ref_fakultas', 'key'=>'id_ref_fakultas', 'val'=>array('ref_fakultas')));

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_jurusan" value="'.$ref_jurusan->id_ref_jurusan.'">
            <input type="hidden" name="permalink" id="title" value="'.$ref_jurusan->permalink.'">
            <div class="form-group">
            <label>Fakultas</label>
            '.form_dropdown('id_ref_fakultas', $fakultas, $ref_jurusan->id_ref_fakultas,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Jurusan</label>
            <input type="text" name="ref_jurusan" id="title" class="form-control" value="'.$ref_jurusan->ref_jurusan.'" placeholder="Jurusan">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/jurusan';
        $data['link'] = ''.site_url().'cms/jurusan/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_jurusan = $this->input->post('id_ref_jurusan');
	    $id_ref_fakultas = $this->input->post('id_ref_fakultas');
	    $ref_jurusan = $this->input->post('ref_jurusan');
	    $permalink = $this->input->post('permalink');

        $par = array(
            'tabel'=>'ref_jurusan',
            'data'=>array(
            'id_ref_fakultas'=>$id_ref_fakultas,
            'ref_jurusan'=>$ref_jurusan,
            'link_jurusan'=>$permalink,
            'status'=> 1
            ),
        );

        if($id_ref_jurusan != NULL) $par['where'] = array('id_ref_jurusan'=>$id_ref_jurusan);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/jurusan');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_jurusan',$data,'id_ref_jurusan',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/referensi/jurusan');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_jurusan',$data,'id_ref_jurusan',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/referensi/jurusan');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_jurusan','id_ref_jurusan',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/jurusan');
	}

}
