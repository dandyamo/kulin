<?php

class Slideshow Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Slideshow";
		$data['title'] = "Data Slideshow";

        $from = array(
			'ref_slideshow rs' => '',
		);

        $ref_slideshow = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rs.ref_slideshow ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_slideshow->result() as $sli){

            if($sli->status == 1) {
                $statusx = anchor(site_url('cms/slideshow/tidak_aktif/'.$sli->id_ref_slideshow.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('cms/slideshow/aktif/'.$sli->id_ref_slideshow.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'cms/tampilan/slideshow/ubah/'.$sli->id_ref_slideshow.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$sli->id_ref_slideshow.'"><i class="fa fa-trash"></i></a>';

            $photo = !empty($sli->berkas) ? FCPATH.'uploads/file_slideshow/'.$sli->berkas : null;
            $background = (file_exists($photo) and !empty($sli->berkas)) ? base_url().'uploads/file_slideshow/'.$sli->berkas : base_url().'assets/backend/dist/img/credit-card.png';

            $table .='<tr>
            <td>'.$no.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$sli->ref_slideshow.'</td>
            <td><center><img style="width:200px;" src="'.$background.'"></center></td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$sli->id_ref_slideshow.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/slideshow/hapus/'.$sli->id_ref_slideshow.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30x">Status</th>
                <th>Nama Slideshow</th>
                <th>Slideshow</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/tampilan/slideshow/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{

    $data['breadcrumb'] = "Tambah slideshow";
    $data['title'] = "Tambah slideshow";

    $data['form_data'] = '
    <div class="card-body">
        <div class="form-group">
        <label>Slideshow</label>
        <input name="ref_slideshow" type="text" class="form-control" placeholder="Slideshow">
        </div>
        <div class="form-group">
        <label>Berkas</label>
        <input name="berkas" type="file" class="form-control" style="line-height: 1.1;">
        </div>
    </div>
    <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/tampilan/slideshow';
		$data['link'] = ''.site_url().'cms/slideshow/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah slideshow";
		$data['title'] = "Ubah slideshow";

        $from = array(
			'ref_slideshow rs' => '',
		);

        $ref_slideshow = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rs.id_ref_slideshow'=> $id)))->row();

        if ($id!=NULL) {
            $logo = '<p><a href="'.base_url('uploads/file_slideshow/'.@$ref_slideshow->berkas).'" width="300">'.@$ref_slideshow->berkas.'</a></p>';
        }

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_slideshow" value="'.$ref_slideshow->id_ref_slideshow.'">
            '.form_hidden('val_berkas_file', @$ref_slideshow->berkas, 'class="form-control"').'
            <div class="form-group">
            <label>Nama Bank</label>
            <input type="text" name="ref_slideshow" class="form-control" value="'.$ref_slideshow->ref_slideshow.'" placeholder="Slideshow">
            </div>
            <div class="form-group">
            <label>Berkas</label>
            <input name="berkas" type="file" class="form-control" style="line-height: 1.1;">
            '.$logo.'
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/tampilan/slideshow';
        $data['link'] = ''.site_url().'cms/slideshow/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_slideshow = $this->input->post('id_ref_slideshow');
	    $ref_slideshow = $this->input->post('ref_slideshow');

        $par = array(
            'tabel'=>'ref_slideshow',
            'data'=>array(
            'ref_slideshow'=>$ref_slideshow,
            'status'=> 1
            ),
        );

        $berkas = $_FILES['berkas']['tmp_name'];
			if($berkas!=NULL){
				$path = './uploads/file_slideshow';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				if ($in['val_berkas']!=NULL) {
					$prev = $in['val_berkas'];
					$path_pasfoto = $path.'/'.$prev;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('berkas');
			    $data = $this->upload->data();
			    $berkas = $data['file_name'];
			    $par['data']['berkas'] = $berkas;
			}

        if($id_ref_slideshow != NULL) $par['where'] = array('id_ref_slideshow'=>$id_ref_slideshow);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/tampilan/slideshow');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_slideshow',$data,'id_ref_slideshow',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/tampilan/slideshow');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_slideshow',$data,'id_ref_slideshow',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/tampilan/slideshow');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_slideshow','id_ref_slideshow',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/tampilan/slideshow');
	}

}
