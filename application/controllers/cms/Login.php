<?php

class Login Extends CI_Controller{

	public function index()
	{
		$this->load->view('cms/login');
	}

	public function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password);
		$operator = $this->Model_general->login('operator',$where)->row_array();
		
		if ($operator > 0) {

			$data_session = array(
				'username' => $username,
				'nama' => $operator['nama_operator'],
				'id' => $operator['id_operator'],
				'role' => $operator['role'],
				'status' => TRUE
				);

			$this->session->set_userdata($data_session);

			redirect(base_url("cms/home"));
			
		} else {

			$this->session->set_flashdata('fail','Username atau Password tidak cocok!<br>Silahkan ulangi kembali');
			redirect(base_url("cms/login"));

		}


	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('cms/login'));
	}

}
