<?php

class Sertifikat Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Sertifikat";
		$data['title'] = "Data Sertifikat";

        $from = array(
			'ref_sertifikat rr' => '',
		);

        $ref_sertifikat = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rr.nama_sertifikat ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_sertifikat->result() as $ser){

            $edit = '<a href="'.site_url().'cms/referensi/sertifikat/ubah/'.$ser->id_ref_sertifikat.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$ser->id_ref_sertifikat.'"><i class="fa fa-trash"></i></a>';
            $view = '<a href="#" class="btn btn-info btn-xs" title="Klik untuk lihat data" data-toggle="modal" data-target="#view'.$ser->id_ref_sertifikat.'"><i class="fa fa-eye"></i></a>';
           /*  $view = '<a href="'.site_url().'cms/referensi/sertifikat/lihat_data/'.$ser->id_ref_sertifikat.'" class="btn btn-info btn-xs" title="Klik untuk lihat data"><i class="fa fa-eye"></i></a>'; */

            $photo = !empty($ser->background) ? FCPATH.'uploads/file_sertifikat/'.$ser->background : null;
            $background = (file_exists($photo) and !empty($ser->background)) ? base_url().'uploads/file_sertifikat/'.$ser->background : base_url().'assets/logo/brand.png';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$ser->nama_sertifikat.'</td>
            <td><center><img style="width:45px;" src="'.$background.'"></center></td>
            <td>'.$view.' '.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$ser->id_ref_sertifikat.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/sertifikat/hapus/'.$ser->id_ref_sertifikat.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          
          <div class="modal fade" id="view'.$ser->id_ref_sertifikat.'">
            <div class="modal-dialog modal-lg" style="max-width:1250px !important;">
              <div class="modal-content" style="height: 1000px !important;">
                <div class="modal-header">
                <a target="_blank" href="'.site_url().'cms/referensi/sertifikat/lihat_data/'.$ser->id_ref_sertifikat.'" class="btn btn-xs btn-warning" title="Klik untuk print data"><i class="fa fa-print"></i> Cetak</a>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body" style="background-image: url('.base_url().'uploads/file_sertifikat/'.$ser->background.');background-repeat:no-repeat;
                background-size:cover;
                background-position:center;">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12" style="margin-top: 200px;">
                            <center>
                            <h1 style="font-size: 56px">'.$ser->header_sertifikat.'</h1>
                            <p style="font-weight: bold">Nomor: 0000/0/0000/0000</p>
                            <br>
                            <h3>DIBERIKAN KEPADA</h3>
                            <h4>Nama Peserta</h4>
                            <hr style="width: 50%; border-top: 4px solid #000;">
                            <h4>Sebagai Peserta</h4>
                            <h6>Pelatihan Peningkatan Sumber Daya</h6>
                            </center>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          ';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama Sertifikat</th>
                <th>Background</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/sertifikat/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{

        $data['breadcrumb'] = "Tambah Sertifikat";
        $data['title'] = "Tambah Sertifikat";

        $data['form_data'] = '
        <div class="card-body">
            <div class="form-group">
            <label>Nama Sertifikat</label>
            <input name="nama_sertifikat" type="text" class="form-control" placeholder="Nama Sertifikat">
            </div>
            <div class="form-group">
            <label>Header Sertifikat</label>
            <input name="header_sertifikat" type="text" class="form-control" placeholder="Header Sertifikat">
            </div>
            <div class="form-group">
            <label>Footer Sertifikat</label>
            <input name="footer_sertifikat" type="text" class="form-control" placeholder="Footer Sertifikat">
            </div>
            <div class="form-group">
            <label>Background</label>
            <input name="background" type="file" class="form-control" style="line-height: 1.1;">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/sertifikat';
		$data['link'] = ''.site_url().'cms/sertifikat/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah sertifikat";
		$data['title'] = "Ubah sertifikat";

        $from = array(
			'ref_sertifikat rr' => '',
		);

        $ref_sertifikat = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rr.id_ref_sertifikat'=> $id)))->row();

        if ($id!=NULL) {
            $logo = '<p><a href="'.base_url('uploads/file_sertifikat/'.@$ref_sertifikat->background).'" width="300">'.@$ref_sertifikat->background.'</a></p>';
        }

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_sertifikat" value="'.$ref_sertifikat->id_ref_sertifikat.'">
            '.form_hidden('val_background', @$ref_sertifikat->background, 'class="form-control"').'
            <div class="form-group">
            <label>Nama Sertifikat</label>
            <input name="nama_sertifikat" type="text" class="form-control"  value="'.$ref_sertifikat->nama_sertifikat.'" placeholder="Nama Sertifikat">
            </div>
            <div class="form-group">
            <label>Header Sertifikat</label>
            <input name="header_sertifikat" type="text" class="form-control"  value="'.$ref_sertifikat->header_sertifikat.'" placeholder="Header Sertifikat">
            </div>
            <div class="form-group">
            <label>Footer Sertifikat</label>
            <input name="footer_sertifikat" type="text" class="form-control"  value="'.$ref_sertifikat->footer_sertifikat.'" placeholder="Footer Sertifikat">
            </div>
            <div class="form-group">
            <label>Background</label>
            <input name="background" type="file" class="form-control" style="line-height: 1.1;">
            '.$logo.'
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/sertifikat';
        $data['link'] = ''.site_url().'cms/sertifikat/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_sertifikat = $this->input->post('id_ref_sertifikat');
	    $nama_sertifikat = $this->input->post('nama_sertifikat');
	    $header_sertifikat = $this->input->post('header_sertifikat');
	    $footer_sertifikat = $this->input->post('footer_sertifikat');

        $par = array(
            'tabel'=>'ref_sertifikat',
            'data'=>array(
            'nama_sertifikat'=>$nama_sertifikat,
            'header_sertifikat'=>$header_sertifikat,
            'footer_sertifikat'=>$footer_sertifikat
            ),
        );

        $background = $_FILES['background']['tmp_name'];
			if($background!=NULL){
				$path = './uploads/file_sertifikat';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				if ($in['val_background']!=NULL) {
					$prev = $in['val_background'];
					$path_pasfoto = $path.'/'.$prev;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('background');
			    $data = $this->upload->data();
			    $background = $data['file_name'];
			    $par['data']['background'] = $background;
			}

        if($id_ref_sertifikat != NULL) $par['where'] = array('id_ref_sertifikat'=>$id_ref_sertifikat);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/sertifikat');

	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_sertifikat','id_ref_sertifikat',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/sertifikat');
	}

    function lihat_data($id=null) {
        
        $from = array(
			'ref_sertifikat rr' => '',
		);

        $dt = $this->Model_general->getdata(array('tabel'=>$from, 'where'=>array('rr.id_ref_sertifikat'=>$id)))->row();

        $data['body'] = '
        <body style="background-image: url('.base_url().'uploads/file_sertifikat/'.$dt->background.');background-repeat:no-repeat;
        background-size:cover;
        background-position:center;" onload="window.print()">

        <div class="container">
            <div class="row">
                <div class="col-lg-12" style="margin-top: 150px;">
                <center>
                <h1 style="font-size: 56px">'.$dt->header_sertifikat.'</h1>
                <p style="font-weight: bold">Nomor: 0000/0/0000/0000</p>
                <br>
                <h3>DIBERIKAN KEPADA</h3>
                <h4>Nama Peserta</h4>
                <hr style="width: 50%; border-top: 4px solid #000;">
                <h4>Sebagai Peserta</h4>
                <h6>Pelatihan Peningkatan Sumber Daya</h6>
                </center>
                </div>
            </div>
        </div>  
        ';

        $data['title'] = ''.$dt->nama_sertifikat.'';
        $data['back'] = ''.site_url().'cms/referensi/sertifikat';
		$this->load->view('sertifikat_view', $data);

	}

}
