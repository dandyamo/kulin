<?php

class Essay Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
    $this->list_data();
	}

	public function list_data($aktif = NULL)
	{
        $data['breadcrumb'] = "Data Soal";
		$data['title'] = "Data Soal";

        if ($aktif == null) {
            $aktif = 1;
          }

        if($aktif == 1){

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rse.id_ref_matkul','left'),
            'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
            'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
            'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left'),
            'tentor t' => array('t.id_tentor = rse.id_tentor','left')
		);

        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'group_by'=>'t.id_tentor','order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_kategori'=>1)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $gan){

            $jml = $this->Model_general->getdata(array('tabel'=> $from ,'where'=>array('rse.id_ref_matkul'=>$gan->id_ref_matkul,'rse.id_ref_kategori'=>1)))->num_rows();

            $detail = '<a href="'.site_url().'cms/soal/essay/detail_soal/'.$gan->id_ref_matkul.'/1" class="btn btn-xs btn-info" title="Klik untuk Lihat data"><i class="fa fa-file-alt"></i> List Soal</a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$gan->nama_tentor.'</td>
            <td>'.$gan->ref_matkul.'</td>
            <td>'.$gan->ref_semester.'</td>
            <td>'.$gan->ref_jurusan.'</td>
            <td>'.$gan->ref_fakultas.'</td>
            <td>Essay</td>
            <td>'.$jml.'</td>
            <td>'.$detail.'</td>
            </tr>';

            $no++;

        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama Tentor</th>
                <th>Mata Kuliah</th>
                <th>Semester</th>
                <th>Jurusan</th>
                <th>Fakultas</th>
                <th>Jenis</th>
                <th>Jumlah Soal</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';


    } else {

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_umum ru' => array('ru.id_ref_umum = rse.id_ref_umum','left'),
            'tentor t' => array('t.id_ref_umum = ru.id_ref_umum','left')
		);

        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'group_by'=>'rse.id_ref_umum','order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_kategori'=>2)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $gan){

            $jml = $this->Model_general->getdata(array('tabel'=> $from ,'where'=>array('rse.id_ref_umum'=>$gan->id_ref_umum,'rse.id_ref_kategori'=>2)))->num_rows();

            $detail = '<a href="'.site_url().'cms/soal/essay/detail_soal/'.$gan->id_ref_umum.'/2" class="btn btn-xs btn-info" title="Klik untuk Lihat data"><i class="fa fa-file-alt"></i> List Soal</a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$gan->nama_tentor.'</td>
            <td>'.$gan->ref_umum.'</td>
            <td>Essay</td>
            <td>'.$jml.'</td>
            <td>'.$detail.'</td>
            </tr>';

            $no++;

        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Nama Tentor</th>
                <th>Kursus</th>
                <th>Jenis</th>
                <th>Jumlah Soal</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';


    }
        $data['content'] = "cms/essay_view";
		$this->load->view('home', $data);
	}

    public function detail_soal($id = null, $aktif = null, $status = 1)
	{
        $data['breadcrumb'] = "Data Soal";
		$data['title'] = "Data Soal";

        if($aktif == 1){

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rse.id_ref_matkul','left'),
            'tentor t' => array('t.id_tentor = rse.id_tentor','left')
		);


        $select = 'rse.*,rm.ref_matkul,t.nama_tentor';
        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_matkul'=>$id)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $ess){

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$ess->ref_soal_essay.'</td>
            <td>'.$ess->jawaban.'</td>
            </tr>';

            $no++;

        }

        $data['table'] = '
            <h3>Soal '.$ref_soal_essay->row()->ref_matkul.'</h3>
            <p>Dibuat oleh : <b>'.$ref_soal_essay->row()->nama_tentor.'</b></p>
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Judul Soal</th>
                <th>Kunci Jawaban</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/soal/essay" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'cms/soal/essay/demo/'.$id.'/1" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Demo Soal</a>';

        } else {

            $from = array(
                'ref_soal_essay rse' => '',
                'ref_umum ru' => array('ru.id_ref_umum = rse.id_ref_umum','left')
            );
    
    
            $select = 'rse.*,ru.ref_umum';
            $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_umum'=>$id)));
    
            $table = '';
            $data['modal'] = '';
            $no = 1;
            foreach($ref_soal_essay->result() as $gan){
    
                $table .='<tr>
                <td>'.$no.'</td>
                <td>'.$gan->ref_soal_essay.'</td>
                <td>'.$gan->jawaban.'</td>
                </tr>';
    
                $no++;
    
            }
    
            $data['table'] = '
                <h3>Soal '.$ref_soal_essay->row()->ref_umum.'</h3>
                <table class="table table-bordered" id="example1">
                  <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Status</th>
                    <th>Judul Soal</th>
                    <th>Kunci A</th>
                    <th>Kunci B</th>
                    <th>Kunci C</th>
                    <th>Kunci D</th>
                    <th>Kunci Jawaban</th>
                    <th width="60px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    '.$table.'
                  </tbody>
                </table>';
    
            $data['tombol'] = '<a href="'.site_url().'cms/soal/essay" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'cms/soal/essay/demo/'.$id.'/2" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Demo Soal</a>';

        }
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

    public function demo($id = null, $aktif = null)
	{
        $data['breadcrumb'] = "Demo Soal";
		$data['title'] = "Demo Soal";

        $cms = $this->session->userdata('id');

        if($aktif == 1){

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rse.id_ref_matkul','left')
		);

        $select = 'rse.*,rm.ref_matkul';
        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_matkul'=>$id,'rse.status'=>1)));

        $data['table'] = '<h3>Soal '.$ref_soal_essay->row()->ref_matkul.'</h3>';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $gan){

            $data['table'] .='
            <div class="card card-primary card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title w-100">
                        <div class="row">
                            <div class="col-sm-1" style="margin-right: -60px">
                                '.$no.'. 
                            </div>
                            <div class="col-sm-11">
                                '.$gan->ref_soal_essay.'
                            </div>
                        </div>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="collapse show" data-parent="#accordion" style="">
            <div class="card-body">
            <textarea class="form-control" rows="5" placeholder="Masukkan jawaban anda"></textarea>
            </div>
            </div>
            </div>';

            $no++;

        }

        $data['tombol'] = '<a href="'.site_url().'cms/soal/essay/detail_soal/'.$id.'/1" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>';

        } else {

            $from = array(
                'ref_soal_essay rse' => '',
                'ref_umum ru' => array('ru.id_ref_umum = rse.id_ref_umum','left')
            );
    
            $select = 'rse.*,ru.ref_umum';
            $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_umum'=>$id,'rse.status'=>1)));
    
            $data['table'] = '<h3>Soal '.$ref_soal_essay->row()->ref_umum.'</h3>';
            $data['modal'] = '';
            $no = 1;
            foreach($ref_soal_essay->result() as $gan){
    
                $data['table'] .='
                <div class="card card-primary card-outline">
                <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                <div class="card-header">
                <h4 class="card-title w-100">
                    <div class="row">
                        <div class="col-sm-1" style="margin-right: -60px">
                            '.$no.'. 
                        </div>
                        <div class="col-sm-11">
                            '.$gan->ref_soal_essay.'
                        </div>
                    </div>
                </h4>
            </div>
                </a>
                <div id="collapseOne" class="collapse show" data-parent="#accordion" style="">
                    <div class="card-body">
                    <textarea class="form-control" rows="5" placeholder="Masukkan jawaban anda"></textarea>
                    </div>
                </div>
                </div>';
    
                $no++;
    
            }
    
            $data['tombol'] = '<a href="'.site_url().'cms/essay/detail_soal/'.$id.'/2" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>';

        }
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

}
