<?php

class Linkzoom Extends CI_Controller{

    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index()
	{
        $data['title'] = "Link Zoom";
		$data['breadcrumb'] = "Link Zoom";


        $data['table'] = '
        <table class="table table-bordered" id="example2">
            <thead>
            <tr>
            <th width="10px">No</th>
            <th>Tanggal</th>
            <th>Nama Peserta</th>
            <th>Kursus</th>
            <th>Nama Tentor</th>
            <th>Link Zoom</th>
            <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>';

		$data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

}
