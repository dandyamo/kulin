<?php

class Fakultas Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Fakultas";
		$data['title'] = "Data Fakultas";

        $from = array(
			'ref_fakultas rf' => '',
            'ref_kategori rk' => array('rk.id_ref_kategori = rf.id_ref_kategori','left')
		);

        $ref_fakultas = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'rf.ref_fakultas ASC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_fakultas->result() as $fak){

            if($fak->status == 1) {
                $statusx = anchor(site_url('cms/fakultas/tidak_aktif/'.$fak->id_ref_fakultas.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('cms/fakultas/aktif/'.$fak->id_ref_fakultas.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'cms/referensi/fakultas/ubah/'.$fak->id_ref_fakultas.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$fak->id_ref_fakultas.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$fak->ref_fakultas.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$fak->id_ref_fakultas.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/fakultas/hapus/'.$fak->id_ref_fakultas.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30px">Status</th>
                <th>Fakultas</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/referensi/fakultas/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{
        $data['breadcrumb'] = "Tambah Fakultas";
		$data['title'] = "Tambah Fakultas";

        $kategori = $this->Model_general->combo_box(array('tabel'=>'ref_kategori', 'key'=>'id_ref_kategori', 'val'=>array('ref_kategori')));

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="permalink" id="permalink">
            <div class="form-group">
            <label>Kategori</label>
            '.form_dropdown('id_ref_kategori', $kategori, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Fakultas</label>
            <input name="ref_fakultas" type="text" id="title" class="form-control" placeholder="Fakultas">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/fakultas';
		$data['link'] = ''.site_url().'cms/fakultas/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah Fakultas";
		$data['title'] = "Ubah Fakultas";

        $from = array(
			'ref_fakultas rf' => '',
            'ref_kategori rk' => array('rk.id_ref_kategori = rf.id_ref_kategori','left')
		);

        $ref_fakultas = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rf.id_ref_fakultas'=> $id)))->row();

        $kategori = $this->Model_general->combo_box(array('tabel'=>'ref_kategori', 'key'=>'id_ref_kategori', 'val'=>array('ref_kategori')));

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_ref_fakultas" value="'.$ref_fakultas->id_ref_fakultas.'">
            <input type="hidden" name="permalink" id="permalink" value="'.$ref_fakultas->permalink.'">
            <div class="form-group">
            <label>Kategori</label>
            '.form_dropdown('id_ref_kategori', $kategori, $ref_fakultas->id_ref_kategori,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Fakultas</label>
            <input type="text" name="ref_fakultas" id="title" class="form-control" value="'.$ref_fakultas->ref_fakultas.'" placeholder="Fakultas">
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/referensi/fakultas';
        $data['link'] = ''.site_url().'cms/fakultas/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_fakultas = $this->input->post('id_ref_fakultas');
	    $id_ref_kategori = $this->input->post('id_ref_kategori');
	    $ref_fakultas = $this->input->post('ref_fakultas');
	    $permalink = $this->input->post('permalink');

        $par = array(
            'tabel'=>'ref_fakultas',
            'data'=>array(
            'id_ref_kategori'=>$id_ref_kategori,
            'ref_fakultas'=>$ref_fakultas,
            'link_fakutlas'=>$permalink,
            'status'=> 1
            ),
        );

        if($id_ref_fakultas != NULL) $par['where'] = array('id_ref_fakultas'=>$id_ref_fakultas);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/referensi/fakultas');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_fakultas',$data,'id_ref_fakultas',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/referensi/fakultas');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_fakultas',$data,'id_ref_fakultas',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/referensi/fakultas');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_fakultas','id_ref_fakultas',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/referensi/fakultas');
	}

}
