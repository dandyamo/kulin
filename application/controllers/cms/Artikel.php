<?php

class Artikel Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("cms/login"));
        }
	}

	public function index($status = 1)
	{
        $data['breadcrumb'] = "Data Artikel";
		$data['title'] = "Data Artikel";

        $from = array(
			'artikel a' => '',
            'ref_kategori_artikel rk' => array('a.id_kategori_artikel = rk.id_kategori_artikel','left'),
            'operator o' => array('o.id_operator = a.id_operator','left'),
		);

        $artikel = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'a.tgl_artikel DESC'));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($artikel->result() as $sli){

            if($sli->status == 1) {
                $statusx = anchor(site_url('cms/artikel/tidak_aktif/'.$sli->id_artikel.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('cms/artikel/aktif/'.$sli->id_artikel.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'cms/artikel/ubah/'.$sli->id_artikel.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$sli->id_artikel.'"><i class="fa fa-trash"></i></a>';

            $photo = !empty($sli->berkas) ? FCPATH.'uploads/file_artikel/'.$sli->berkas : null;
            $background = (file_exists($photo) and !empty($sli->berkas)) ? base_url().'uploads/file_artikel/'.$sli->berkas : base_url().'assets/backend/dist/img/credit-card.png';

            $table .='<tr>
            <td>'.$no.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$sli->nama_artikel.'</td>
            <td>'.$sli->kategori_artikel.'</td>
            <td>'.$sli->tgl_artikel.'</td>
            <td><center><img style="width:200px;" src="'.$background.'"></center></td>
            <td>'.$sli->nama_operator.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$sli->id_artikel.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'cms/artikel/hapus/'.$sli->id_artikel.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="30x">Status</th>
                <th>Nama Artikel</th>
                <th>Kategori Artikel</th>
                <th>Tanggal Artikel</th>
                <th>Foto</th>
                <th>Created By</th>
                <th width="20px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'cms/artikel/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        $data['content'] = "cms/data_view";
		$this->load->view('home', $data);
	}

	public function tambah()
	{

    $data['breadcrumb'] = "Tambah Artikel";
    $data['title'] = "Tambah Artikel";

    $kategori = $this->Model_general->combo_box(array('tabel'=>'ref_kategori_artikel', 'key'=>'id_kategori_artikel', 'val'=>array('kategori_artikel')));

    $id = $this->session->userdata('id');

    $data['form_data'] = '
    <input type="hidden" name="id_operator" value="'.$id.'">
    <div class="card-body">
        <div class="form-group">
        <label>Nama Artikel</label>
        <input name="nama_artikel" type="text" class="form-control" placeholder="Artikel">
        </div>
        <div class="form-group">
            <label>Kategori Artikel</label>
            '.form_dropdown('id_kategori_artikel', $kategori, '','class="form-control" style="width: 100%"').'
        </div>
        <div class="form-group">
        <label>Deskripsi Artikel</label>
        <textarea name="deskripsi_artikel" type="text" class="form-control" placeholder="Deskripsi Artikel"></textarea>
        </div>
        <div class="form-group">
        <label>Gambar Artikel</label>
        <input name="gambar_artikel" type="file" class="form-control" style="line-height: 1.1;">
        </div>
    </div>
    <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/artikel';
		$data['link'] = ''.site_url().'cms/artikel/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null)
	{
        $data['breadcrumb'] = "Ubah artikel";
		$data['title'] = "Ubah artikel";

        $from = array(
			'artikel rs' => '',
		);

        $artikel = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rs.id_artikel'=> $id)))->row();

        if ($id!=NULL) {
            $logo = '<p><a href="'.base_url('uploads/file_artikel/'.@$artikel->berkas).'" width="300">'.@$artikel->berkas.'</a></p>';
        }

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_artikel" value="'.$artikel->id_artikel.'">
            '.form_hidden('val_berkas_file', @$artikel->berkas, 'class="form-control"').'
            <div class="form-group">
            <label>Nama Bank</label>
            <input type="text" name="artikel" class="form-control" value="'.$artikel->artikel.'" placeholder="artikel">
            </div>
            <div class="form-group">
            <label>Berkas</label>
            <input name="berkas" type="file" class="form-control" style="line-height: 1.1;">
            '.$logo.'
            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'cms/artikel';
        $data['link'] = ''.site_url().'cms/artikel/simpan';
		$data['content'] = "cms/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_operator = $this->input->post('id_operator');
	    $nama_artikel = $this->input->post('nama_artikel');
	    $deskripsi_artikel = $this->input->post('deskripsi_artikel');
	    $id_kategori_artikel = $this->input->post('id_kategori_artikel');

        $par = array(
            'tabel'=>'artikel',
            'data'=>array(
            'id_operator'=>$id_operator,
            'nama_artikel'=>$nama_artikel,
            'deskripsi_artikel'=>$deskripsi_artikel,
            'id_kategori_artikel'=>$id_kategori_artikel,
            'tgl_artikel'=>date('Y-m-d'),
            'status'=> 1
            ),
        );

        $gambar_artikel = $_FILES['gambar_artikel']['tmp_name'];
			if($gambar_artikel!=NULL){
				$path = './uploads/file_artikel';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				// if ($in['val_gambar_artikel']!=NULL) {
				// 	$prev = $in['val_gambar_artikel'];
				// 	$path_pasfoto = $path.'/'.$prev;
				// 	if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				// }
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('gambar_artikel');
			    $data = $this->upload->data();
			    $gambar_artikel = $data['file_name'];
			    $par['data']['gambar_artikel'] = $gambar_artikel;
			}

        if($id_artikel != NULL) $par['where'] = array('id_artikel'=>$id_artikel);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('cms/artikel');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('artikel',$data,'id_artikel',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('cms/artikel');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('artikel',$data,'id_artikel',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('cms/artikel');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('artikel','id_artikel',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/artikel');
	}

}
