<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cart extends CI_Controller {

	public function index()
	{
		$data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$data['content'] = "front/cart";
		$this->load->view('front/index', $data);
	}
}
