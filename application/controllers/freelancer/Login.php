<?php

class Login Extends CI_Controller{

	public function index()
	{
		$this->load->view('freelancer/login');
	}

	public function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => $password);
		$freelancer = $this->Model_general->login('freelancer',$where)->row_array();
		
		if ($freelancer > 0) {

			$data_session = array(
				'username' => $username,
				'nama' => $freelancer['nama_freelancer'],
				'id' => $freelancer['id_freelancer'],
				'foto' => $freelancer['photo'],
				'role' => $freelancer['role'],
				'status' => TRUE
				);

			$this->session->set_userdata($data_session);
			redirect(base_url("freelancer/home"));
			
		}

			redirect(base_url("freelancer/login"));
            
	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

}
