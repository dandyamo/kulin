<?php

class Profil Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("freelancer/login"));
        }
	}

	public function index($status = 1)
	{
		$data['title'] = "Profil";
		$data['breadcrumb'] = "Profil";
        $freelancer = $this->Model_general->getfreelancer();

        $rekening = $this->Model_general->getdata(array('tabel'=>'rek_freelancer', 'order'=>'bank_freelancer ASC','where'=>array('id_freelancer'=>$freelancer->id_freelancer)));

        
        $tab_rek = '';
        $no = 1;
        $data['modal'] ='';
        foreach($rekening->result() as $rek){

          if($rek->status == 1) {
            $statusx = anchor(site_url('freelancer/profil/tidak_aktif/'.$rek->id_rek_freelancer.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
          }else{
            $statusx = anchor(site_url('freelancer/profil/aktif/'.$rek->id_rek_freelancer.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
          }

          $edit = '<a href="#" class="btn btn-xs btn-warning" title="Klik untuk edit data" data-toggle="modal" data-target="#edit'.$rek->id_rek_freelancer.'"><i class="fa fa-edit"></i></a>';
          $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$rek->id_rek_freelancer.'"><i class="fa fa-trash"></i></a>';

          $tab_rek .='<tr>
            <td>'.$no.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$rek->bank_freelancer.'</td>
            <td>'.$rek->an_freelancer.'</td>
            <td>'.$rek->rek_freelancer.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';


          $no++;

          $data['modal'] .= '
            <div class="modal fade" id="hapus'.$rek->id_rek_freelancer.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'freelancer/profil/hapus_rek/'.$rek->id_rek_freelancer.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          
          <div class="modal fade" id="edit'.$rek->id_rek_freelancer.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                <form action="'.site_url().'freelancer/profil/save_rek" method="post">
                    <input type="hidden" name="id_rek_freelancer" value="'.$rek->id_rek_freelancer.'">
                    <input type="hidden" name="id_freelancer" value="'.$rek->id_freelancer.'">
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Atas Nama</label>
                    <div class="col-sm-9">
                        <input type="text" name="an_freelancer" value="'.$rek->an_freelancer.'" class="form-control" placeholder="Atas Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-9">
                        <input type="text" name="bank_freelancer" value="'.$rek->bank_freelancer.'" class="form-control" placeholder="Nama Bank">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">No Rekening</label>
                    <div class="col-sm-9">
                        <input type="text" name="rek_freelancer" value="'.$rek->rek_freelancer.'" class="form-control" placeholder="No Rekening">
                    </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          ';

        }

        $jenis_kelamin = array(
			'' => '-- Pilih --',
			'Laki-laki' => 'Laki-laki',
			'Perempuan' => 'Perempuan'
		);

        $data['data'] ='
        <!-- About Me Box -->
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Tentang Saya</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong><i class="fas fa-user mr-1"></i> Jenis Kelamin</strong>
        
            <p class="text-muted">
                '.$freelancer->jenis_kelamin.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-phone mr-1"></i> No Telepon</strong>
        
            <p class="text-muted">
                '.$freelancer->no_telp.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-envelope mr-1"></i> Email</strong>
        
            <p class="text-muted">
                '.$freelancer->email.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
        
            <p class="text-muted">'.$freelancer->alamat.'</p>
        
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
        <div class="card">
            <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#profil" data-toggle="tab">Ubah Profil</a></li>
                <li class="nav-item"><a class="nav-link" href="#password" data-toggle="tab">Ubah Password</a></li>
                <li class="nav-item"><a class="nav-link" href="#photo" data-toggle="tab">Foto Profil</a></li>
                <li class="nav-item"><a class="nav-link" href="#rekening" data-toggle="tab">Rekening</a></li>
            </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
            <div class="tab-content">
                <div class="active tab-pane" id="profil">
                <form class="form-horizontal" action="'.site_url().'freelancer/profil/simpan_profil" method="post">
                    <input type="hidden" name="id_freelancer" value="'.$freelancer->id_freelancer.'">
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_freelancer" value="'.$freelancer->nama_freelancer.'" placeholder="Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        '.form_dropdown('jenis_kelamin', $jenis_kelamin, $freelancer->jenis_kelamin,'class="form-control"').'
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">No Telepon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="no_telp" value="'.$freelancer->no_telp.'" placeholder="No Telepon">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" value="'.$freelancer->email.'" placeholder="Email">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Alamat">'.$freelancer->alamat.'</textarea>
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" value="'.$freelancer->username.'" placeholder="Username">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="password">
                <form class="form-horizontal" action="'.site_url().'freelancer/profil/simpan_password" method="post">
                    <input type="hidden" name="id_freelancer" value="'.$freelancer->id_freelancer.'">
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_baru" placeholder="Password Baru">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Konfirmasi Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_konfirm" placeholder="Konfirmasi Password Baru">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="photo">
                <form class="form-horizontal" action="'.site_url().'freelancer/profil/simpan_foto" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_freelancer" value="'.$freelancer->id_freelancer.'">
                    '.form_hidden('val_photo', @$video->photo, 'class="form-control"').'
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Foto Profil</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="photo" style="line-height: 1.1;">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                    <button type="submit" class="btn btn-success float-right ml-2">Simpan <i class="fa fa-save"></i></button>
                    <a href="'.site_url().'freelancer/profil/reset_foto/'.$freelancer->id_freelancer.'" class="btn btn-danger float-right">Reset <i class="fa fa-sync-alt"></i></a>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="rekening">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="#" class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#rekening'.$freelancer->id_freelancer.'"><i class="fa fa-plus"></i> Tambah Data</a>
                        </div>
                    </div>
                    <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th style="width: 10px">No</th>
                        <th style="width: 15px">Status</th>
                        <th>Bank</th>
                        <th>Atas Nama</th>
                        <th>No Rekening</th>
                        <th style="width: 75px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tab_rek.'
                    </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="color: red">*Pastikan no rekening aktif untuk proses pembayaran</p>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->';

        $data['modal'] .= '
            <div class="modal fade" id="rekening'.$freelancer->id_freelancer.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Data Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                <form action="'.site_url().'freelancer/profil/save_rek" method="post">
                    <input type="hidden" name="id_freelancer" value="'.$freelancer->id_freelancer.'">
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Atas Nama</label>
                    <div class="col-sm-9">
                        <input type="text" name="an_freelancer" class="form-control" placeholder="Atas Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-9">
                        <input type="text" name="bank_freelancer" class="form-control" placeholder="Nama Bank">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">No Rekening</label>
                    <div class="col-sm-9">
                        <input type="text" name="rek_freelancer" class="form-control" placeholder="No Rekening">
                    </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          ';

        $data['ava'] = !empty($freelancer->photo) ? ''.base_url().'uploads/photo/'.$freelancer->photo.'' : ''.base_url().'assets/backend/dist/img/user.png';

		$data['content'] = "freelancer/profil";
		$this->load->view('home', $data);
	}

    public function simpan_profil()
	{
	    $id_freelancer = $this->input->post('id_freelancer');
	    $nama_freelancer = $this->input->post('nama_freelancer');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');

        $par = array(
            'tabel'=>'freelancer',
            'data'=>array(
            'nama_freelancer'=>$nama_freelancer,
            'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
            'email'=>$email,
            'alamat'=>$alamat,
            'username'=>$username,
            'role'=> 3
            ),
        );

        if($id_freelancer != NULL) $par['where'] = array('id_freelancer'=>$id_freelancer);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('freelancer/profil');

	}

    public function simpan_password()
	{
	    $id_freelancer = $this->input->post('id_freelancer');
	    $password_baru = $this->input->post('password_baru');
	    $password_konfirm = $this->input->post('password_konfirm');


        if($password_baru == $password_konfirm){

            $par = array(
                'tabel'=>'freelancer',
                'data'=>array(
                'password'=>$password_konfirm
                ),
            );
    
            if($id_freelancer != NULL) $par['where'] = array('id_freelancer'=>$id_freelancer);
    
            $sim = $this->Model_general->save_data($par);
    
            $this->session->set_flashdata('ok', 'Password Berhasil Disimpan');
            
            redirect('freelancer/profil');

        } else {

            $this->session->set_flashdata('fail', 'Password Tidak Cocok! Cek kembali');

            redirect('freelancer/profil');

        }

	}

    public function simpan_foto()
	{
	    $id_freelancer = $this->input->post('id_freelancer');

        $par = array(
            'tabel'=>'freelancer',
            'data'=>array(
                'role' => 3
            ),
        );

        $photo = $_FILES['photo']['tmp_name'];
			if($photo!=NULL){
				$path = './uploads/photo';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				if ($in['val_photo']!=NULL) {
					$prev = $in['val_photo'];
					$path_pasfoto = $path.'/'.$prev;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('photo');
			    $data = $this->upload->data();
			    $photo = $data['file_name'];
			    $par['data']['photo'] = $photo;
			}

        if($id_freelancer != NULL) $par['where'] = array('id_freelancer'=>$id_freelancer);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('freelancer/profil');

	}

    public function reset_foto($id = null)
	{

            $foto = $this->Model_general->getdata(array(
                'tabel' =>'freelancer',
                'where' => array('id_freelancer' => $id)
            ))->row();
            
            $path = './uploads/photo/'.$foto->photo;
            unlink($path);

            $par = array(
                'tabel'=>'freelancer',
                'data'=>array(
                'photo'=> NULL
                ),
            );
    
            if($id != NULL) $par['where'] = array('id_freelancer'=>$id);
    
            $sim = $this->Model_general->save_data($par);
    
            $this->session->set_flashdata('ok', 'Photo Berhasil Direset');
            
            redirect('freelancer/profil');

	}

    public function save_rek()
	{
	    $id_rek_freelancer = $this->input->post('id_rek_freelancer');
	    $id_freelancer = $this->input->post('id_freelancer');
	    $bank_freelancer = $this->input->post('bank_freelancer');
	    $an_freelancer = $this->input->post('an_freelancer');
    	$rek_freelancer = $this->input->post('rek_freelancer');

        $par = array(
            'tabel'=>'rek_freelancer',
            'data'=>array(
            'id_freelancer'=>$id_freelancer,
            'bank_freelancer'=>$bank_freelancer,
            'an_freelancer'=>$an_freelancer,
            'rek_freelancer'=>$rek_freelancer,
            'status'=> 1
            ),
        );

        if($id_rek_freelancer != NULL) $par['where'] = array('id_rek_freelancer'=>$id_rek_freelancer);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('freelancer/profil');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('rek_freelancer',$data,'id_rek_freelancer',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('freelancer/profil');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('rek_freelancer',$data,'id_rek_freelancer',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('freelancer/profil');
	}


    function hapus_rek($id=null) {

		$del = $this->Model_general->delete_data('rek_freelancer','id_rek_freelancer',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('freelancer/profil');
	}
}
