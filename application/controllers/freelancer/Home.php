<?php

class Home Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("freelancer/login"));
        }
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$data['breadcrumb'] = "Dashboard";

		$id = $this->session->userdata('id');

		$data['tot_video'] = $this->Model_general->getdata(array('tabel'=>'video','id_freelancer'=>$id))->num_rows();
		$data['video_aktif'] = $this->Model_general->getdata(array('tabel'=>'video','where'=>array('status'=> 1,'id_freelancer'=>$id)))->num_rows();
		$data['video_pending'] = $this->Model_general->getdata(array('tabel'=>'video','where'=>array('status'=> 0,'id_freelancer'=>$id)))->num_rows();

		$from = array(
		'video v' => '',
		'ref_kategori rk' => array('rk.id_ref_kategori = v.id_ref_kategori','left'),
		'ref_matkul rm' => array('rm.id_ref_matkul = v.id_ref_matkul','left'),
		'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left'),
		'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
		'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
			);

		$video = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('v.status'=> 0,'v.id_ref_kategori'=> 1,'v.id_freelancer'=>$id)));


		$data['video'] = '';

		$no = 1;
		foreach($video->result() as $vid){

			if($vid->status == 1){

				$data['video'] .= '<tr style="background:#00FF00">
					<td>'.$no.'</td>
					<td>'.$vid->nama_video.'</td>
					<td>'.$vid->ref_fakultas.'</td>
					<td>'.$vid->ref_jurusan.'</td>
					<td>'.$vid->ref_matkul.'</td>
					</tr>
				';

			}else{

				$data['video'] .= '<tr style="background:#dc3545; color: #fff;">
					<td>'.$no.'</td>
					<td>'.$vid->nama_video.'</td>
					<td>'.$vid->ref_fakultas.'</td>
					<td>'.$vid->ref_jurusan.'</td>
					<td>'.$vid->ref_matkul.'</td>
					</tr>
				';

			}

		  $no++;

		}

		$from_umum = array(
			'video v' => '',
			'ref_umum ru' => array('ru.id_ref_umum = v.id_ref_umum','left')
				);
	
			$video_umum = $this->Model_general->getdata(array('tabel'=>$from_umum,'where'=>array('v.status'=> 0,'v.id_ref_kategori'=> 2,'v.id_freelancer'=>$id)));
	
	
			$data['video_umum'] = '';
	
			$no = 1;
			foreach($video_umum->result() as $vid){
	
				if($vid->status == 1){
	
					$data['video_umum'] .= '<tr style="background:#00FF00">
						<td>'.$no.'</td>
						<td>'.$vid->nama_video.'</td>
						<td>'.$vid->ref_umum.'</td>
						</tr>
					';
	
				}else{
	
					$data['video_umum'] .= '<tr style="background:#dc3545; color: #fff;">
						<td>'.$no.'</td>
						<td>'.$vid->nama_video.'</td>
						<td>'.$vid->ref_umum.'</td>
						</tr>
					';
	
				}
	
			  $no++;
	
			}

		$data['content'] = "freelancer/home";
		$this->load->view('home', $data);
	}

}
