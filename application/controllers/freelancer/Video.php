<?php

class Video Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("freelancer/login"));
        }
	}

	public function index()
	{
    $this->list_data();
	}

	public function list_data($aktif = NULL,$status = 1)
	{

    if ($aktif == null) {
        $aktif = 1;
    }

		$data['title'] = "Video";
		$data['breadcrumb'] = "Video";

        $id_freelancer = $this->session->userdata('id');

        if($aktif == 1){

          $from = array(
            'video v' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = v.id_ref_matkul','left'),
          );
  
          $select = 'v.*,rm.ref_matkul';

          $video = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'v.nama_video ASC','where'=>array('id_freelancer'=>$id_freelancer,'id_ref_kategori'=> 1)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($video->result() as $vid){

            if($vid->aktif == 1) {
                $statusx = anchor(site_url('freelancer/video/tidak_aktif/'.$vid->id_video.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('freelancer/video/aktif/'.$vid->id_video.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'freelancer/video/ubah/'.$vid->id_video.'/'.$aktif.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$vid->id_video.'"><i class="fa fa-trash"></i></a>';
            $view = '<a href="'.site_url().'freelancer/video/detail/'.$vid->id_video.'" class="btn btn-info btn-xs" title="Klik untuk melihat video"><i class="fa fa-video"></i></a>';

            if($vid->status == 1) {
                $verifikasi = '<center><i class="fa fa-star"></i></center>';
            }else{
                $verifikasi = '';
            }

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$verifikasi.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$vid->nama_video.'</td>
            <td>'.$vid->ref_matkul.'</td>
            <td>Rp. '.@number_format($vid->harga_buy,2,",",".").'</td>
            <td>'.$vid->ukuran.' KB</td>
            <td>'.$vid->views.'</td>
            <td>'.$view.' '.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$vid->id_video.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'freelancer/video/hapus/'.$vid->id_video.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="10px"></th>
                <th width="30x">Aktif</th>
                <th>Nama Video</th>
                <th>Materi</th>
                <th>Harga</th>
                <th>Ukuran File</th>
                <th>Views</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>
            <div class="row">
                <div class="col-lg-12">
                <i class="fa fa-star"></i> Video Diterima
                </div>
            </div>';

        $data['tombol'] = '<a href="'.site_url().'freelancer/video/tambah/1" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';

        } else {

          $from = array(
            'video v' => '',
            'ref_umum rm' => array('rm.id_ref_umum = v.id_ref_umum','left'),
          );
  
          $select = 'v.*,rm.ref_umum';
          
          $video = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'v.nama_video ASC','where'=>array('id_freelancer'=>$id_freelancer,'id_ref_kategori'=> 2)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($video->result() as $vid){

            if($vid->aktif == 1) {
                $statusx = anchor(site_url('freelancer/video/tidak_aktif/'.$vid->id_video.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
            }else{
                
                $statusx = anchor(site_url('freelancer/video/aktif/'.$vid->id_video.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
            }

            $edit = '<a href="'.site_url().'freelancer/video/ubah/'.$vid->id_video.'/'.$aktif.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$vid->id_video.'"><i class="fa fa-trash"></i></a>';
            $view = '<a href="'.site_url().'freelancer/video/detail/'.$vid->id_video.'" class="btn btn-info btn-xs" title="Klik untuk melihat video"><i class="fa fa-video"></i></a>';

            if($vid->status == 1) {
                $verifikasi = '<center><i class="fa fa-star"></i></center>';
            }else{
                $verifikasi = '';
            }

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$verifikasi.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$vid->nama_video.'</td>
            <td>'.$vid->ref_umum.'</td>
            <td>Rp. '.@number_format($vid->harga_buy,2,",",".").'</td>
            <td>'.$vid->ukuran.' KB</td>
            <td>'.$vid->views.'</td>
            <td>'.$view.' '.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$vid->id_video.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'freelancer/video/hapus/'.$vid->id_video.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';
        }

        $data['table'] = '
            <table class="table table-bordered" id="example2">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th width="10px"></th>
                <th width="30x">Aktif</th>
                <th>Nama Video</th>
                <th>Materi</th>
                <th>Harga</th>
                <th>Ukuran File</th>
                <th>Views</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>
            <div class="row">
                <div class="col-lg-12">
                <i class="fa fa-star"></i> Video Diterima
                </div>
            </div>';

        $data['tombol'] = '<a href="'.site_url().'freelancer/video/tambah/2" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';
        }
        
		$data['content'] = "freelancer/video_view";
		$this->load->view('home', $data);
	}

    public function detail($id = null)
	{
        $data['breadcrumb'] = "Detail Video";
		    $data['title'] = "Detail Video";

        $from = array(
			  'video v' => '',
        'ref_kategori rk' => array('rk.id_ref_kategori = v.id_ref_kategori','left'),
        'ref_matkul rm' => array('rm.id_ref_matkul = v.id_ref_matkul','left'),
        'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left'),
        'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
        'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
		    );

        $video = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('v.id_video'=> $id)))->row();

        $data['detail'] ='
        <!-- About Me Box -->
        <div class="row">
        <div class="col-lg-3">
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Detail Video</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong>Kategori</strong>
        
            <p class="text-muted">
                '.$video->ref_kategori.'
            </p>
        
            <hr>
        
            <strong>Fakultas</strong>
        
            <p class="text-muted">
                '.$video->ref_fakultas.'
            </p>
        
            <hr>
        
            <strong>Jurusan</strong>
        
            <p class="text-muted">
                '.$video->ref_jurusan.'
            </p>
        
            <hr>
        
            <strong>Semester</strong>
        
            <p class="text-muted">'.$video->ref_semester.'</p>
        
            <hr>
        
            <strong>Mata Kuliah</strong>
        
            <p class="text-muted">'.$video->ref_matkul.'</p>
        
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
            <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                  <h3>'.$video->nama_video.'</h3>
                  <video oncontextmenu="return false;" width="100%" height="350px" controls controlsList="nodownload">
                      <source src="'.base_url('uploads/file_video/'.urlencode($video->berkas_file)).'" type="video/mp4">
                  </video>
                </div><!-- /.card-body -->
            </div>
            <!-- /.card -->
            </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->';

		$data['back'] = ''.site_url().'freelancer/video';
		$data['content'] = "freelancer/detail_view";
		$this->load->view('home', $data);
	}

    public function tambah($aktif=NULL)
	{
        $data['breadcrumb'] = "Tambah Video";
	    	$data['title'] = "Tambah Video";

        $id_freelancer = $this->session->userdata('id');

        if($aktif == 1){

        $matkul = $this->Model_general->combo_box(array('tabel'=>'ref_matkul', 'key'=>'id_ref_matkul', 'val'=>array('ref_matkul')));

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_freelancer" value="'.$id_freelancer.'">
            '.form_hidden('id_ref_kategori', '1', '','class="form-control" style="width: 100%"').'
            <div class="form-group">
            <label>Mata Kuliah</label>
            '.form_dropdown('id_ref_matkul', $matkul, '','class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Nama Video</label>
            <input name="nama_video" type="text" class="form-control" placeholder="Nama Video">
            </div>
            <div class="form-group">
            <label>Harga</label>
            <input name="harga" type="number" class="form-control" placeholder="Harga">
            </div>
            <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="ckeditor" type="text" class="form-control" placeholder="Keterangan"></textarea>
            </div>
            <div class="form-group">
            <label>Cover</label>
            <input name="thumbnail" type="file" class="form-control" style="line-height: 1.1;">
            </div>
            <div class="form-group">
            <label>Video</label>
            <input name="berkas_file" type="file" class="form-control" style="line-height: 1.1;">
            </div>
        </div>
        <!-- /.card-body -->';


    $data['script'] ='<script>
    var ckeditor = CKEDITOR.replace("ckeditor",{
          height:"100px"
    });
    
    CKEDITOR.disableAutoInline = true;
    </script>';

        } else {

          $umum = $this->Model_general->combo_box(array('tabel'=>'ref_umum', 'key'=>'id_ref_umum', 'val'=>array('ref_umum')));

          $data['form_data'] = '
          <div class="card-body">
              <input type="hidden" name="id_freelancer" value="'.$id_freelancer.'">
              '.form_hidden('id_ref_kategori', '2', '','class="form-control" style="width: 100%"').'
              <div class="form-group">
              <label>Kursus Umum</label>
              '.form_dropdown('id_ref_umum', $umum, '','class="form-control" style="width: 100%"').'
              </div>
              <div class="form-group">
              <label>Nama Video</label>
              <input name="nama_video" type="text" class="form-control" placeholder="Nama Video">
              </div>
              <div class="form-group">
              <label>Harga</label>
              <input name="harga" type="number" class="form-control" placeholder="Harga">
              </div>
              <div class="form-group">
              <label>Keterangan</label>
              <textarea name="keterangan" id="ckeditor" type="text" class="form-control" placeholder="Keterangan"></textarea>
              </div>
              <div class="form-group">
              <label>Cover</label>
              <input name="thumbnail" type="file" class="form-control" style="line-height: 1.1;">
              </div>
              <div class="form-group">
              <label>Video</label>
              <input name="berkas_file" type="file" class="form-control" style="line-height: 1.1;">
              </div>
          </div>
          <!-- /.card-body -->';
  
  
      $data['script'] ='<script>
      var ckeditor = CKEDITOR.replace("ckeditor",{
            height:"100px"
      });
      
      CKEDITOR.disableAutoInline = true;
      </script>';

        }

		$data['back'] = ''.site_url().'freelancer/video';
		$data['link'] = ''.site_url().'freelancer/video/simpan';
		$data['content'] = "freelancer/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null, $aktif = NULL)
	{
        $data['breadcrumb'] = "Ubah video";
		$data['title'] = "Ubah video";

        $id_freelancer = $this->session->userdata('id');

        $from = array(
        'video v' => '',
      );

      $video = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('v.id_video'=> $id)))->row();

      if($aktif == 1){

        $matkul = $this->Model_general->combo_box(array('tabel'=>'ref_matkul', 'key'=>'id_ref_matkul', 'val'=>array('ref_matkul')));

        if ($id!=NULL) {
            $thumbnail = '<p><a href="'.base_url('uploads/thumbnail/'.@$video->thumbnail).'" width="300">'.@$video->thumbnail.'</a></p>';
            $file_video = '<p><a href="'.base_url('uploads/file_video/'.@$video->berkas_file).'" width="300">'.@$video->berkas_file.'</a></p>';
        }

        $data['form_data'] = '
        <div class="card-body">
            <input type="hidden" name="id_freelancer" value="'.$id_freelancer.'">
            <input type="hidden" name="id_video" value="'.$video->id_video.'">
            '.form_hidden('val_berkas_file', @$video->berkas_file, 'class="form-control"').'
            '.form_hidden('val_thumbnail', @$video->thumbnail, 'class="form-control"').'
            '.form_hidden('id_ref_kategori', 1,'class="form-control" style="width: 100%"').'
            <div class="form-group">
            <label>Mata Kuliah</label>
            '.form_dropdown('id_ref_matkul', $matkul, $video->id_ref_matkul,'class="form-control" style="width: 100%"').'
            </div>
            <div class="form-group">
            <label>Nama Video</label>
            <input name="nama_video" type="text" class="form-control" value="'.$video->nama_video.'" placeholder="Nama Video">
            </div>
            <div class="form-group">
            <label>Harga</label>
            <input name="harga" type="text" class="form-control" value="'.$video->harga_buy.'" placeholder="Harga">
            </div>
            <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" id="ckeditor" type="text" class="form-control" placeholder="Keterangan">'.$video->keterangan.'</textarea>
            </div>
            <div class="form-group">
            <label>Cover</label>
            <input name="thumbnail" type="file" class="form-control" style="line-height: 1.1;">
            '.$thumbnail.'
            </div>
            <div class="form-group">
            <label>Video</label>
            <input name="berkas_file" type="file" class="form-control" style="line-height: 1.1;">
            '.$file_video.'
            </div>
        </div>
        <!-- /.card-body -->';

        $data['script'] ='<script>
    var ckeditor = CKEDITOR.replace("ckeditor",{
          height:"100px"
    });
    
    CKEDITOR.disableAutoInline = true;
    </script>';

        } else {
  
          if ($id!=NULL) {
              $thumbnail = '<p><a href="'.base_url('uploads/thumbnail/'.@$video->thumbnail).'" width="300">'.@$video->thumbnail.'</a></p>';
              $file_video = '<p><a href="'.base_url('uploads/file_video/'.@$video->berkas_file).'" width="300">'.@$video->berkas_file.'</a></p>';
          }
  
          $data['form_data'] = '
          <div class="card-body">
              <input type="hidden" name="id_freelancer" value="'.$id_freelancer.'">
              <input type="hidden" name="id_video" value="'.$video->id_video.'">
              '.form_hidden('val_berkas_file', @$video->berkas_file, 'class="form-control"').'
              '.form_hidden('val_thumbnail', @$video->thumbnail, 'class="form-control"').'
              '.form_hidden('id_ref_kategori', 2,'class="form-control" style="width: 100%"').'
              <div class="form-group">
              <label>Nama Video</label>
              <input name="nama_video" type="text" class="form-control" value="'.$video->nama_video.'" placeholder="Nama Video">
              </div>
              <div class="form-group">
              <label>Harga</label>
              <input name="harga" type="text" class="form-control" value="'.$video->harga_buy.'" placeholder="Harga">
              </div>
              <div class="form-group">
              <label>Keterangan</label>
              <textarea name="keterangan" id="ckeditor" type="text" class="form-control" placeholder="Keterangan">'.$video->keterangan.'</textarea>
              </div>
              <div class="form-group">
              <label>Cover</label>
              <input name="thumbnail" type="file" class="form-control" style="line-height: 1.1;">
              '.$thumbnail.'
              </div>
              <div class="form-group">
              <label>Video</label>
              <input name="berkas_file" type="file" class="form-control" style="line-height: 1.1;">
              '.$file_video.'
              </div>
          </div>
          <!-- /.card-body -->';
  
          $data['script'] ='<script>
      var ckeditor = CKEDITOR.replace("ckeditor",{
            height:"100px"
      });
      
      CKEDITOR.disableAutoInline = true;
      </script>';

        }

        

		$data['back'] = ''.site_url().'freelancer/video';
        $data['link'] = ''.site_url().'freelancer/video/simpan';
		$data['content'] = "freelancer/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_video = $this->input->post('id_video');
	    $id_freelancer = $this->input->post('id_freelancer');
	    $id_ref_kategori = $this->input->post('id_ref_kategori');
	    $id_ref_matkul = $this->input->post('id_ref_matkul');
	    $nama_video = $this->input->post('nama_video');
	    $harga = $this->input->post('harga');
	    $keterangan = $this->input->post('keterangan');

        $par = array(
            'tabel'=>'video',
            'data'=>array(
            'id_freelancer'=>$id_freelancer,
            'id_ref_kategori'=>$id_ref_kategori,
            'id_ref_matkul'=>$id_ref_matkul,
            'nama_video'=>$nama_video,
            'keterangan'=>$keterangan,
            'harga_buy'=>$harga,
            'aktif'=> 1,
            'status'=> 0
            ),
        );

        $thumbnail = $_FILES['thumbnail']['tmp_name'];
			if($thumbnail!=NULL){
				$path = './uploads/thumbnail';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				if ($in['val_thumbnail']!=NULL) {
					$prev = $in['val_thumbnail'];
					$path_pasfoto = $path.'/'.$prev;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('thumbnail');
			    $data = $this->upload->data();
			    $thumbnail = $data['file_name'];
			    $par['data']['thumbnail'] = $thumbnail;
			}

        $berkas_file = $_FILES['berkas_file']['tmp_name'];
        if($berkas_file!=NULL){
            $path = './uploads/file_video';
            if (!is_dir($path)) mkdir($path,0777,TRUE);
        
            if ($in['val_berkas_file']!=NULL) {
                $prev = $in['val_berkas_file'];
                $path_pasfoto = $path.'/'.$prev;
                if(file_exists($path_pasfoto)) unlink($path_pasfoto);
            }
            $this->load->library('upload');
            $config['upload_path'] = $path;
            $config['allowed_types'] = '*';
            $config['max_size']      = '0';
            $this->upload->initialize($config);
            $this->upload->do_upload('berkas_file');
            $data = $this->upload->data();
            $berkas_file = $data['file_name'];
            $file_size = $data['file_size'];
            $par['data']['berkas_file'] = $berkas_file;
            $par['data']['ukuran'] = $file_size;
		}

        if($id_video != NULL) $par['where'] = array('id_video'=>$id_video);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('freelancer/video');

	}

    function aktif($id=null,$status) {
		$data = array('aktif'	=> '1');
		$this->Model_general->save_data('video',$data,'id_video',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('freelancer/video');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('aktif'	=> '0');
		$this->Model_general->save_data('video',$data,'id_video',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('freelancer/video');
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('video','id_video',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('freelancer/video');
	}

}
