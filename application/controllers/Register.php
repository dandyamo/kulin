<?php

class Register Extends CI_Controller{

	public function index()
	{
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$data['content'] = "front/register";
		$this->load->view('front/index', $data);
	}

	public function action_register()
	{
	    $nama_peserta = $this->input->post('nama_peserta');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');


		if($password == $this->input->post('conf_password')){

			$par = array(
				'tabel'=>'peserta',
				'data'=>array(
				'nama_peserta'=>$nama_peserta,
				'jenis_kelamin'=>$jenis_kelamin,
				'no_telp'=>$no_telp,
				'email'=>$email,
				'alamat'=>$alamat,
				'username'=>$username,
				'password'=>md5($password)
				),
			);

			$sim = $this->Model_general->save_data($par);
	
			$this->session->set_flashdata('ok_register', 'Berhasil');

			$this->send_notif($email);

			redirect('login');
	
			
		}else{

			$this->session->set_flashdata('fail_register', 'Gagal');
			redirect('register');
			
		}
		
	}

	public function send_notif($email)
    {

        // Konfigurasi email
			$config['protocol']    = 'smtp';
			$config['smtp_host']    = 'mail.kulin.id';
			$config['smtp_port']    = 465;
			$config['smtp_timeout'] = '7';
			$config['smtp_user']    = 'info@kulin.id'; // email akun
			$config['smtp_pass']    = 'Kulinofficial22'; // password email
			$config['charset']    = 'utf-8';
			$config['smtp_crypto']    = 'ssl';
			$config['newline']  = "\r\n";
			$config['mailtype'] = 'html'; // or html
			$config['validation'] = TRUE; // bool whether to validate email or not


			$pesan = 'Yeay!, Anda sudah terdaftar di Kulin';

			$this->load->library('email', $config);
			$this->email->initialize($config);
			
			$this->email->from('info@kulin.id', 'Register Kulin.id');
			$this->email->to($email); 
			$this->email->subject('Pendaftaran Berhasil');
			$this->email->message($pesan);
			$sent = $this->email->send();

    }

}
