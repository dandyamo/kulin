<?php

class Soal Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("tentor/login"));
        }
	}

	public function index()
	{
    $this->list_data();
	}

	public function list_data($aktif = NULL)
	{
        $data['breadcrumb'] = "Data Soal";
		$data['title'] = "Data Soal";

        $id = $this->session->userdata('id');

        $tentor = $this->Model_general->gettentor();

        if (!empty($tentor->ref_matkul)) {

            $from = array(
                'ref_soal_essay rse' => '',
                'ref_matkul rm' => array('rm.id_ref_matkul = rse.id_ref_matkul','left'),
                'ref_semester rs' => array('rs.id_ref_semester = rm.id_ref_semester','left'),
                'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
                'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left')
            );
    
            $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'group_by'=>'rse.id_ref_matkul','order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_tentor'=>$id)));
    
            $table = '';
            $data['modal'] = '';
            $no = 1;
            foreach($ref_soal_essay->result() as $gan){
    
                $jml = $this->Model_general->getdata(array('tabel'=> $from ,'where'=>array('rse.id_ref_matkul'=>$gan->id_ref_matkul,'rse.id_tentor'=>$id)))->num_rows();
    
                $detail = '<a href="'.site_url().'tentor/soal/detail_soal/'.$gan->id_ref_matkul.'" class="btn btn-xs btn-info" title="Klik untuk Lihat data"><i class="fa fa-file-alt"></i> List Soal</a>';
    
                $table .='<tr>
                <td>'.$no.'</td>
                <td>'.$gan->ref_matkul.'</td>
                <td>'.$gan->ref_semester.'</td>
                <td>'.$gan->ref_jurusan.'</td>
                <td>'.$gan->ref_fakultas.'</td>
                <td>Essay</td>
                <td>'.$jml.'</td>
                <td>'.$detail.'</td>
                </tr>';
    
                $no++;
    
            }
    
            $data['table'] = '
                <table class="table table-bordered" id="example1">
                  <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Mata Kuliah</th>
                    <th>Semester</th>
                    <th>Jurusan</th>
                    <th>Fakultas</th>
                    <th>Jenis</th>
                    <th>Jumlah Soal</th>
                    <th width="60px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    '.$table.'
                  </tbody>
                </table>';
    
            $data['tombol'] = '<a href="'.site_url().'tentor/soal/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';

        } else {

            $from = array(
                'ref_soal_essay rse' => '',
                'ref_umum ru' => array('ru.id_ref_umum = rse.id_ref_umum','left')
            );
    
            $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'group_by'=>'rse.id_ref_umum','order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_tentor'=>$id)));
    
            $table = '';
            $data['modal'] = '';
            $no = 1;
            foreach($ref_soal_essay->result() as $gan){
    
                $jml = $this->Model_general->getdata(array('tabel'=> $from ,'where'=>array('rse.id_ref_umum'=>$gan->id_ref_umum,'rse.id_tentor'=>$id)))->num_rows();
    
                $detail = '<a href="'.site_url().'tentor/soal/detail_soal/'.$gan->id_ref_umum.'" class="btn btn-xs btn-info" title="Klik untuk Lihat data"><i class="fa fa-file-alt"></i> List Soal</a>';
    
                $table .='<tr>
                <td>'.$no.'</td>
                <td>'.$gan->ref_umum.'</td>
                <td>Essay</td>
                <td>'.$jml.'</td>
                <td>'.$detail.'</td>
                </tr>';
    
                $no++;
    
            }
    
            $data['table'] = '
                <table class="table table-bordered" id="example1">
                  <thead>
                  <tr>
                    <th width="10px">No</th>
                    <th>Kursus</th>
                    <th>Jenis</th>
                    <th>Jumlah Soal</th>
                    <th width="60px">Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                    '.$table.'
                  </tbody>
                </table>';
    
            $data['tombol'] = '<a href="'.site_url().'tentor/soal/tambah" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>';

        }

        

        $data['content'] = "tentor/data_view";
		$this->load->view('home', $data);
	}

    public function detail_soal($id = null, $status = 1)
	{
        $data['breadcrumb'] = "Data Soal";
		$data['title'] = "Data Soal";

        $tentor = $this->session->userdata('id');

        $dt_tentor = $this->Model_general->gettentor();

        if (!empty($dt_tentor->ref_matkul)) {

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rse.id_ref_matkul','left')
		);


        $select = 'rse.*,rm.ref_matkul';
        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_matkul'=>$id,'rse.id_tentor'=>$tentor)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $ess){

            if($ess->status == 1) {

                $statusx = anchor(site_url('tentor/soal/tidak_aktif/'.$ess->id_ref_soal_essay.'/'.$id.''),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');

            }else{
                
                $statusx = anchor(site_url('tentor/soal/aktif/'.$ess->id_ref_soal_essay.'/'.$id.''),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');

            }

            $edit = '<a href="'.site_url().'tentor/soal/ubah/'.$ess->id_ref_soal_essay.'/'.$id.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$ess->id_ref_soal_essay.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$ess->ref_soal_essay.'</td>
            <td>'.$ess->jawaban.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$ess->id_ref_soal_essay.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'tentor/soal/hapus/'.$ess->id_ref_soal_essay.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';

        }

        $data['table'] = '
            <h3>Soal '.$ref_soal_essay->row()->ref_matkul.'</h3>
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Status</th>
                <th>Judul Soal</th>
                <th>Kunci Jawaban</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'tentor/soal" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'tentor/soal/demo/'.$id.'/1" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Demo Soal</a>';

    } else {

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_umum ru' => array('ru.id_ref_umum = rse.id_ref_umum','left')
		);


        $select = 'rse.*,ru.ref_umum';
        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select,'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_umum'=>$id,'rse.id_tentor'=>$tentor)));

        $table = '';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $ess){

            if($ess->status == 1) {

                $statusx = anchor(site_url('tentor/soal/tidak_aktif/'.$ess->id_ref_soal_essay.'/'.$id.''),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');

            }else{
                
                $statusx = anchor(site_url('tentor/soal/aktif/'.$ess->id_ref_soal_essay.'/'.$id.''),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');

            }

            $edit = '<a href="'.site_url().'tentor/soal/ubah/'.$ess->id_ref_soal_essay.'/'.$id.'" class="btn btn-xs btn-warning" title="Klik untuk edit data"><i class="fa fa-edit"></i></a>';
            $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$ess->id_ref_soal_essay.'"><i class="fa fa-trash"></i></a>';

            $table .='<tr>
            <td>'.$no.'</td>
            <td>'.$statusx.'</td>
            <td>'.$ess->ref_soal_essay.'</td>
            <td>'.$ess->jawaban.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';

            $no++;

            $data['modal'] .= '
            <div class="modal fade" id="hapus'.$ess->id_ref_soal_essay.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'tentor/soal/hapus/'.$ess->id_ref_soal_essay.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>';

        }

        $data['table'] = '
            <h3>Soal '.$ref_soal_essay->row()->ref_umum.'</h3>
            <table class="table table-bordered" id="example1">
              <thead>
              <tr>
                <th width="10px">No</th>
                <th>Status</th>
                <th>Judul Soal</th>
                <th>Kunci Jawaban</th>
                <th width="60px">Aksi</th>
              </tr>
              </thead>
              <tbody>
                '.$table.'
              </tbody>
            </table>';

        $data['tombol'] = '<a href="'.site_url().'tentor/soal" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a> <a href="'.site_url().'tentor/soal/demo/'.$id.'/1" class="btn btn-sm btn-info"><i class="fa fa-eye"></i> Demo Soal</a>';

    }
        $data['content'] = "tentor/data_view";
		$this->load->view('home', $data);
	}

    public function demo($id = null, $aktif = null)
	{
        $data['breadcrumb'] = "Demo Soal";
		$data['title'] = "Demo Soal";

        $tentor = $this->session->userdata('id');

        $dt_tentor = $this->Model_general->gettentor();

        if (!empty($dt_tentor->ref_matkul)) {

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = rse.id_ref_matkul','left')
		);

        $select = 'rse.*,rm.ref_matkul';
        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_matkul'=>$id,'rse.status'=>1,'rse.id_tentor'=>$tentor)));

        $data['table'] = '<h3>Soal '.$ref_soal_essay->row()->ref_matkul.'</h3>';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $gan){

            $data['table'] .='
            <div class="card card-primary card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title w-100">
                        <div class="row">
                            <div class="col-sm-1" style="margin-right: -60px">
                                '.$no.'. 
                            </div>
                            <div class="col-sm-11">
                                '.$gan->ref_soal_essay.'
                            </div>
                        </div>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="collapse show" data-parent="#accordion" style="">
            <div class="card-body">
            <textarea class="form-control" rows="5" placeholder="Masukkan jawaban anda"></textarea>
            </div>
            </div>
            </div>';

            $no++;

        }

        $data['tombol'] = '<a href="'.site_url().'tentor/soal/detail_soal/'.$id.'" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>';

    } else {

        $from = array(
			'ref_soal_essay rse' => '',
            'ref_umum ru' => array('ru.id_ref_umum = rse.id_ref_umum','left')
		);

        $select = 'rse.*,ru.ref_umum';
        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rse.ref_soal_essay ASC','where'=>array('rse.id_ref_umum'=>$id,'rse.status'=>1,'rse.id_tentor'=>$tentor)));

        $data['table'] = '<h3>Soal '.$ref_soal_essay->row()->ref_umum.'</h3>';
        $data['modal'] = '';
        $no = 1;
        foreach($ref_soal_essay->result() as $gan){

            $data['table'] .='
            <div class="card card-primary card-outline">
            <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" aria-expanded="true">
                <div class="card-header">
                    <h4 class="card-title w-100">
                        <div class="row">
                            <div class="col-sm-1" style="margin-right: -60px">
                                '.$no.'. 
                            </div>
                            <div class="col-sm-11">
                                '.$gan->ref_soal_essay.'
                            </div>
                        </div>
                    </h4>
                </div>
            </a>
            <div id="collapseOne" class="collapse show" data-parent="#accordion" style="">
            <div class="card-body">
            <textarea class="form-control" rows="5" placeholder="Masukkan jawaban anda"></textarea>
            </div>
            </div>
            </div>';

            $no++;

        }

        $data['tombol'] = '<a href="'.site_url().'tentor/soal/detail_soal/'.$id.'" class="btn btn-sm btn-default"><i class="fa fa-arrow-left"></i> Kembali</a>';

    }
        $data['content'] = "tentor/data_view";
		$this->load->view('home', $data);
	}

    public function tambah()
	{
        $data['breadcrumb'] = "Tambah Soal";
		$data['title'] = "Tambah Soal";
        
        $id = $this->session->userdata('id');

        $dt_tentor = $this->Model_general->gettentor();

        if (!empty($dt_tentor->ref_matkul)) {

        $matkul = $this->Model_general->getdata(array('tabel'=>'tentor','where'=>array('id_tentor'=>$id)))->row();

        $data['form_data'] = '
        <input type="hidden" name="id_ref_matkul" value="'.$matkul->id_ref_matkul.'">
        <input type="hidden" name="id_tentor" value="'.$id.'">
        <input type="hidden" name="id_ref_kategori" value="1">
        <div class="card-body">
            <div class="form-group">
            <label>Soal Essay</label>
            <textarea name="ref_soal_essay" id="ckeditor" type="text" class="form-control" placeholder="Soal Essay"></textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            <textarea name="jawaban" id="ckeditor1" type="text" class="form-control" placeholder="Kunci Jawaban"></textarea>
            </div>
        </div>
        <!-- /.card-body -->';

        } else {

        $umum = $this->Model_general->getdata(array('tabel'=>'tentor','where'=>array('id_tentor'=>$id)))->row();

        $data['form_data'] = '
        <input type="hidden" name="id_ref_umum" value="'.$umum->id_ref_umum.'">
        <input type="hidden" name="id_tentor" value="'.$id.'">
        <input type="hidden" name="id_ref_kategori" value="2">
        <div class="card-body">
            <div class="form-group">
            <label>Soal Essay</label>
            <textarea name="ref_soal_essay" id="ckeditor" type="text" class="form-control" placeholder="Soal Essay"></textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            <textarea name="jawaban" id="ckeditor1" type="text" class="form-control" placeholder="Kunci Jawaban"></textarea>
            </div>
        </div>
        <!-- /.card-body -->';

        }

        $data['script'] ='
        <script type="text/javascript">
            $(function () {
                    CKEDITOR.replace("ckeditor",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });
                    CKEDITOR.replace("ckeditor1",{
                        filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                        height: "100px"
                    });
                });
        </script>';

		$data['back'] = ''.site_url().'tentor/soal';
		$data['link'] = ''.site_url().'tentor/soal/simpan';
		$data['content'] = "tentor/form_view";
		$this->load->view('home', $data);
	}

    public function ubah($id = null, $id_detail = null)
	{
        $data['breadcrumb'] = "Ubah Soal";
		$data['title'] = "Ubah Soal";
        
        $tentor = $this->session->userdata('id');

        $dt_tentor = $this->Model_general->gettentor();

        if (!empty($dt_tentor->ref_matkul)) {

        $from = array(
            'ref_soal_essay rse' => '',
        );

        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rse.id_ref_soal_essay'=> $id)))->row();

        $matkul = $this->Model_general->getdata(array('tabel'=>'tentor','where'=>array('id_tentor'=>$id)))->row();
        

        $data['form_data'] = '
        <input type="hidden" name="id_tentor" value="'.$tentor.'">
        <input type="hidden" name="id_ref_matkul" value="'.$matkul->id_ref_matkul.'">
        <input type="hidden" name="id_ref_kategori" value="1">
        <div class="card-body">
            <div class="form-group">
            <label>Soal Essay</label>
            <textarea name="ref_soal_essay" id="ckeditor" type="text" class="form-control" placeholder="Soal Essay">'.$ref_soal_essay->ref_soal_essay.'</textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            <textarea name="jawaban" id="ckeditor1" type="text" class="form-control" placeholder="Kunci Jawaban">'.$ref_soal_essay->jawaban.'</textarea>
            </div>
        </div>
        <!-- /.card-body -->';

    } else {

        $from = array(
            'ref_soal_essay rse' => '',
        );

        $ref_soal_essay = $this->Model_general->getdata(array('tabel'=>$from,'where'=>array('rse.id_ref_soal_essay'=> $id)))->row();

        $umum = $this->Model_general->getdata(array('tabel'=>'tentor','where'=>array('id_tentor'=>$id)))->row();
        

        $data['form_data'] = '
        <input type="hidden" name="id_tentor" value="'.$tentor.'">
        <input type="hidden" name="id_ref_umum" value="'.$umum->id_ref_umum.'">
        <input type="hidden" name="id_ref_kategori" value="2">
        <div class="card-body">
            <div class="form-group">
            <label>Soal Essay</label>
            <textarea name="ref_soal_essay" id="ckeditor" type="text" class="form-control" placeholder="Soal Essay">'.$ref_soal_essay->ref_soal_essay.'</textarea>
            </div>
            <div class="form-group">
            <label>Kunci Jawaban</label>
            <textarea name="jawaban" id="ckeditor1" type="text" class="form-control" placeholder="Kunci Jawaban">'.$ref_soal_essay->jawaban.'</textarea>
            </div>
        </div>
        <!-- /.card-body -->';

    }

        
        $data['script'] ='
        <script type="text/javascript">
        $(function () {
            CKEDITOR.replace("ckeditor",{
                filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                height: "100px"
            });
            CKEDITOR.replace("ckeditor1",{
                filebrowserImageBrowseUrl : "'.base_url('assets/backend/kcfinder/browse.php').'",
                height: "100px"
            });
        });
        </script>';
        
        $data['back'] = ''.site_url().'tentor/soal/detail_soal/'.$id_detail.'';
		$data['link'] = ''.site_url().'tentor/soal/simpan';
		$data['content'] = "tentor/form_view";
		$this->load->view('home', $data);
	}

    public function simpan()
	{
	    $id_ref_soal_essay = $this->input->post('id_ref_soal_essay');
	    $ref_soal_essay = $this->input->post('ref_soal_essay');
	    $id_ref_kategori = $this->input->post('id_ref_kategori');
	    $id_ref_matkul = $this->input->post('id_ref_matkul');
	    $id_ref_umum = $this->input->post('id_ref_umum');
	    $id_tentor = $this->input->post('id_tentor');
	    $jawaban = $this->input->post('jawaban');

        $par = array(
            'tabel'=>'ref_soal_essay',
            'data'=>array(
            'id_ref_kategori'=>$id_ref_kategori,
            'id_ref_matkul'=>$id_ref_matkul,
            'id_ref_umum'=>$id_ref_umum,
            'id_tentor'=>$id_tentor,
            'ref_soal_essay'=>$ref_soal_essay,
            'jawaban'=>$jawaban,
            'status'=>1,
            ),
        );

        if($id_ref_soal_essay != NULL) $par['where'] = array('id_ref_soal_essay'=>$id_ref_soal_essay);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('tentor/soal');

	}

    function aktif($id=null,$status,$aktif) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('ref_soal_essay',$data,'id_ref_soal_essay',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('tentor/soal/detail_soal/'.$status.'/'.$aktif);
	}

	
	function tidak_aktif($id=null,$status,$aktif) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('ref_soal_essay',$data,'id_ref_soal_essay',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('tentor/soal/detail_soal/'.$status.'/'.$aktif);
	}

    function hapus($id=null) {

		$del = $this->Model_general->delete_data('ref_soal_essay','id_ref_soal_essay',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('cms/soal/essay');
	}

}
