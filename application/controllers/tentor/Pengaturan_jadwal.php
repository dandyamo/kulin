<?php

class Pengaturan_jadwal Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("tentor/login"));
        }
	}

	public function index()
	{
		$this->tambah();
	}

    public function tambah()
	{
        $data['breadcrumb'] = "Tambah Jadwal";
		$data['title'] = "Tambah Jadwal";
        
        $id = $this->session->userdata('id');

        $matkul = $this->Model_general->getdata(array('tabel'=>'tentor','where'=>array('id_tentor'=>$id)))->row();

        $data['form_data'] = '
        <input type="hidden" name="id_tentor" value="'.$id.'">
        <div class="card-body">
            <div class="form-group">

            <label>Jadwal</label>

            <div class="table-responsive">
                <table class="table table-bordered">
                    <input type="hidden" id="jumlahjadwal" name="jmlphone" value="1">
                    <thead id="element_jadwal">
                        <tr class="rec-element-jadwal">
                            <td style="width: 10px;">1</td>
                            <td><div class="form-group">
                            <label>Tanggal</label>
                            <input name="tgl_jadwal0" type="date" class="form-control" placeholder="...">
                            </div>
                            </td>
                            <td><div class="form-group">
                            <label>Waktu</label>
                            <input name="waktu0" type="time" class="form-control" placeholder="...">
                            </div>
                            </td>
                            <td style="width: 10px;"><a href="#" id="add_jadwal" class="btn btn-flat btn-sm btn-success"><i class="fas fa-plus"></i></a></td>
                        </tr>
                        <div class="clear"></div>
                </thead>
                </table>
            </div>

            </div>
        </div>
        <!-- /.card-body -->';

		$data['back'] = ''.site_url().'tentor/soal';
		$data['link'] = ''.site_url().'tentor/soal/simpan';
		$data['content'] = "tentor/form_view";
		$this->load->view('home', $data);
	}

}
