<?php

class Pendapatan Extends CI_Controller{

    public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("tentor/login"));
        }
	}

	public function index()
	{
        $data['title'] = "Pendapatan";
		$data['breadcrumb'] = "Pendapatan";


        $data['table'] = '
        <table class="table table-bordered" id="example2">
            <thead>
            <tr>
            <th width="10px">No</th>
            <th>Tanggal</th>
            <th>Pendapatan</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>';

		$data['content'] = "tentor/data_view";
		$this->load->view('home', $data);
	}

}
