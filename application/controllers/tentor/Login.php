<?php

class Login Extends CI_Controller{

	public function index()
	{
		$from = array(
			'ref_kategori rk' => ''
		);

		$select = 'rk.*';
		$data['ref_kategori'] = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rk.ref_kategori ASC'));
		$this->load->view('tentor/login',$data);
	}

	public function aksi_login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password));
		$tentor = $this->Model_general->login('tentor',$where)->row_array();
		
		if ($tentor > 0) {

			$data_session = array(
				'username' => $username,
				'nama' => $tentor['nama_tentor'],
				'id' => $tentor['id_tentor'],
				'foto' => $tentor['photo'],
				'role' => $tentor['role'],
				'status' => TRUE
				);

			$this->session->set_userdata($data_session);
			redirect(base_url("tentor/home"));
			
		}

			redirect(base_url("tentor/login"));
            
	}

	public function aksi_register()
	{
	    $nama_tentor = $this->input->post('nama_tentor');
    	$email = $this->input->post('email');
    	$username = $this->input->post('username');
    	$password = $this->input->post('password');
    	$id_ref_kategori = $this->input->post('id_ref_kategori');
    	$id_kursus = $this->input->post('id_kursus');

		if($id_ref_kategori == 1) {

			$par = array(
				'tabel'=>'tentor',
				'data'=>array(
				'id_ref_kategori'=>$id_ref_kategori,
				'id_ref_matkul'=>$id_kursus,
				'nama_tentor'=>$nama_tentor,
				'email'=>$email,
				'username'=>$username,
				'password'=>md5($password),
				'role'=> 4
				),
			);

		} else {

			$par = array(
				'tabel'=>'tentor',
				'data'=>array(
				'id_ref_kategori'=>$id_ref_kategori,
				'id_ref_umum'=>$id_kursus,
				'nama_tentor'=>$nama_tentor,
				'email'=>$email,
				'username'=>$username,
				'password'=>md5($password),
				'role'=> 4
				),
			);

		}

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('tentor/login');

	}

	function logout(){
		$this->session->sess_destroy();
		redirect(base_url());
	}

	function get_matkul(){
        $id_ref_kategori=$this->input->post('id');

		$from = array(
			'ref_matkul rm' => '',
			'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left')
		);

		$select = 'rm.*,rf.id_ref_kategori';
		$data = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'rm.ref_matkul ASC','where'=>array('rf.id_ref_kategori'=>$id_ref_kategori)))->result();
        echo json_encode($data);
    }

	function get_umum(){
        $id_ref_kategori=$this->input->post('id');

		$from = array(
			'ref_umum ru' => '',
			'ref_kat_umum rku' => array('rku.id_ref_kat_umum = ru.id_ref_kat_umum','left')
		);

		$select = 'ru.*,rku.id_ref_kategori';
		$data = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ru.ref_umum ASC','where'=>array('rku.id_ref_kategori'=>$id_ref_kategori)))->result();
        echo json_encode($data);
    }

}
