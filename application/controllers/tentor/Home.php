<?php

class Home Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("tentor/login"));
        }
	}

	public function index()
	{
		$data['title'] = "Dashboard";
		$data['breadcrumb'] = "Dashboard";

		$id = $this->session->userdata('id');

		$data['table']='<table class="table table-bordered">
		<thead>
		<tr>
		<th width="10px">No</th>
		<th>Status</th>
		<th>Tanggal</th>
		<th>Nama Peserta</th>
		<th>Kursus</th>
		<th>Link Zoom</th>
		</tr>
		</thead>
		<tbody>
		<tr style="background:#00FF00">
			<td>1</td>
			<td><center><i class="btn btn-xs btn-default">Berlangsung</a></center></td>
			<td>11-7-2014</td>
			<td>John Doe</td>
			<td>John Doe</td>
			<td>John Doe</td>
		</tr>
		<tr style="background:#F0E68C">
			<td>2</td>
			<td><center><i class="btn btn-xs btn-default">Rencana</a></center></td>
			<td>11-7-2014</td>
			<td>John Doe</td>
			<td>John Doe</td>
			<td>John Doe</td>
		</tr>
		</tbody>
	</table>';

		$data['content'] = "tentor/home";
		$this->load->view('home', $data);
	}

}
