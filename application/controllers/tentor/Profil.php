<?php

class Profil Extends CI_Controller{

	public function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != TRUE){
            redirect(base_url("tentor/login"));
        }
	}

	public function index($status = 1)
	{
		$data['title'] = "Profil";
		$data['breadcrumb'] = "Profil";
        $tentor = $this->Model_general->gettentor();

        if (!empty($tentor->ref_matkul)) {

            $kursus = $tentor->ref_matkul;

        } else {

            $kursus = $tentor->ref_umum;
            
        }

        $rekening = $this->Model_general->getdata(array('tabel'=>'rek_tentor', 'order'=>'bank_tentor ASC','where'=>array('id_tentor'=>$tentor->id_tentor)));

        
        $tab_rek = '';
        $no = 1;
        $data['modal'] ='';
        foreach($rekening->result() as $rek){

          if($rek->status == 1) {
            $statusx = anchor(site_url('tentor/profil/tidak_aktif/'.$rek->id_rek_tentor.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tampil</i>','class="btn btn-xs btn-success"');
          }else{
            $statusx = anchor(site_url('tentor/profil/aktif/'.$rek->id_rek_tentor.'/'.$status),'<i class="fa fa-power-off">&nbsp;&nbsp;Tidak Tampil</i>','class="btn btn-xs btn-default"');
          }

          $edit = '<a href="#" class="btn btn-xs btn-warning" title="Klik untuk edit data" data-toggle="modal" data-target="#edit'.$rek->id_rek_tentor.'"><i class="fa fa-edit"></i></a>';
          $hapus = '<a href="#" class="btn btn-danger btn-xs" title="Klik untuk hapus data" data-toggle="modal" data-target="#hapus'.$rek->id_rek_tentor.'"><i class="fa fa-trash"></i></a>';

          $tab_rek .='<tr>
            <td>'.$no.'</td>
            <td><center>'.$statusx.'</center></td>
            <td>'.$rek->bank_tentor.'</td>
            <td>'.$rek->an_tentor.'</td>
            <td>'.$rek->rek_tentor.'</td>
            <td>'.$edit.' '.$hapus.'</td>
            </tr>';


          $no++;

          $data['modal'] .= '
            <div class="modal fade" id="hapus'.$rek->id_rek_tentor.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-trash"></i> Konfirmasi Hapus</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <p>Yakin ini menghapus data ini?</p>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <a href="'.site_url().'tentor/profil/hapus_rek/'.$rek->id_rek_tentor.'" class="btn btn-success">Hapus</a>
                </div>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          
          <div class="modal fade" id="edit'.$rek->id_rek_tentor.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-edit"></i> Edit Data Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                <form action="'.site_url().'tentor/profil/save_rek" method="post">
                    <input type="hidden" name="id_rek_tentor" value="'.$rek->id_rek_tentor.'">
                    <input type="hidden" name="id_tentor" value="'.$rek->id_tentor.'">
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Atas Nama</label>
                    <div class="col-sm-9">
                        <input type="text" name="an_tentor" value="'.$rek->an_tentor.'" class="form-control" placeholder="Atas Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-9">
                        <input type="text" name="bank_tentor" value="'.$rek->bank_tentor.'" class="form-control" placeholder="Nama Bank">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">No Rekening</label>
                    <div class="col-sm-9">
                        <input type="text" name="rek_tentor" value="'.$rek->rek_tentor.'" class="form-control" placeholder="No Rekening">
                    </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          ';

        }

        $jenis_kelamin = array(
			'' => '-- Pilih --',
			'Laki-laki' => 'Laki-laki',
			'Perempuan' => 'Perempuan'
		);

        $data['data'] ='
        <!-- About Me Box -->
        <div class="card card-primary">
            <div class="card-header">
            <h3 class="card-title">Tentang Saya</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <strong><i class="fas fa-user mr-1"></i> Jenis Kelamin</strong>
        
            <p class="text-muted">
                '.$tentor->jenis_kelamin.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-phone mr-1"></i> No Telepon</strong>
        
            <p class="text-muted">
                '.$tentor->no_telp.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-envelope mr-1"></i> Email</strong>
        
            <p class="text-muted">
                '.$tentor->email.'
            </p>
        
            <hr>
        
            <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>
        
            <p class="text-muted">'.$tentor->alamat.'</p>
        
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
        <div class="card">
            <div class="card-header p-2">
            <ul class="nav nav-pills">
                <li class="nav-item"><a class="nav-link active" href="#profil" data-toggle="tab">Ubah Profil</a></li>
                <li class="nav-item"><a class="nav-link" href="#password" data-toggle="tab">Ubah Password</a></li>
                <li class="nav-item"><a class="nav-link" href="#photo" data-toggle="tab">Foto Profil</a></li>
                <li class="nav-item"><a class="nav-link" href="#rekening" data-toggle="tab">Rekening</a></li>
            </ul>
            </div><!-- /.card-header -->
            <div class="card-body">
            <div class="tab-content">
                <div class="active tab-pane" id="profil">
                <form class="form-horizontal" action="'.site_url().'tentor/profil/simpan_profil" method="post">
                    <input type="hidden" name="id_tentor" value="'.$tentor->id_tentor.'">
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Nama</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="nama_tentor" value="'.$tentor->nama_tentor.'" placeholder="Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Jenis Kelamin</label>
                    <div class="col-sm-10">
                        '.form_dropdown('jenis_kelamin', $jenis_kelamin, $tentor->jenis_kelamin,'class="form-control"').'
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">No Telepon</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="no_telp" value="'.$tentor->no_telp.'" placeholder="No Telepon">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="email" value="'.$tentor->email.'" placeholder="Email">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Alamat</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="alamat" placeholder="Alamat">'.$tentor->alamat.'</textarea>
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Username</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="username" value="'.$tentor->username.'" placeholder="Username">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Tentor Kursus</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="'.$kursus.'" readonly>
                        <small>Untuk mengubah kategori kursus, Silahkan menghubungi admin</small>
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="password">
                <form class="form-horizontal" action="'.site_url().'tentor/profil/simpan_password" method="post">
                    <input type="hidden" name="id_tentor" value="'.$tentor->id_tentor.'">
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_baru" placeholder="Password Baru">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Konfirmasi Password Baru</label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="password_konfirm" placeholder="Konfirmasi Password Baru">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-success float-right">Simpan <i class="fa fa-save"></i></button>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="photo">
                <form class="form-horizontal" action="'.site_url().'tentor/profil/simpan_foto" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id_tentor" value="'.$tentor->id_tentor.'">
                    '.form_hidden('val_photo', @$video->photo, 'class="form-control"').'
                    <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Foto Profil</label>
                    <div class="col-sm-8">
                        <input type="file" class="form-control" name="photo" style="line-height: 1.1;">
                    </div>
                    </div>
                    <div class="form-group row">
                    <div class="col-lg-12">
                    <button type="submit" class="btn btn-success float-right ml-2">Simpan <i class="fa fa-save"></i></button>
                    <a href="'.site_url().'tentor/profil/reset_foto/'.$tentor->id_tentor.'" class="btn btn-danger float-right">Reset <i class="fa fa-sync-alt"></i></a>
                    </div>
                    </div>
                </form>
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="rekening">
                    <div class="row">
                        <div class="col-lg-12">
                            <a href="#" class="btn btn-sm btn-primary mb-3" data-toggle="modal" data-target="#rekening'.$tentor->id_tentor.'"><i class="fa fa-plus"></i> Tambah Data</a>
                        </div>
                    </div>
                    <table class="table table-bordered">
                    <thead>
                        <tr>
                        <th style="width: 10px">No</th>
                        <th style="width: 15px">Status</th>
                        <th>Bank</th>
                        <th>Atas Nama</th>
                        <th>No Rekening</th>
                        <th style="width: 75px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        '.$tab_rek.'
                    </tbody>
                    </table>
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="color: red">*Pastikan no rekening aktif untuk proses pembayaran</p>
                        </div>
                    </div>
                </div>
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
            </div><!-- /.card-body -->
        </div>
        <!-- /.card -->
        </div>
        <!-- /.col -->
        </div>
        <!-- /.row -->';

        $data['modal'] .= '
            <div class="modal fade" id="rekening'.$tentor->id_tentor.'">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title"><i class="fa fa-plus"></i> Tambah Data Rekening</h4>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                <form action="'.site_url().'tentor/profil/save_rek" method="post">
                    <input type="hidden" name="id_tentor" value="'.$tentor->id_tentor.'">
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Atas Nama</label>
                    <div class="col-sm-9">
                        <input type="text" name="an_tentor" class="form-control" placeholder="Atas Nama">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Nama Bank</label>
                    <div class="col-sm-9">
                        <input type="text" name="bank_tentor" class="form-control" placeholder="Nama Bank">
                    </div>
                    </div>
                    <div class="form-group row">
                    <label class="col-sm-3 col-form-label">No Rekening</label>
                    <div class="col-sm-9">
                        <input type="text" name="rek_tentor" class="form-control" placeholder="No Rekening">
                    </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                  <button type="button" class="btn btn-danger" data-dismiss="modal">Batal</button>
                  <button type="submit" class="btn btn-success">Simpan</button>
                </div>
                </form>
              </div>
              <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
          </div>
          ';

        $data['ava'] = !empty($tentor->photo) ? ''.base_url().'uploads/photo/'.$tentor->photo.'' : ''.base_url().'assets/backend/dist/img/user.png';

		$data['content'] = "tentor/profil";
		$this->load->view('home', $data);
	}

    public function simpan_profil()
	{
	    $id_tentor = $this->input->post('id_tentor');
	    $nama_tentor = $this->input->post('nama_tentor');
    	$jenis_kelamin = $this->input->post('jenis_kelamin');
    	$no_telp = $this->input->post('no_telp');
    	$email = $this->input->post('email');
    	$alamat = $this->input->post('alamat');
    	$username = $this->input->post('username');

        $par = array(
            'tabel'=>'tentor',
            'data'=>array(
            'nama_tentor'=>$nama_tentor,
            'jenis_kelamin'=>$jenis_kelamin,
            'no_telp'=>$no_telp,
            'email'=>$email,
            'alamat'=>$alamat,
            'username'=>$username,
            'role'=> 4
            ),
        );

        if($id_tentor != NULL) $par['where'] = array('id_tentor'=>$id_tentor);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('tentor/profil');

	}

    public function simpan_password()
	{
	    $id_tentor = $this->input->post('id_tentor');
	    $password_baru = $this->input->post('password_baru');
	    $password_konfirm = $this->input->post('password_konfirm');


        if($password_baru == $password_konfirm){

            $par = array(
                'tabel'=>'tentor',
                'data'=>array(
                'password'=>$password_konfirm
                ),
            );
    
            if($id_tentor != NULL) $par['where'] = array('id_tentor'=>$id_tentor);
    
            $sim = $this->Model_general->save_data($par);
    
            $this->session->set_flashdata('ok', 'Password Berhasil Disimpan');
            
            redirect('tentor/profil');

        } else {

            $this->session->set_flashdata('fail', 'Password Tidak Cocok! Cek kembali');

            redirect('tentor/profil');

        }

	}

    public function simpan_foto()
	{
	    $id_tentor = $this->input->post('id_tentor');

        $par = array(
            'tabel'=>'tentor',
            'data'=>array(
                'role' => 4
            ),
        );

        $photo = $_FILES['photo']['tmp_name'];
			if($photo!=NULL){
				$path = './uploads/photo';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
			
				if ($in['val_photo']!=NULL) {
					$prev = $in['val_photo'];
					$path_pasfoto = $path.'/'.$prev;
					if(file_exists($path_pasfoto)) unlink($path_pasfoto);
				}
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('photo');
			    $data = $this->upload->data();
			    $photo = $data['file_name'];
			    $par['data']['photo'] = $photo;
			}

        if($id_tentor != NULL) $par['where'] = array('id_tentor'=>$id_tentor);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('tentor/profil');

	}

    public function reset_foto($id = null)
	{

            $foto = $this->Model_general->getdata(array(
                'tabel' =>'tentor',
                'where' => array('id_tentor' => $id)
            ))->row();
            
            $path = './uploads/photo/'.$foto->photo;
            unlink($path);

            $par = array(
                'tabel'=>'tentor',
                'data'=>array(
                'photo'=> NULL
                ),
            );
    
            if($id != NULL) $par['where'] = array('id_tentor'=>$id);
    
            $sim = $this->Model_general->save_data($par);
    
            $this->session->set_flashdata('ok', 'Photo Berhasil Direset');
            
            redirect('tentor/profil');

	}

    public function save_rek()
	{
	    $id_rek_tentor = $this->input->post('id_rek_tentor');
	    $id_tentor = $this->input->post('id_tentor');
	    $bank_tentor = $this->input->post('bank_tentor');
	    $an_tentor = $this->input->post('an_tentor');
    	$rek_tentor = $this->input->post('rek_tentor');

        $par = array(
            'tabel'=>'rek_tentor',
            'data'=>array(
            'id_tentor'=>$id_tentor,
            'bank_tentor'=>$bank_tentor,
            'an_tentor'=>$an_tentor,
            'rek_tentor'=>$rek_tentor,
            'status'=> 1
            ),
        );

        if($id_rek_tentor != NULL) $par['where'] = array('id_rek_tentor'=>$id_rek_tentor);

        $sim = $this->Model_general->save_data($par);

        $this->session->set_flashdata('ok', 'Data Berhasil Disimpan');

        redirect('tentor/profil');

	}

    function aktif($id=null,$status) {
		$data = array('status'	=> '1');
		$this->Model_general->save_data('rek_tentor',$data,'id_rek_tentor',$id);
		$this->session->set_flashdata('ok','Data Berhasil diaktifkan');
		redirect('tentor/profil');
	}

	
	function tidak_aktif($id=null,$status) {
		$data = array('status'	=> '0');
		$this->Model_general->save_data('rek_tentor',$data,'id_rek_tentor',$id);
		$this->session->set_flashdata('fail','Data Berhasil dinonaktifkan');
		redirect('tentor/profil');
	}


    function hapus_rek($id=null) {

		$del = $this->Model_general->delete_data('rek_tentor','id_rek_tentor',$id);

		if ($del) {
			$this->session->set_flashdata('ok','Data Berhasil di Hapus');
		}else{
			$this->session->set_flashdata('fail','Data Gagal di Hapus');
		}
		redirect('tentor/profil');
	}
}
