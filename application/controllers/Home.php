<?php

class Home Extends CI_Controller{

	public function index()
	{
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        /* fakultas */
        $kategori = $this->Model_general->getdata(array('tabel'=>'ref_fakultas', 'order'=>'ref_fakultas ASC'));
        $data['kategori'] ='';

        foreach($kategori->result() as $kat){

            $data['kategori'] .='   
            <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                <a class="categories-item" href="'.site_url().'category-list/'.$kat->link_fakultas.'">
                    <div class="icon-part">
                        <img src="'.base_url().'assets/frontend/images/categories/icons/1.png" alt=""> 
                    </div>
                    <div class="content-part">
                        <h4 class="title">'.$kat->ref_fakultas.'</h4>
                        <span class="courses">05 Courses</span>
                    </div>
                </a>
            </div>';

        }

		$data['content'] = "front/home";
		$this->load->view('front/index', $data);
	}

    public function about()
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $data['content'] = "front/about";
		$this->load->view('front/index', $data);
    }

    public function category($offset = null)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $config['base_url']	= site_url('home/category/');
		$config['total_rows'] = $this->Model_general->getdata(array('tabel' => 'ref_fakultas', 'order'=>'ref_fakultas ASC'))->num_rows();
		$config['per_page']		= '9';
		$config['uri_segment']	= '3';

		//tampilan pagination
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next <i class="fa fa-long-arrow-right"></i>';
		$config['prev_link']        = '<i class="fa fa-long-arrow-left"></i> Prev';
		$config['full_tag_open']    = '<ul class="pagination-part">';
		$config['full_tag_close']   = '</ul>';
		$config['num_tag_open']     = '<li><a>';
		$config['num_tag_close']    = '</a></li>';
		$config['cur_tag_open']     = '<li class="active"><a>';
		$config['cur_tag_close']    = '</a></li>';
		$config['next_tag_open']    = '<li><a>';
		$config['next_tagl_close']  = '&raquo;</a></li>';
		$config['prev_tag_open']    = '<li><a>';
		$config['prev_tagl_close']  = '</a>Next</li>';
		$config['first_tag_open']   = '<li><a>';
		$config['first_tagl_close'] = '</a></li>';
		$config['last_tag_open']    = '<li><a>';
		$config['last_tagl_close']  = '</a></li>';

		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
        $lim = $config['per_page'];
        $offs = $offset;
        $kategori = $this->Model_general->getdata(array('tabel'=>'ref_fakultas', 'order'=>'ref_fakultas ASC','limit'=>$lim, 'offset'=>$offs,));
        $data['kategori'] ='';

        foreach($kategori->result() as $kat){

            $data['kategori'] .='
            <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                <a class="categories-item" href="'.site_url().'category-list/'.$kat->link_fakultas.'">
                    <div class="icon-part">
                        <img src="'.base_url().'assets/frontend/images/categories/icons/1.png" alt=""> 
                    </div>
                    <div class="content-part">
                        <h4 class="title">'.$kat->ref_fakultas.'</h4>
                        <span class="courses">05 Courses</span>
                    </div>
                </a>
            </div>';

        }

        $data['content'] = "front/category";
		$this->load->view('front/index', $data);
        
    }

    public function umum($offset = null)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $config['base_url']	= site_url('home/category/');
		$config['total_rows'] = $this->Model_general->getdata(array('tabel' => 'ref_kat_umum', 'order'=>'ref_kat_umum ASC'))->num_rows();
		$config['per_page']		= '9';
		$config['uri_segment']	= '3';

		//tampilan pagination
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next <i class="fa fa-long-arrow-right"></i>';
		$config['prev_link']        = '<i class="fa fa-long-arrow-left"></i> Prev';
		$config['full_tag_open']    = '<ul class="pagination-part">';
		$config['full_tag_close']   = '</ul>';
		$config['num_tag_open']     = '<li><a>';
		$config['num_tag_close']    = '</a></li>';
		$config['cur_tag_open']     = '<li class="active"><a>';
		$config['cur_tag_close']    = '</a></li>';
		$config['next_tag_open']    = '<li><a>';
		$config['next_tagl_close']  = '&raquo;</a></li>';
		$config['prev_tag_open']    = '<li><a>';
		$config['prev_tagl_close']  = '</a>Next</li>';
		$config['first_tag_open']   = '<li><a>';
		$config['first_tagl_close'] = '</a></li>';
		$config['last_tag_open']    = '<li><a>';
		$config['last_tagl_close']  = '</a></li>';

		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
        $lim = $config['per_page'];
        $offs = $offset;
        $kategori = $this->Model_general->getdata(array('tabel'=>'ref_kat_umum', 'order'=>'ref_kat_umum ASC','limit'=>$lim, 'offset'=>$offs,));
        $data['kategori'] ='';

        foreach($kategori->result() as $kat){

            $data['kategori'] .='
            <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                <a class="categories-item" href="'.site_url().'course-umum/'.$kat->link_kat_umum.'">
                    <div class="icon-part">
                        <img src="'.base_url().'assets/frontend/images/categories/icons/1.png" alt=""> 
                    </div>
                    <div class="content-part">
                        <h4 class="title">'.$kat->ref_kat_umum.'</h4>
                        <span class="courses">05 Courses</span>
                    </div>
                </a>
            </div>';

        }

        $data['content'] = "front/umum";
		$this->load->view('front/index', $data);
        
    }

    public function category_list($id = null, $offset = null)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $from = array(
			'ref_jurusan rj' => '',
			'ref_fakultas rf' => array('rf.id_ref_fakultas=rj.id_ref_fakultas','left')
		);

        $select = 'rj.*,rf.link_fakultas';

        $config['base_url']	= site_url('home/category_list/'.$id);
		$config['total_rows'] = $this->Model_general->getdata(array('tabel' => $from,'select'=>$select, 'order'=>'ref_jurusan ASC', 'where'=>array('rf.link_fakultas'=>$id)))->num_rows();
		$config['per_page']		= '9';
		$config['uri_segment']	= '3';

		//tampilan pagination
		$config['first_link']       = 'First';
		$config['last_link']        = 'Last';
		$config['next_link']        = 'Next <i class="fa fa-long-arrow-right"></i>';
		$config['prev_link']        = '<i class="fa fa-long-arrow-left"></i> Prev';
		$config['full_tag_open']    = '<ul class="pagination-part">';
		$config['full_tag_close']   = '</ul>';
		$config['num_tag_open']     = '<li><a>';
		$config['num_tag_close']    = '</a></li>';
		$config['cur_tag_open']     = '<li class="active"><a>';
		$config['cur_tag_close']    = '</a></li>';
		$config['next_tag_open']    = '<li><a>';
		$config['next_tagl_close']  = '&raquo;</a></li>';
		$config['prev_tag_open']    = '<li><a>';
		$config['prev_tagl_close']  = '</a>Next</li>';
		$config['first_tag_open']   = '<li><a>';
		$config['first_tagl_close'] = '</a></li>';
		$config['last_tag_open']    = '<li><a>';
		$config['last_tagl_close']  = '</a></li>';

		$this->pagination->initialize($config);
		$data['links'] = $this->pagination->create_links();
        $lim = $config['per_page'];
        $offs = $offset;
        $list_kategori = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ref_jurusan ASC','limit'=>$lim, 'offset'=>$offs,'where'=>array('rf.link_fakultas'=>$id)));
        $data['list_kategori'] ='';

        foreach($list_kategori->result() as $kat){

            $data['list_kategori'] .='
            <div class="col-lg-4 col-md-6 mb-30 wow fadeInUp" data-wow-delay="300ms" data-wow-duration="2000ms">
                <a class="categories-item" href="'.site_url().'course/'.$kat->link_jurusan.'">
                    <div class="icon-part">
                        <img src="'.base_url().'assets/frontend/images/categories/icons/1.png" alt=""> 
                    </div>
                    <div class="content-part">
                        <h4 class="title">'.$kat->ref_jurusan.'</h4>
                        <span class="courses">05 Courses</span>
                    </div>
                </a>
            </div>';

        }

        $data['content'] = "front/category_list";
		$this->load->view('front/index', $data);
        
    }

    public function course($id)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $from = array(
            'ref_matkul rm' => '',
            'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
            'ref_fakultas rf' => array('rf.id_ref_fakultas = rj.id_ref_fakultas','left')
              );

        $select = 'rm.*,rj.ref_jurusan,rf.ref_fakultas';

        $course = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ref_fakultas ASC'));

        $data['course'] ='';

        foreach($course->result() as $row){

            $data['course'] .='
            <div class="col-lg-4 col-md-6 grid-item filter1">
                <div class="courses-item mb-30">
                    <div class="img-part">
                        <img src="'.base_url().'assets/frontend/images/course-3/1.jpg" alt="">
                    </div>
                    <div class="content-part">
                        <ul class="meta-part">
                            <li><span class="price">$55.00</span></li>
                            <li><a class="categorie" href="#">'.$row->ref_jurusan.'</a></li>
                        </ul>
                        <h3 class="title"><a href="detail-course.php">'.$row->ref_matkul.'</a></h3>
                        <div class="bottom-part">
                            <div class="info-meta">
                                <ul>
                                    <li class="user"><i class="fa fa-user"></i> 245</li>
                                    <li class="user"><i class="fa fa-file"></i> 6 Lessons</li>

                                </ul>
                            </div>
                            <div class="btn-part">
                                <a href="'.site_url().'detail-course/'.$row->link_matkul.'"><i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

        }

        $data['title'] = $course->row()->ref_jurusan;
        $data['content'] = "front/course";
		$this->load->view('front/index', $data);
    }

    public function course_umum($id)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $from = array(
            'ref_umum ru' => '',
            'ref_kat_umum rku' => array('rku.id_ref_kat_umum = ru.id_ref_kat_umum','left')
              );

        $select = 'ru.*,rku.ref_kat_umum';

        $course = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ref_umum ASC'));

        $data['course'] ='';

        foreach($course->result() as $row){

            $data['course'] .='
            <div class="col-lg-4 col-md-6 grid-item filter1">
                <div class="courses-item mb-30">
                    <div class="img-part">
                        <img src="'.base_url().'assets/frontend/images/course-3/1.jpg" alt="">
                    </div>
                    <div class="content-part">
                        <ul class="meta-part">
                            <li><span class="price">$55.00</span></li>
                            <li><a class="categorie" href="#">'.$row->ref_kat_umum.'</a></li>
                        </ul>
                        <h3 class="title"><a href="detail-course.php">'.$row->ref_umum.'</a></h3>
                        <div class="bottom-part">
                            <div class="info-meta">
                                <ul>
                                    <li class="user"><i class="fa fa-user"></i> 245</li>
                                    <li class="user"><i class="fa fa-file"></i> 6 Lessons</li>

                                </ul>
                            </div>
                            <div class="btn-part">
                                <a href="'.site_url().'detail-umum/'.$row->link_umum.'"><i class="flaticon-right-arrow"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>';

        }

        $data['title'] = $course->row()->ref_kat_umum;
        $data['content'] = "front/course_umum";
		$this->load->view('front/index', $data);
    }

    public function detail_course($id)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $from = array(
            'ref_matkul rm' => '',
            'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
            'ref_fakultas rf' => array('rf.id_ref_fakultas = rj.id_ref_fakultas','left')
        );

        $select = 'rm.*,rj.ref_jurusan,rf.ref_fakultas';

        $course = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ref_fakultas ASC','where'=>array('rm.link_matkul'=>$id)))->row();

        $data['judul'] = $course->ref_matkul;
        $data['deskripsi'] = $course->deskripsi;

        $from_video = array(
            'video v' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = v.id_ref_matkul','left'),
          );

        $video = $this->Model_general->getdata(array('tabel'=>$from_video,'order'=>'v.id_video ASC','limit'=>2,'where'=>array('rm.link_matkul'=>$id)));

        $data['video'] ='';

        foreach($video->result() as $row){

            $data['video'] .='
            <div class="content">
                <div class="clearfix">
                    <div class="pull-left">
                        <a class="popup-videos play-icon" href="'.base_url('uploads/file_video/'.urlencode($row->berkas_file)).'"><i class="fa fa-play"></i>'.$row->nama_video.'</a>
                    </div>
                    <div class="pull-right">
                        <div class="minutes">35 Minutes</div>
                    </div>
                </div>
            </div>';

        }

        $video_kunci = $this->db->query('SELECT * FROM `video` `v` LEFT JOIN `ref_matkul` `rm` ON `rm`.`id_ref_matkul` = `v`.`id_ref_matkul` WHERE `rm`.`link_matkul` = "'.$id.'" ORDER BY `v`.`id_video` ASC LIMIT 2,3');
        $data['video_kunci'] ='';

        $data['modal_kunci'] ='';

        foreach($video_kunci->result() as $vk){

            $data['video_kunci'] .='
            <div class="content">
                <div class="clearfix">
                    <div class="pull-left">
                    <a href="#" class=" play-icon"  type="button" data-toggle="modal" data-target="#ModalPlan'.$vk->id_video.'"><span class="fa fa-lock"></span>'.$vk->nama_video.'</a>
                    </div>
                    <div class="pull-right">
                        <div class="minutes"> 35 Minutes </div>
                    </div>
                </div>
            </div>';

            $data['modal_kunci'] .='
             <!-- Modal -->
                <div class="modal fade" id="ModalPlan'.$vk->id_video.'">
                <div class="modal-dialog modal-md  modal-dialog-centered" role="document">
                    <div class="modal-content rounded-0">
                    <div class="modal-body py-0">

                        
                        <div class="d-block main-content">
                        <img src="'.base_url().'assets/frontend/images/undraw_updated_rr85.svg" alt="Image" class="img-fluid" style="background-color: #b2fcff;">
                        <div class="content-text p-4">
                        <h3 class="text-center">This link is password protected</h3>
                        <div class="warp-icon mb-4">
                            <span class="icon-lock2"><i class="fa fa-lock"></i></span>
                            </div>

                            <p class="mb-4">All their equipment and instruments are alive. The sky was this is cloudless and of a deep dark blue. A shining crescent far beneath the flying vessel.</p>

                        

                            <h4 class="mb-4 text-center">Choose Upgrade Your Plan</h4>
                            <div class="d-flex col-md-12">
                        
                            
                                <a href="#" class="btn readon2 orange px-4 w-50 m-2">Watch Video Only</a>
                                <a href="#" class="btn readon2 orange px-4 w-50 m-2"  type="button" data-toggle="modal" data-target="#dada">Include Tutor</a>
                            
                            </div>
                            
                            <div class="ml-auto">
                                <!-- <a href="#" class="btn btn-link f-right" data-dismiss="modal">No thanks</a> -->
                            
                            </div>
                        </div>
                        </div>

                    </div>
                    </div>
                </div>
                </div>';

            

        }

        $from_tentor = array(
            'tentor t' => '',
            'ref_matkul rm' => array('rm.id_ref_matkul = t.id_ref_matkul','left'),
          );

        $tentor = $this->Model_general->getdata(array('tabel'=>$from_tentor,'order'=>'t.id_tentor ASC','where'=>array('rm.link_matkul'=>$id)));

        $data['tentor'] ='';

        foreach($tentor->result() as $row){

            $data['tentor'] .='
            <div class="col-lg-6 col-md-6 col-sm-12 sm-mb-30">
                <div class="team-item">
                    <img src="'.base_url('uploads/photo/'.$row->photo).'" style="width:50%">
                    <div class="content-part">
                        <h4 class="name"><a href="#">'.$row->nama_tentor.'</a></h4>
                        <span class="designation">Professor</span>
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>';

        }

        $data['content'] = "front/detail_course";
		$this->load->view('front/index', $data);
    }

    public function detail_umum($id)
    {
        $data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

        $from = array(
            'ref_umum ru' => '',
            'ref_kat_umum rrk' => array('rrk.id_ref_kat_umum = ru.id_ref_kat_umum','left')
        );

        $select = 'ru.*,rrk.ref_kat_umum';

        $course = $this->Model_general->getdata(array('tabel'=>$from,'select'=>$select, 'order'=>'ref_umum ASC','where'=>array('ru.link_umum'=>$id)))->row();

        $data['judul'] = $course->ref_umum;
        $data['deskripsi'] = $course->deskripsi;

        $from_video = array(
            'video v' => '',
            'ref_umum ru' => array('ru.id_ref_umum = v.id_ref_umum','left'),
          );

        $video = $this->Model_general->getdata(array('tabel'=>$from_video,'order'=>'v.id_video ASC','where'=>array('ru.link_umum'=>$id)));

        $data['video'] ='';

        foreach($video->result() as $row){

            $data['video'] .='
            <div class="content">
                <div class="clearfix">
                    <div class="pull-left">
                        <a class="popup-videos play-icon" href="'.base_url('uploads/file_video/'.urlencode($row->berkas_file)).'"><i class="fa fa-play"></i>'.$row->nama_video.'</a>
                    </div>
                    <div class="pull-right">
                        <div class="minutes">35 Minutes</div>
                    </div>
                </div>
            </div>';

        }

        $from_tentor = array(
            'tentor t' => '',
            'ref_umum ru' => array('ru.id_ref_umum = t.id_ref_umum','left'),
          );

        $tentor = $this->Model_general->getdata(array('tabel'=>$from_tentor,'order'=>'t.id_tentor ASC','where'=>array('ru.link_umum'=>$id)));

        $data['tentor'] ='';

        foreach($tentor->result() as $row){

            $data['tentor'] .='
            <div class="col-lg-6 col-md-6 col-sm-12 sm-mb-30">
                <div class="team-item">
                    <img src="'.base_url('uploads/photo/'.$row->photo).'" style="width:50%">
                    <div class="content-part">
                        <h4 class="name"><a href="#">'.$row->nama_tentor.'</a></h4>
                        <span class="designation">Professor</span>
                        <ul class="social-links">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>';

        }

        $data['content'] = "front/detail_course";
		$this->load->view('front/index', $data);
    }


}
