<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	public function index()
	{
		$data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$id = $this->session->userdata('id');

		$peserta = $this->Model_general->getdata(array('tabel'=>'peserta', 'where'=>array('id_peserta'=>$id)))->row();

        $ava = !empty($peserta->photo) ? ''.base_url().'uploads/photo/'.$peserta->photo.'' : ''.base_url().'assets/backend/dist/img/user.png';

		$data['profile'] = '
		<section class="profile-section orange-color pt-100 pb-100 md-pt-70 md-pb-70"> 
                <div class="container">                   
                    <div class="row clearfix">
                        <!-- Image Section -->
                        <div class="image-column col-lg-5 md-mb-50">
                            <div class="inner-column mb-50 md-mb-0">
                                <div class="image p-4">
                                    <img src="'.$ava.'" alt="" />
                                </div>
                                
                                <div class="social-box">
                                <button class="btn btn-success"> Change Profile </button>
                                  
                                </div>
                            </div>
                        </div>                      
                        <!-- Content Section -->
                        <div class="image-column col-lg-7 pl-60 md-pl-15 md-pt-0">
                            <div class="inner-column">
                                <h4>Nama Lengkap : '.$peserta->nama_peserta.'</h4>
                                <h4>Jenis Kelamin : '.$peserta->jenis_kelamin.'</h4>
								<h4>No Telepon : '.$peserta->no_telp.'</h4>
								<h4>Email : '.$peserta->email.'</h4>
								<h4>Alamat : '.$peserta->alamat.'</h4>

                                <a href="'.site_url().'profile/ubah_profile" class="btn btn-success w-100"> Change Profile </a>

								<a href="'.site_url().'profile/ubah_password" class="btn btn-success w-100 mt-2"> Change Password </a>
                                
                            </div>
                        </div>
                       
                    </div>
                </div>
            </section>';

		$data['content'] = "front/profile";
		$this->load->view('front/index', $data);
	}

	public function ubah_profile()
	{
		$data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$id = $this->session->userdata('id');

		$peserta = $this->Model_general->getdata(array('tabel'=>'peserta', 'where'=>array('id_peserta'=>$id)))->row();

		$data['profile'] = '
		<section class="register-section pt-50 pb-50">
    <div class="container">
	<div class="register-box">
            
            <div class="sec-title text-center mb-30">
                <h2 class="title mb-10">Ubah Profil</h2>
            </div>
            
            <!-- Login Form -->
            <div class="styled-form">
                <div id="form-messages"></div>
                <form method="post" action="'.site_url().'profile/simpan_profil">  
				<input type="hidden" name="id_peserta" value="'.$peserta->id_peserta.'">                             
                    <div class="row clearfix">                                    
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="nama" name="nama_peserta" value="'.$peserta->nama_peserta.'" placeholder="Nama Lengkap" required>
                        </div>
                    
                            <!-- Form Group -->
                            <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="nomor" name="no_telp" value="'.$peserta->no_telp.'" placeholder="Nomor Telephone " required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="email" id="email" name="email" value="'.$peserta->email.'" placeholder="Alamat Email" required>
                        </div>

                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="text" id="alamat" name="alamat" value="'.$peserta->alamat.'" placeholder="Alamat" required>
                        </div>
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-center mb-25">
                            <button type="submit" class="readon register-btn"><span class="txt">Simpan</span></button>
                        </div>
                    </div>
                    
                </form>
            </div>
            
        </div>
		</div>
		</section>';

		$data['content'] = "front/profile";
		$this->load->view('front/index', $data);
	}

	public function ubah_password()
	{
		$data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$id = $this->session->userdata('id');

		$peserta = $this->Model_general->getdata(array('tabel'=>'peserta', 'where'=>array('id_peserta'=>$id)))->row();

		$data['profile'] = '
		<section class="register-section pt-50 pb-50">
    <div class="container">
	<div class="register-box">
            
            <div class="sec-title text-center mb-30">
                <h2 class="title mb-10">Ubah Password</h2>
            </div>
            
            <!-- Login Form -->
            <div class="styled-form">
                <div id="form-messages"></div>
                <form method="post" action="'.site_url().'profile/simpan_password">  
				<input type="hidden" name="id_peserta" value="'.$peserta->id_peserta.'">                             
                    <div class="row clearfix">                                    
                        <!-- Form Group -->
                        <div class="form-group col-lg-12 mb-25">
                            <input type="password" name="old_password" placeholder="Masukkan Password Lama" required>
                        </div>

						<div class="form-group col-lg-12 mb-25">
                            <input type="password" name="new_password" placeholder="Masukkan Password Baru" required>
                        </div>

						<div class="form-group col-lg-12 mb-25">
                            <input type="password" name="conf_password" placeholder="Konfirmasi Password Baru" required>
                        </div>
                        
                        <div class="form-group col-lg-12 col-md-12 col-sm-12 text-center mb-25">
                            <button type="submit" class="readon register-btn"><span class="txt">Simpan</span></button>
                        </div>
                    </div>
                    
                </form>
            </div>
            
        </div>
		</div>
		</section>';

		$data['content'] = "front/profile";
		$this->load->view('front/index', $data);
	}

	public function simpan_profil()
	{
	    $id_peserta = $this->input->post('id_peserta');
	    $nama_peserta = $this->input->post('nama_peserta');
	    $email = $this->input->post('email');
	    $no_telp = $this->input->post('no_telp');
	    $alamat = $this->input->post('alamat');

        $par = array(
            'tabel'=>'peserta',
            'data'=>array(
            'nama_peserta'=>$nama_peserta,
            'email'=>$email,
            'no_telp'=>$no_telp,
            'alamat'=>$alamat,
            ),
        );

        if($id_peserta != NULL) $par['where'] = array('id_peserta'=>$id_peserta);

        $sim = $this->Model_general->save_data($par);

        redirect('profile');

	}

	public function simpan_password()
	{
	    $id_peserta = $this->input->post('id_peserta');

		$peserta = $this->Model_general->getdata(array('tabel'=>'peserta', 'where'=>array('id_peserta'=>$id_peserta)))->row();

	    $old_password = $this->input->post('old_password');
	    $new_password = $this->input->post('new_password');
	    $conf_password = $this->input->post('conf_password');

		if (md5($old_password) == $peserta->password) {
			if ($new_password == $conf_password) {
				$par = array(
					'tabel'=>'peserta',
					'data'=>array(
					'password'=>md5($new_password),
					),
				);

				if($id_peserta != NULL) $par['where'] = array('id_peserta'=>$id_peserta);

				$sim = $this->Model_general->save_data($par);

				$this->session->set_flashdata('ok','swal.fire({
					title: "Berhasil",
					text: "Password anda berhasil diubah",
					icon: "success",
				});');

				redirect('profile');
			} else {
				$this->session->set_flashdata('fail','swal.fire({
					title: "Gagal",
					text: "Password tidak sama, silahkan dicoba kembali",
					icon: "warning",
				});');
				redirect('profile/ubah_password');
			}
		} else {
				$this->session->set_flashdata('fail','swal.fire({
					title: "Gagal",
					text: "Password tidak ditemukan, silahkan dicoba kembali",
					icon: "warning",
				});');
				redirect('profile/ubah_password');
		}

        

	}

}
