<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Video extends REST_Controller
{

  var $table = 'video';

  public function index_get()
  {
    $id_video = $this->get('id_video');
    $status = 1;
    $aktif = 1;

    $fro = array(
        ''.$this->table.' a'=>'',
        'ref_kategori b'=>'b.id_ref_kategori=a.id_ref_kategori',
        'freelancer e'=>'e.id_freelancer=a.id_freelancer',
        'ref_matkul f'=>'f.id_ref_matkul=a.id_ref_matkul'
    );

    $sel = 'a.*,b.ref_kategori,f.ref_matkul';

    $whe = array();

    if(!empty($id_video)) $whe['a.id_video'] = $id_video;
    if(!empty($status)) $whe['a.status'] = $status;
    if(!empty($aktif)) $whe['a.aktif'] = $aktif;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = $model->result();
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
