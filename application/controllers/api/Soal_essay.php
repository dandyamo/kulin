<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Soal_essay extends REST_Controller
{

  var $table = 'ref_soal_essay';

  public function index_get()
  {
    $id_ref_soal_essay = $this->get('id_ref_soal_essay');
    $id_ref_kategori = $this->get('id_ref_kategori');
    $id_ref_matkul = $this->get('id_ref_matkul');
    $id_ref_umum = $this->get('id_ref_umum');
    $id_tentor = $this->get('id_tentor');
    $status = 1;

    $fro = array(
        ''.$this->table.' s'=>'',
        'ref_kategori rk' => array('rk.id_ref_kategori = s.id_ref_kategori','left'),
        'ref_matkul rm' => array('rm.id_ref_matkul = s.id_ref_matkul','left'),
        'ref_umum ru' => array('ru.id_ref_umum = s.id_ref_umum','left'),
        'tentor t' => array('t.id_tentor = s.id_tentor','left'),
    );
    $sel = 's.*,rk.ref_kategori,rm.ref_matkul,ru.ref_umum,t.nama_tentor';

    $whe = array();

    if(!empty($id_ref_soal_essay)) $whe['s.id_ref_soal_essay'] = $id_ref_soal_essay;
    if(!empty($id_ref_kategori)) $whe['s.id_ref_kategori'] = $id_ref_kategori;
    if(!empty($id_ref_matkul)) $whe['s.id_ref_matkul'] = $id_ref_matkul;
    if(!empty($id_ref_umum)) $whe['s.id_ref_umum'] = $id_ref_umum;
    if(!empty($id_tentor)) $whe['s.id_tentor'] = $id_tentor;
    if(!empty($status)) $whe['s.status'] = $status;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = $model->result();
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
