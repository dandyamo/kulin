<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Matkul extends REST_Controller
{

  var $table = 'ref_matkul';

  public function index_get()
  {
    $id_ref_jurusan = $this->get('id_ref_jurusan');
    $status = 1;

    $fro = array(
        ''.$this->table.' rm'=>'',
        'ref_jurusan rj' => array('rj.id_ref_jurusan = rm.id_ref_jurusan','left'),
        'ref_fakultas rf' => array('rf.id_ref_fakultas = rm.id_ref_fakultas','left')
    );

    $sel = 'rm.*,rj.ref_jurusan,rf.ref_fakultas';

    $whe = array();

    if(!empty($id_ref_jurusan)) $whe['rm.id_ref_jurusan'] = $id_ref_jurusan;
    if(!empty($status)) $whe['rm.status'] = $status;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = $model->result();
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
