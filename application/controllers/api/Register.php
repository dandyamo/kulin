<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Register extends REST_Controller
{

  var $table = 'peserta';

  public function index_post()
  {

    if(md5($this->post('password')) == md5($this->post('conf_password'))){

      $data = [
        'nama_peserta' => $this->post('nama_peserta'),
        'no_telp' => $this->post('no_telp'),
        'email' => $this->post('email'),
        'alamat' => $this->post('alamat'),
        'username' => $this->post('username'),
        'password' => md5($this->post('password')),
        'jenis_kelamin' => $this->post('jenis_kelamin')
      ];
  
      if ($this->Model_api->createData($this->table,$data)) {
  
          $peserta = $this->Model_general->getdata(array('tabel'=>$this->table, 'where'=>array('username'=>$this->post('username'))))->row();
          $encode = base64_encode($this->post('no_telp').$this->post('email')); 
          $key = [
              'user_id' => $peserta->id_peserta,
              'key' => $encode
          ];
  
          $this->Model_api->createData('keys',$key);

          // Konfigurasi email
          $config['protocol']    = 'smtp';
          $config['smtp_host']    = 'mail.kulin.id';
          $config['smtp_port']    = 465;
          $config['smtp_timeout'] = '7';
          $config['smtp_user']    = 'info@kulin.id'; // email akun
          $config['smtp_pass']    = 'Kulinofficial22'; // password email
          $config['charset']    = 'utf-8';
          $config['smtp_crypto']    = 'ssl';
          $config['newline']  = "\r\n";
          $config['mailtype'] = 'html'; // or html
          $config['validation'] = TRUE; // bool whether to validate email or not


          $pesan = 'Yeay!, Anda sudah terdaftar di Kulin';

          $this->load->library('email', $config);
          $this->email->initialize($config);
          
          $this->email->from('info@kulin.id', 'Register Kulin.id');
          $this->email->to($this->post('email')); 
          $this->email->subject('Pendaftaran Berhasil');
          $this->email->message($pesan);
          $sent = $this->email->send();
  
          $this->response([
            'status' => true,
            'message' => 'Data berhasil ditambahkan'
          ], REST_Controller::HTTP_CREATED);
        }else{
          $this->response([
              'status' => false,
              'message' => 'Data gagal ditambah'
          ], REST_Controller::HTTP_BAD_REQUEST);
        }

    }else{

        $this->response([
          'status' => false,
          'message' => 'Password anda tidak sama'
      ], REST_Controller::HTTP_BAD_REQUEST);

    }
    
  }

}
