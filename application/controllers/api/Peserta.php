<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Peserta extends REST_Controller
{

  var $table = 'peserta';

  public function index_get()
  {
    $id_peserta = $this->get('id_peserta');
    $key = $this->get('API-KEY');

    $fro = array(
        ''.$this->table.' p'=>'',
        'keys k' => array('k.user_id = p.id_peserta','left')
    );
    
    $sel = 'p.*,k.key';

    $whe = array(
      'p.id_peserta' => $id_peserta,
      'k.key' => $key
  );

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = array();
    foreach ($model->result() as $r) {

        $photo = $this->config->item('appdir').'uploads/photo/'.$r->photo;
        if(file_exists($photo) && is_file($photo)) $photo = base_url().'uploads/photo/'.$r->photo;
        else $photo = base_url().'assets/backend/dist/img/user.png';

        $results[] = array(
            'id_peserta'=>$r->id_peserta,
            'nama_peserta'=>$r->nama_peserta,
            'no_telp'=>$r->no_telp,
            'email'=>$r->email,
            'alamat'=>$r->alamat,
            'username'=>$r->username,
            'password'=>$r->password,
            'jenis_kelamin'=>$r->jenis_kelamin,
            'photo'=>$photo, 
        );
    }
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

  public function index_put()
  {
    $id_peserta = $this->put('id_peserta');
    $key = $this->put('API-KEY');

    $where = array(
      'id_peserta' => $id_peserta
    );

    $data = [
        'nama_peserta' => $this->put('nama_peserta'),
        'no_telp' => $this->put('no_telp'),
        'email' => $this->put('email'),
        'alamat' => $this->put('alamat'),
        'username' => $this->put('username'),
        'jenis_kelamin' => $this->put('jenis_kelamin')
    ];

    if ($this->Model_api->updateData($this->table,$data, $where) > 0) {
      $this->response([
          'status' => true,
          'message' => 'Data berhasil diubah'
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data gagal diubah'
      ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function ganti_foto_post()
  {
    $id_peserta = $this->post('id_peserta');
    // $key = $this->put('API-KEY');

    $par = array(
        'tabel'=>'peserta',
        'data'=>array(
          'id_peserta'=>$id_peserta
        )
    );

    $photo = $_FILES['photo']['tmp_name'];
			if($photo!=NULL){
				$path = './uploads/photo';
				if (!is_dir($path)) mkdir($path,0777,TRUE);
				$this->load->library('upload');
				$config['upload_path'] = $path;
			    $config['allowed_types'] = '*';
			    $config['max_size']      = '0';
			    $this->upload->initialize($config);
			    $this->upload->do_upload('photo');
			    $data = $this->upload->data();
			    $photo = $data['file_name'];
			    $par['data']['photo'] = $photo;
			}

      if($id_peserta != NULL) $par['where'] = array('id_peserta'=>$id_peserta);

      $sim = $this->Model_general->save_data($par);

    if ($sim) {
      $this->response([
          'status' => true,
          'message' => 'Foto profil berhasil diubah'
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Foto profil gagal diubah'
      ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

  public function ubah_password_post()
  {
    $id_peserta = $this->post('id_peserta');
    $password = $this->post('password');
    $key = $this->post('API-KEY');

    $par = array(
        'tabel'=>'peserta',
        'data'=>array(
          'password'=>md5($password)
        )
    );

    if($id_peserta != NULL) $par['where'] = array('id_peserta'=>$id_peserta);

    $sim = $this->Model_general->save_data($par);

    if ($sim) {
      $this->response([
          'status' => true,
          'message' => 'Password berhasil diubah'
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Password gagal diubah'
      ], REST_Controller::HTTP_BAD_REQUEST);
    }
  }

}
