<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Login extends REST_Controller
{

  var $table = 'peserta';

  public function index_post()
  {
    $fro = array(
        ''.$this->table.' p'=>'',
        'keys k' => array('k.user_id = p.id_peserta','left')
    );
    $sel = 'p.*,k.key';

    $whe = array(
        'username' => $this->post('username'),
        'password' => md5($this->post('password'))
    );

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);
    $results = $model->result();

    if($results){
        $this->response([
            'status' => true,
            'data' => $results
        ], REST_Controller::HTTP_OK);
      }else{
        $this->response([
            'status' => false,
            'message' => 'Data tidak ditemukan'
        ], REST_Controller::HTTP_NOT_FOUND);
      }
  }

}
