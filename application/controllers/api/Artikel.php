<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Artikel extends REST_Controller
{

  var $table = 'artikel';

  public function index_get()
  {
    $id_artikel = $this->get('id_artikel');
    $status = 1;

    $fro = array(
        ''.$this->table.''=>'',
    );
    $sel = '*';

    $whe = array();

    if(!empty($id_artikel)) $whe['id_artikel'] = $id_artikel;
    if(!empty($status)) $whe['status'] = $status;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = array();
    foreach ($model->result() as $r) {

        $photo = $this->config->item('appdir').'uploads/file_artikel/'.$r->gambar_artikel;
        if(file_exists($photo) && is_file($photo)) $photo = base_url().'uploads/file_artikel/'.$r->gambar_artikel;
        else $photo = base_url().'assets/backend/dist/img/user.png';

        $results[] = array(
            'id_artikel'=>$r->id_artikel,
            'id_kategori_artikel'=>$r->id_kategori_artikel,
            'nama_artikel'=>$r->nama_artikel,
            'deskripsi_artikel'=>$r->deskripsi_artikel,
            'tgl_artikel'=>$r->tgl_artikel,
            'status'=>$r->status,
            'id_operator'=>$r->id_operator,
            'photo'=>$photo,
        );
    }
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
