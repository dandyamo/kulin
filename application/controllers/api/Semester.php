<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Semester extends REST_Controller
{

  var $table = 'ref_semester';

  public function index_get()
  {
    $id_ref_semester = $this->get('id_ref_semester');
    $status = 1;

    $fro = array(
        ''.$this->table.''=>'',
    );
    $sel = '*';

    $whe = array();

    if(!empty($id_ref_semester)) $whe['id_ref_semester'] = $id_ref_semester;
    if(!empty($status)) $whe['status'] = $status;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = $model->result();
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
