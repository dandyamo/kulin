<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Rekening extends REST_Controller
{

  var $table = 'ref_rekening';

  public function index_get()
  {
    $id_ref_rekening = $this->get('id_ref_rekening');
    $status = 1;

    $fro = array(
        ''.$this->table.''=>'',
    );
    $sel = '*';

    $whe = array();

    if(!empty($id_ref_rekening)) $whe['id_ref_rekening'] = $id_ref_rekening;
	if(!empty($status)) $whe['status'] = $status;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = array();
    foreach ($model->result() as $r) {

        $photo = $this->config->item('appdir').'uploads/bank/'.$r->logo_rekening;
        if(file_exists($photo) && is_file($photo)) $photo = base_url().'uploads/bank/'.$r->logo_rekening;
        else $photo = base_url().'assets/backend/dist/img/credit-card.png';

        $results[] = array(
            'id_ref_rekening'=>$r->id_ref_rekening,
            'nama_rekening'=>$r->nama_rekening,
            'nama_bank'=>$r->nama_bank,
            'no_rekening'=>$r->no_rekening,
            'photo'=>$photo,
            'status'=>$r->status,
        );
    }

    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }
  }

}
