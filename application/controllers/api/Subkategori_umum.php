<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Subkategori_umum extends REST_Controller
{

  var $table = 'ref_umum';

  public function index_get()
  {
    $id_ref_umum = $this->get('id_ref_umum');
    $id_ref_kat_umum = $this->get('id_ref_kat_umum');

    $fro = array(
        ''.$this->table.' ru'=>'',
        'ref_kat_umum ku' => array('ku.id_ref_kat_umum = ru.id_ref_kat_umum','left'),
    );
    $sel = 'ru.*';

    $whe = array();

    if(!empty($id_ref_umum)) $whe['id_ref_umum'] = $id_ref_umum;
    if(!empty($id_ref_kat_umum)) $whe['id_ref_kat_umum'] = $id_ref_kat_umum;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = $model->result();
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
