<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Tentor extends REST_Controller
{

  var $table = 'tentor';

  public function index_get()
  {
    $id_tentor = $this->get('id_tentor');

    $fro = array(
        ''.$this->table.''=>'',
    );
    $sel = '*';

    $whe = array();

    if(!empty($id_tentor)) $whe['id_tentor'] = $id_tentor;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = array();
    foreach ($model->result() as $r) {

        $photo = $this->config->item('appdir').'uploads/photo/'.$r->photo;
        if(file_exists($photo) && is_file($photo)) $photo = base_url().'uploads/photo/'.$r->photo;
        else $photo = base_url().'assets/backend/dist/img/user.png';

        $results[] = array(
            'id_tentor'=>$r->id_tentor,
            'nama_tentor'=>$r->nama_tentor,
            'no_telp'=>$r->no_telp,
            'email'=>$r->email,
            'alamat'=>$r->alamat,
            'username'=>$r->username,
            'password'=>$r->password,
            'jenis_kelamin'=>$r->jenis_kelamin,
            'photo'=>$photo,
        );
    }
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
