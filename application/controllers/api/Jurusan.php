<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Jurusan extends REST_Controller
{

  var $table = 'ref_jurusan';

  public function index_get()
  {
    $id_ref_fakultas = $this->get('id_ref_fakultas');
    $status = 1;

    $fro = array(
        ''.$this->table.' rj'=>'',
        'ref_fakultas rf' => array('rf.id_ref_fakultas = rj.id_ref_fakultas','left')
      );
  
      $sel = 'rj.*,rf.ref_fakultas';

    $whe = array();

    if(!empty($id_ref_fakultas)) $whe['rj.id_ref_fakultas'] = $id_ref_fakultas;
    if(!empty($status)) $whe['rj.status'] = $status;

    $model_params = array(
        'tabel'=>$fro,
        'select'=>$sel,
        'where'=>$whe
    );

    $model = $this->Model_api->datagrabs($model_params);

    $results = $model->result();
    
    if($results){
      $this->response([
          'status' => true,
          'data' => $results
      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
          'status' => false,
          'message' => 'Data tidak ditemukan'
      ], REST_Controller::HTTP_NOT_FOUND);
    }

  }

}
