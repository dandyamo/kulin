<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function index()
	{
		$data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$data['artikel'] = '';

		$from = array(
			'artikel a' => '',
            'ref_kategori_artikel rk' => array('a.id_kategori_artikel = rk.id_kategori_artikel','left'),
            'operator o' => array('o.id_operator = a.id_operator','left'),
		);

        $artikel = $this->Model_general->getdata(array('tabel'=>$from, 'order'=>'a.tgl_artikel DESC'));

		foreach ($artikel->result() as $art) {
			$data['artikel'] .= '<div class="col-lg-6">
                            <div class="course-item mb-40">
                                <div class="course-image">
                                  <a href="#">
                                      <img src="'.base_url().'uploads/file_artikel/'.$art->gambar_artikel.'" alt="images">
                                  </a>
                                </div>
                                <div class="course-info">
                                 
                                    <h3 class="course-title">
                                        <a href="course-single.html">'.$art->nama_artikel.'</a>
                                    </h3>
									<p>'.$art->tgl_artikel.'</p>
                                    <div class="bottom-part">
                                        <div class="info-meta">
                                           
                                        </div>
                                        <div class="btn-part">
                                            <a href="'.site_url().'artikel/detail/'.$art->id_artikel.'">Selengkapnya<i class="flaticon-right-arrow"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';
		}

		
		$data['content'] = "front/artikel";
		$this->load->view('front/index', $data);
	}

	public function detail($id=null)
	{
		$data['par'] = $this->Model_general->get_param(array('title','description','address','phone','email','footer','logo','keywords'),2);

		$from = array(
			'artikel a' => '',
            'ref_kategori_artikel rk' => array('a.id_kategori_artikel = rk.id_kategori_artikel','left'),
            'operator o' => array('o.id_operator = a.id_operator','left'),
		);

        $artikel = $this->Model_general->getdata(array('tabel'=>$from, 'where'=>array('a.id_artikel'=>$id)))->row();

		$data['artikel'] = '<div class="col-lg-12 md-mb-50">
                            <!-- Intro Info Tabs-->
                            <div class="intro-info-tabs">
                              <img src="'.base_url().'uploads/file_artikel/'.$artikel->gambar_artikel.'" style="width: 100%"
                                <div class="tab-content tabs-content" id="myTabContent">
                                    <div class="tab-pane tab fade show active" id="prod-overview" role="tabpanel" aria-labelledby="prod-overview-tab">
                                        <div class="content white-bg pt-30">
                                            <!-- Cource Overview -->
                                            <div class="course-overview">
                                                <div class="inner-box">
                                                    <h4>'.$artikel->nama_artikel.'</h4>
                                                    <p>'.$artikel->deskripsi_artikel.'</p>
                                                    <ul class="student-list">
                                                        <li>'.$artikel->tgl_artikel.'</li>
                                                        <li>'.$artikel->nama_operator.'</li>
                                                    </ul>
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>';

		
		$data['content'] = "front/detail_artikel";
		$this->load->view('front/index', $data);
	}
}
